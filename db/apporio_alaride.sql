-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:05 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_alaride`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'Ala Ride', 'uploads/logo/logo_59bb8a5d090c8.png', 'hello@apporio.com', 'Dubai - United Arab Emirates', 'AIzaSyCe2u2K-2ZEhrJ8ShCUtLUoBppjp2jFFSM ', '25.2048493', '55.2707828', 'alaride-178310');

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_allocated`
--

INSERT INTO `booking_allocated` (`booking_allocated_id`, `rental_booking_id`, `driver_id`, `status`) VALUES
(1, 1, 8, 1),
(2, 2, 8, 1),
(3, 3, 16, 1),
(4, 4, 17, 1),
(5, 5, 17, 1),
(6, 6, 17, 1),
(7, 7, 17, 1),
(8, 8, 17, 1),
(9, 9, 17, 1),
(10, 10, 17, 1),
(11, 11, 17, 1),
(12, 12, 17, 1),
(13, 13, 8, 1),
(14, 14, 19, 1),
(15, 15, 17, 1),
(16, 16, 8, 1),
(17, 17, 19, 1),
(18, 18, 19, 1),
(19, 19, 14, 1),
(20, 20, 14, 1),
(21, 21, 24, 1),
(22, 22, 14, 1),
(23, 23, 24, 1),
(24, 24, 14, 1),
(25, 25, 24, 1),
(26, 26, 24, 1),
(27, 27, 14, 1),
(28, 28, 14, 1),
(29, 29, 14, 1),
(30, 30, 14, 1),
(31, 31, 14, 1),
(32, 32, 14, 1),
(33, 33, 14, 1),
(34, 34, 14, 1),
(35, 35, 14, 1),
(36, 36, 14, 1),
(37, 37, 14, 1),
(38, 38, 14, 1),
(39, 39, 14, 1),
(40, 40, 14, 1),
(41, 41, 24, 1),
(42, 42, 14, 1),
(43, 43, 24, 1),
(44, 44, 14, 1),
(45, 45, 24, 1),
(46, 46, 24, 1),
(47, 47, 24, 1),
(48, 48, 22, 1),
(49, 49, 24, 1),
(50, 51, 27, 1),
(51, 52, 27, 1),
(52, 53, 27, 1),
(53, 54, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(1, 'cus_BWf8J38LzFVuoM', 17),
(2, 'cus_BWf8G5I71nOyfb', 17);

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1),
(13, 'yamaha', 'uploads/car/car_13.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `cartype_rating` varchar(255) NOT NULL DEFAULT '0',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `cartype_rating`, `car_admin_status`) VALUES
(2, 'HATCHBACK', '', 'HATCHBACK', 'uploads/car/editcar_2.png', '', 1, '3.625', 1),
(3, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_3.png', '', 1, '4.58333333333', 1),
(4, 'Mini', '', 'Mini', 'uploads/car/editcar_4.png', '', 1, '4.21153846154', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '&#8377', 'Miles', '', '', 1),
(120, 'Dubai - United Arab Emirates', '25.2048493', '55.2707828', '$', 'Km', '', '', 1),
(121, 'Karachi', '24.8614622', '67.0099388', '$', 'Km', '', '', 1),
(122, 'Sharjah - United Arab Emirates', '25.3462553', '55.4209317', '$', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(2, 'Ashar', 'noorabbasi74@gmail.com', '0509400567', 'Dubai - United Arab Emirates', 'UNITED ARAB EMIRATES', 120, 'Noor ul haq Abbasi', 'forgotten', '123456', 'uploads/company/company_2.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 63, 'MOTO TAXI SP JÃ', 1, 2, 1, 2, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com2', '', 'tawsila', '', 'tawsila', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(74, 'APPORIO', '10', 0, 0, '2017-08-14', '2017-09-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2018-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2018-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-13', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1),
(108, 'nominalashar', '110', 100, 2, '2017-09-20', '2017-09-28', 'Nominal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`) VALUES
(1, 'Doller', '&#36', '&#36'),
(2, 'POUND', '&#163', '&#163'),
(3, 'Indian Rupees', '&#8377', '&#8377');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 1, 'ananna', 'anaknak', '939293', 'kaksak', 'Saturday, Aug 26, 01:05 PM'),
(2, 2, 'xhxhxhhx', 'jcjcjcjcjc45@t.com', '1234567990', 'xhlduohldgkzDJtisxkggzk', 'Saturday, Aug 26, 01:06 PM'),
(3, 2, 'dyhddh', 'hxhxjccj57@y.com', '3664674456', 'chicxgxhxyhccphxoh', 'Saturday, Aug 26, 01:10 PM'),
(4, 2, 'bzxhjx', 'xhcjlbgkog46@t.com', '488668966', 'hxcjycpots gkkcDuizgk g kbclh', 'Saturday, Aug 26, 01:12 PM'),
(5, 1, 'hdxhxh', 'pcijcjcjc56@y.com', '63589669', 'bhxcjn tfg nzgogxoxgxl', 'Saturday, Aug 26, 01:46 PM'),
(6, 2, 'fbfbfbfb', 'fbfbfbfb', 'hdhdhhdhdhdh', 'udydhdhdg', 'Saturday, Aug 26, 06:20 PM'),
(7, 2, 'd xrcr', 'd xrcr', 'wzxxwxwxe', 'ssxscecececrcrcrcrcrcrcrc', 'Saturday, Aug 26, 06:25 PM'),
(8, 2, 'Shilpa', 'Shilpa', '9865321478', 'hi ', 'Sunday, Aug 27, 09:25 AM'),
(9, 2, 'hsjhhshh', 'hsjhhshh', '9969569', 'vvvvvvgg', 'Monday, Aug 28, 11:17 AM'),
(10, 1, 'Shivani', 'fxuxyuguhco@gmail.com', 'trafsyxtuxfuxfu', 'Fyfzyhf hf', 'Monday, Aug 28, 11:25 AM'),
(11, 2, 'zfjzgjzjgzgj', 'jfititig@gmail.com', 'dghvvch', 'Dfhxxhhxhc', 'Monday, Aug 28, 11:26 AM'),
(12, 2, 'dhjsjz', 'dhjsjz', '76979797979', 'zhhzhzhzhhz', 'Monday, Aug 28, 05:28 PM'),
(13, 1, 'Halmat ', 'LuxuryRidesLimo@gmail.com', '8477645466', 'Hey this is Halmat \nCan you call me or email me \nNeed to talk to you\n\nThanks ', 'Wednesday, Sep 6, 08:38 PM'),
(14, 2, 'anuuuuuu', 'anuuuuuu', '8874531856', 'jftjffugjvvd', 'Thursday, Sep 7, 10:45 AM'),
(15, 2, 'André', 'André', '1195855708870', 'seja bem-vindo ', 'Tuesday, Sep 12, 05:38 AM'),
(16, 2, 'anurag', 'anurag', '880858557585', 'zfgfgfgxxfdcghdgc', 'Tuesday, Sep 12, 11:20 AM'),
(17, 2, 'MOHAMED ', 'MOHAMED ', '8639206529', 'taxi app', 'Wednesday, Sep 13, 11:17 PM'),
(18, 2, 'amrit', 'amrit', '0686566855', 'ucjcjyjbkvffvkb', 'Thursday, Sep 14, 10:56 AM'),
(19, 2, 'ndufu', 'ndufu', '27342438', 'ududcucuc', 'Thursday, Sep 14, 11:06 AM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `previous_outstanding` varchar(255) NOT NULL DEFAULT '0',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `previous_outstanding`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(28, 35, '28.4120600708895', '77.0432610700599', '28.412122169441', '77.043165914021', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:38:47 PM', '03:38:57 PM', '03:39:20 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '100', '5.00', '95.00', '100.00', '0.00', '0', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(30, 39, '25.3006127011267', '55.4425412603239', '25.3005399042976', '55.4426619597296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Unnamed Road - Sharjah - United Arab Emirates', '07:26:15 PM', '07:26:21 PM', '07:27:35 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '1.00', '99.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(31, 40, '28.4121731758348', '77.0431668783676', '28.4121263502669', '77.0432270905836', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:16:46 AM', '06:18:23 AM', '06:20:28 AM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 9, '115', '5.75', '109.25', '100.00', '0.00', '0', '0.00 Miles', '0.0', '2', '115', 1, '', 1, 1, '0000-00-00'),
(32, 42, '28.4120642798562', '77.0431972317563', '28.4121045411424', '77.0433127902247', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:32:44 AM', '06:32:49 AM', '06:33:53 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '100', '5.00', '95.00', '100.00', '0.00', '0.00', '0.00 Miles', '236.41051397014', '1', '100', 1, '', 1, 0, '0000-00-00'),
(33, 45, '28.4121626409675', '77.0431586545998', '28.4120431688056', '77.0432494182313', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:52:10 AM', '06:52:31 AM', '06:52:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '100', '5.00', '95.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(34, 46, '25.0896719238023', '55.1526474394418', '25.0896487897496', '55.1526047755546', 'Shatha Tower - Dubai - United Arab Emirates', 'Shatha Tower - Dubai - United Arab Emirates', '07:54:27 AM', '07:54:56 AM', '07:55:27 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '1.00', '99.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(35, 47, '25.089627457806', '55.1525274105884', '25.089627457806', '55.1525274105884', 'Media 1 Tower - Dubai - United Arab Emirates', 'Media 1 Tower - Dubai - United Arab Emirates', '08:07:34 AM', '08:07:39 AM', '08:07:50 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '1.00', '99.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(36, 48, '25.0891877431656', '55.1520973351366', '25.0891248369823', '55.1520828344442', 'Alkhayay St - Dubai - United Arab Emirates', 'Alkhayay St - Dubai - United Arab Emirates', '08:30:45 AM', '08:30:49 AM', '08:30:55 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '0.00', '100', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(37, 49, '25.0946455075416', '55.1507776494982', '25.0944726209676', '55.1507947607672', 'King Salman Bin Abdulaziz Al Saud St - Dubai - United Arab Emirates', 'King Salman Bin Abdulaziz Al Saud St - Dubai - United Arab Emirates', '08:39:50 AM', '08:39:54 AM', '08:39:59 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '0.00', '100', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(38, 50, '25.0897293655119', '55.152499157999', '25.2519659', '55.3328027', 'Media 1 Tower - Dubai - United Arab Emirates', '27 8th St - Dubai - United Arab Emirates', '08:59:09 AM', '08:59:13 AM', '08:59:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '0.00', '100', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(39, 52, '25.0896585313416', '55.1526191258868', '25.0893096998568', '55.1529314183212', 'Shatha Tower - Dubai - United Arab Emirates', 'Al Shatha Tower - Dubai - United Arab Emirates', '03:41:21 PM', '03:41:29 PM', '03:42:05 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '0.00', '100', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(40, 53, '25.0896204773982', '55.1525552313179', '25.0897600332183', '55.1527247645629', 'Al Falak St - Dubai - United Arab Emirates', 'Shatha Tower - Dubai - United Arab Emirates', '04:10:35 PM', '04:10:43 PM', '04:20:32 PM', '0', '0.00', '8.00', '0.00', '0.00', '0.00', 8, '108', '0.00', '108', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '9', '108', 1, '', 1, 1, '0000-00-00'),
(41, 55, '', '', '', '', '', '', '04:46:37 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 8, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(42, 55, '', '', '', '', '', '', '04:46:43 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 8, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(43, 55, '', '', '', '', '', '', '04:46:51 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 8, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(44, 55, '', '', '', '', '', '', '04:46:58 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 8, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(45, 56, '25.089557343186', '55.1521783881403', '25.0895147631179', '55.152148632384', 'Al Falak St - Dubai - United Arab Emirates', 'Al Falak St - Dubai - United Arab Emirates', '12:25:21 PM', '12:25:29 PM', '12:25:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '0.00', '100', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(46, 57, '25.0896938940055', '55.1524604966996', '25.0897662391077', '55.1524509640218', 'Media 1 Tower - Dubai - United Arab Emirates', 'Media 1 Tower - Dubai - United Arab Emirates', '04:13:26 PM', '04:13:30 PM', '04:13:37 PM', '0', '0.00', '00.00', '0.00', '0.00', '250', 8, '0', '0.00', '0.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(47, 59, '25.0892728613923', '55.1519363187767', '25.0892728613923', '55.1519363187767', 'Alkhayay St - Dubai - United Arab Emirates', 'Alkhayay St - Dubai - United Arab Emirates', '04:26:46 PM', '04:26:53 PM', '04:26:59 PM', '0', '0.00', '00.00', '0.00', '0.00', '110', 8, '0', '0.00', '0.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(48, 60, '28.4120626662052', '77.0432522745212', '28.4121496244161', '77.043216143922', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:06:46 AM', '07:07:00 AM', '07:07:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '100', '5.00', '95.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(49, 66, '25.300743681596', '55.4423807771109', '25.3006865876031', '55.4426776338885', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Unnamed Road - Sharjah - United Arab Emirates', '10:12:34 PM', '10:12:41 PM', '10:12:56 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '100', '0.00', '100', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(50, 69, '28.4120744594858', '77.0432923687375', '28.412103374172', '77.0432401432104', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:48:20 AM', '11:50:26 AM', '11:50:41 AM', '2', '20.00', '00.00', '0.00', '0.00', '0.00', 6, '120', '6.00', '114.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '120', 1, '', 1, 1, '0000-00-00'),
(51, 72, '28.5927566', '77.3100342', '28.4122189', '77.0432347', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:25:04 AM', '09:25:27 AM', '09:15:03 AM', '0', '0.00', '135.00', '0.00', '0.00', '0.00', 13, '235', '9.40', '225.60', '100.00', '0.00', '0', '0.00 Miles', '0.0', '10', '235', 1, '', 1, 0, '0000-00-00'),
(52, 75, '25.0885734', '55.1533528', '25.0885734', '55.1533528', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '08:39:52 AM', '08:39:57 AM', '08:41:10 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 16, '80', '0.00', '80', '80.00', '0.00', '0', '0.00 Km', '0.0', '1', '80', 1, '', 1, 1, '0000-00-00'),
(53, 76, '25.0885734', '55.1533528', '25.0885734', '55.1533528', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '09:03:06 AM', '09:03:36 AM', '09:04:19 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 16, '80', '0.00', '80', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 1, '0000-00-00'),
(54, 77, '28.4122096', '77.0432463', '28.4122098', '77.043246', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:18:52 AM', '09:18:54 AM', '09:18:59 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 13, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(55, 78, '28.4122141', '77.0432404', '28.4122141', '77.0432404', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:33:38 AM', '09:33:40 AM', '09:33:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 13, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(56, 79, '25.0889658', '55.150858', '25.0892203', '55.1515676', 'Alkhayay St - Dubai - United Arab Emirates', 'Alkhayay St - Dubai - United Arab Emirates', '09:52:04 AM', '09:52:19 AM', '09:52:50 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '80', '8.00', '72.00', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 0, '0000-00-00'),
(57, 80, '28.4122103', '77.0432477', '28.4122103', '77.0432477', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:08:15 AM', '10:08:18 AM', '10:08:22 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 13, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(58, 81, '25.0894871028374', '55.1528216153897', '25.0893941056217', '55.1527579967446', 'Al Shatha Tower - Dubai - United Arab Emirates', 'Al Shatha Tower - Dubai - United Arab Emirates', '10:10:33 AM', '10:10:44 AM', '10:12:39 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '80', '8.00', '72.00', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '1', '80', 1, '', 1, 1, '0000-00-00'),
(59, 82, '25.0895070098574', '55.1525165979333', '25.0893974164734', '55.1522640511907', 'Al Shatha Tower - Dubai - United Arab Emirates', 'Al Falak St - Dubai - United Arab Emirates', '10:16:38 AM', '10:16:44 AM', '10:16:59 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '80', '8.00', '72.00', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 1, '0000-00-00'),
(60, 90, '25.0898077944527', '55.1523824874825', '25.0898139970611', '55.1525088865824', 'Media 1 Tower - Dubai - United Arab Emirates', 'Media 1 Tower - Dubai - United Arab Emirates', '02:18:11 PM', '02:18:30 PM', '02:18:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '80', '8.00', '72.00', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 0, '0000-00-00'),
(61, 91, '25.0898139970611', '55.1525088865824', '25.0898139970611', '55.1525088865824', 'Media 1 Tower - Dubai - United Arab Emirates', 'Media 1 Tower - Dubai - United Arab Emirates', '02:21:53 PM', '02:22:00 PM', '02:22:08 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '80', '8.00', '72.00', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 1, '0000-00-00'),
(62, 98, '25.3010724635801', '55.442881251438', '25.2788468', '55.3309395', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '12 Hor Al Anz St - Dubai - United Arab Emirates', '08:24:24 PM', '08:24:36 PM', '08:24:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 19, '100', '1.00', '99.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(63, 102, '25.0897528277014', '55.1525520043574', '25.2547879', '55.3298467', 'Media 1 Tower - Dubai - United Arab Emirates', '40 30 St - Dubai - United Arab Emirates', '10:12:28 AM', '10:12:45 AM', '10:12:54 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 19, '100', '1.00', '99.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(64, 104, '25.3006524732572', '55.4425990954558', '25.3006524732572', '55.4425990954558', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '04:10:12 PM', '04:10:25 PM', '04:11:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 19, '100', '1.00', '99.00', '100.00', '0.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(65, 107, '25.300772292563', '55.442507145978', '25.300772292563', '55.442507145978', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '04:55:06 PM', '04:55:39 PM', '04:56:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 23, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 1, 1, '0000-00-00'),
(66, 109, '25.3008213766176', '55.4427723161056', '25.3007538087356', '55.4425515682645', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '04:59:39 PM', '05:03:58 PM', '05:04:07 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 23, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 1, 1, '0000-00-00'),
(67, 110, '28.4122038564548', '77.0433068896323', '28.4121005863178', '77.0432783496831', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:24:20 AM', '06:24:26 AM', '06:27:45 AM', '0', '0.00', '30.00', '0.00', '0.00', '0.00', 14, '130', '5.20', '124.80', '100.00', '0.00', '0.00', '1.09 Miles', '1505.97849367191', '3', '130', 1, '', 1, 1, '0000-00-00'),
(68, 111, '28.4121325731934', '77.0432958006265', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '06:35:25 AM', '06:35:32 AM', '06:35:42 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 13, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(69, 112, '28.4121325731934', '77.0432958006265', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '06:36:29 AM', '06:36:44 AM', '06:36:49 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 13, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(70, 115, '28.4121056823748', '77.0432572892246', '28.4121156147183', '77.0432165348106', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:43:38 AM', '06:44:25 AM', '06:48:15 AM', '0', '0.00', '30.00', '0.00', '0.00', '0.00', 13, '130', '5.20', '124.80', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '3', '130', 1, '', 1, 1, '0000-00-00'),
(71, 117, '', '', '', '', '', '', '06:51:20 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 13, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(72, 118, '28.4120888068767', '77.0432920565624', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '07:31:39 AM', '07:31:44 AM', '07:32:02 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(73, 120, '28.4120934995083', '77.0432656599105', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '07:40:20 AM', '07:40:24 AM', '07:40:29 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(74, 121, '28.4121339658638', '77.0433880733484', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '07:46:55 AM', '07:47:11 AM', '07:47:29 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(75, 122, '28.4122114', '77.0431702', '28.4122113', '77.0431695', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:56:55 AM', '09:57:17 AM', '10:19:38 AM', '0', '0.00', '300.00', '0.00', '0.00', '0.00', 24, '478', '28.68', '449.32', '178.00', '0.00', '0.00', '0.00 Miles', '0.0', '22', '478', 1, '', 1, 1, '0000-00-00'),
(76, 123, '28.4122115', '77.0431691', '28.4122038', '77.0431839', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:21:20 AM', '10:21:40 AM', '10:26:57 AM', '0', '0.00', '45.00', '0.00', '0.00', '0.00', 24, '223', '13.38', '209.62', '178.00', '0.00', '0.00', '0.00 Miles', '0.0', '5', '223', 1, '', 1, 0, '0000-00-00'),
(77, 124, '28.4122011', '77.0431896', '28.4121992', '77.0431931', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:28:34 AM', '10:28:56 AM', '10:29:29 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 24, '178', '10.68', '167.32', '178.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(78, 125, '28.4121058672937', '77.0433537577345', '28.524578699999996', '77.206615', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Pramod Mahajan Marg, Block A, Block D, Saket, New Delhi, Delhi 110017, India', '10:31:04 AM', '10:32:27 AM', '10:32:44 AM', '1', '10.00', '00.00', '0.00', '0.00', '0.00', 14, '110', '4.40', '105.60', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '110', 1, '', 1, 1, '0000-00-00'),
(79, 126, '28.4122188', '77.0432334', '28.4122184', '77.0432297', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:14:41 AM', '11:18:27 AM', '11:18:50 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 24, '178', '10.68', '167.32', '178.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(80, 127, '28.41219445799', '77.0433570146672', '28.4121620529884', '77.043235872781', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:18:36 AM', '11:18:52 AM', '11:20:11 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 0, '0000-00-00'),
(81, 134, '28.412173260055', '77.0433120875275', '28.4121707008563', '77.0433445868474', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:30:08 AM', '11:30:17 AM', '11:30:25 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(82, 152, '28.4122965546431', '77.0432523043682', '28.4120983062654', '77.0432776662826', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:13:07 PM', '12:13:17 PM', '12:13:25 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(83, 153, '28.4120934979089', '77.043300951434', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '12:20:40 PM', '12:20:45 PM', '12:20:51 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(84, 156, '28.4122188', '77.0432349', '28.4122188', '77.0432349', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:30:12 PM', '12:30:16 PM', '12:30:22 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 24, '178', '10.68', '167.32', '178.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(85, 157, '28.4122405444987', '77.0432745093745', '28.4121141239284', '77.0432308058575', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:36:32 PM', '12:36:36 PM', '12:36:41 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(86, 158, '28.4121755248167', '77.0434493994575', '28.4121936431597', '77.0433271888447', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:28:09 PM', '01:28:14 PM', '01:28:20 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(87, 159, '28.4120982932147', '77.0433289068324', '28.4122545671826', '77.0432582097884', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:32:56 PM', '01:33:00 PM', '01:33:07 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '4.00', '96.00', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(88, 160, '25.0891624717276', '55.1528972201563', '25.089239593386', '55.1530119561584', 'Shatha Tower - Dubai - United Arab Emirates', 'Shatha Tower - Dubai - United Arab Emirates', '03:11:50 PM', '03:12:05 PM', '03:13:56 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 16, '80', '0.00', '80', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '1', '80', 1, '', 1, 1, '0000-00-00'),
(89, 161, '25.0895055430244', '55.1527717430658', '25.0895055430244', '55.1527717430658', 'Al Shatha Tower - Dubai - United Arab Emirates', 'Al Shatha Tower - Dubai - United Arab Emirates', '03:15:41 PM', '03:15:48 PM', '03:16:09 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 16, '80', '0.00', '80', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 1, '0000-00-00'),
(90, 163, '25.0893592992198', '55.1529612216692', '25.0892573636092', '55.1531849306135', 'Al Shatha Tower - Dubai - United Arab Emirates', 'Shatha Tower - Dubai - United Arab Emirates', '05:17:58 PM', '05:18:06 PM', '06:49:06 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 16, '80', '0.00', '80', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '31', '80', 1, '', 1, 1, '0000-00-00'),
(91, 164, '25.3007408', '55.4427149', '25.3007497', '55.4427158', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '07:19:35 PM', '07:19:42 PM', '07:19:56 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 1, 1, '0000-00-00'),
(92, 165, '25.3007497', '55.4427158', '25.3007497', '55.4427158', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '08:10:52 PM', '08:11:01 PM', '08:12:22 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '1', '200', 1, '', 1, 1, '0000-00-00'),
(93, 166, '25.3007408', '55.4427149', '25.2968339', '55.439567', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Fire Station Road - Muwaileh - Sharjah - United Arab Emirates', '08:13:31 PM', '08:13:52 PM', '08:29:07 PM', '0', '0.00', '50.00', '0.00', '0.00', '0.00', 22, '250', '2.50', '247.50', '200.00', '0.00', '0.00', '0.62 Km', '510.0', '15', '250', 1, '', 1, 1, '0000-00-00'),
(94, 167, '', '', '', '', '', '', '08:57:46 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 22, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(95, 168, '', '', '', '', '', '', '08:59:41 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 22, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(96, 169, '25.3007408', '55.4427149', '25.3007497', '55.4427158', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '09:04:40 PM', '09:04:46 PM', '09:05:39 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 1, 0, '0000-00-00'),
(97, 170, '25.3007497', '55.4427158', '', '', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '05:30:27 AM', '05:30:31 AM', '', '0', '0', '0', '0.00', '0.00', '0.00', 22, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(98, 175, '25.0885157', '55.153439', '25.0885157', '55.153439', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '01:38:42 PM', '01:38:48 PM', '01:38:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 1, 1, '0000-00-00'),
(99, 176, '25.0885157', '55.153439', '25.0885156', '55.153439', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '01:40:04 PM', '01:40:17 PM', '01:45:11 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0', '0.00 Km', '0.0', '4', '200', 1, '', 1, 1, '0000-00-00'),
(100, 178, '25.0885157', '55.153439', '25.0885157', '55.153439', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '01:50:29 PM', '01:51:01 PM', '01:51:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 0, 1, '0000-00-00'),
(101, 177, '25.0894823251526', '55.1523301844067', '25.0895157270367', '55.1522967406131', 'Al Falak St - Dubai - United Arab Emirates', 'Al Falak St - Dubai - United Arab Emirates', '01:50:36 PM', '01:51:11 PM', '01:51:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 16, '80', '0.00', '80', '80.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '80', 1, '', 1, 1, '0000-00-00'),
(102, 179, '25.0885157', '55.153439', '25.0885157', '55.153439', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '05:04:00 PM', '05:04:34 PM', '05:04:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(103, 180, '25.0885157', '55.153439', '25.0885157', '55.153439', 'Al Naseem St - Dubai - United Arab Emirates', 'Al Naseem St - Dubai - United Arab Emirates', '05:05:49 PM', '05:07:47 PM', '05:07:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(104, 181, '25.0890321', '55.1531501', '', '', 'Al Naseem St - Dubai - United Arab Emirates', '', '05:44:20 PM', '05:44:26 PM', '', '0', '0', '0', '0.00', '0.00', '0.00', 22, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(105, 182, '28.4122129', '77.0432422', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:35:40 AM', '06:35:44 AM', '07:00:35 AM', '0', '0.00', '345.00', '0.00', '0.00', '0.00', 13, '445', '17.80', '427.20', '100.00', '0.00', '0.00', '0.00 Miles', '0.0', '24', '445', 1, '', 1, 0, '0000-00-00'),
(106, 184, '', '', '', '', '', '', '07:01:36 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 13, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(107, 185, '', '', '', '', '', '', '07:02:23 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 13, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(108, 187, '28.413541098675', '77.0450808112732', '28.4592693', '77.0724192', 'Tower 14A, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '08:31:23 AM', '08:31:41 AM', '08:31:47 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 26, '100', '50.00', '50.00', '100.00', '0.00', '0', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(109, 190, '', '', '', '', '', '', '11:36:54 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 27, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(110, 191, '25.3007530141857', '55.4425798170785', '25.3004655568165', '55.4427031148741', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Unnamed Road - Sharjah - United Arab Emirates', '11:40:41 AM', '11:40:51 AM', '12:12:21 PM', '0', '0.00', '52.00', '0.00', '0.00', '0.00', 27, '152', '1.52', '150.48', '100.00', '0.00', '0', '0.00 Km', '0.0', '31', '152', 1, '', 1, 0, '0000-00-00'),
(111, 193, '', '', '', '', '', '', '12:29:47 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 27, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(112, 196, '', '', '', '', '', '', '12:44:26 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 27, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(113, 197, '', '', '', '', '', '', '12:46:57 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 27, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(114, 199, '25.3007409', '55.4427117', '25.3007425', '55.4427124', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '01:04:35 PM', '01:04:48 PM', '01:04:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 28, '200', '2.00', '198.00', '200.00', '0.00', '0.00', '0.00 Km', '0.0', '0', '200', 1, '', 1, 1, '0000-00-00'),
(115, 200, '28.4095252', '77.042669', '28.4256805', '77.0377979', 'P-103, Sohna Rd, Parsvnath Greenville, Tatvam Villas, Uppal Southend, Sector 48, Gurugram, Haryana 122004, India', 'Malibu Condominiums, Sohna - Gurgaon Rd, Malibu Town, Sector 47, Gurugram, Haryana 122018, India', '01:18:49 PM', '01:18:52 PM', '01:23:58 PM', '0', '0.00', '60.00', '0', '0.00', '0.00', 26, '160', '80.00', '80.00', '100.00', '0.00', '0.00', '1.75 Miles', '2650.0', '5', '160', 1, '', 1, 0, '0000-00-00'),
(116, 204, '', '', '', '', '', '', '02:39:41 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 27, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(117, 210, '25.3007459', '55.4427154', '25.286754', '55.4481742', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', 'Maliha Rd - Sharjah - United Arab Emirates', '07:02:08 PM', '07:02:21 PM', '07:35:06 PM', '0', '0.00', '64.00', '0.00', '0.00', '0.00', 28, '152.05', '1.52', '150.53', '88.05', '0.00', '0', '6.61 Km', '6410.0', '32', '152.05', 1, '', 1, 0, '0000-00-00'),
(118, 211, '25.3005779', '55.442559', '', '', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '05:47:15 AM', '05:47:35 AM', '', '0', '0', '0', '0.00', '0.00', '0.00', 28, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(119, 212, '', '', '', '', '', '', '05:04:36 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 28, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(120, 213, '25.090031', '55.1493392', '', '', 'Al Sharta St - Dubai - United Arab Emirates', '', '05:09:16 PM', '05:09:55 PM', '', '0', '0', '0', '0.00', '0.00', '0.00', 28, '0', '0', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(121, 214, '', '', '', '', '', '', '', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 26, '0', '500', '0', '', '0.00', '0', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'Your application is in process. You will be notified soon.',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(13, 2, 4, 'Ankit', 'akki@gmail.com', '+928285469066', '', '123456', 'DoRzMhNouQCRqSGs', '1545.6', '0', '', 'eUdLuyRpIM0:APA91bHVzOG3wgSzSaaywXanLVM3rZ4CzKS9c-jCZ82A8iU3OvQWkMeZgLkY9_hWhmbL1RhmK0OCA5Gf6hE0mZUZRmzKmqjxucpSsCxWbzQWiKfwkirNBnU-rNp4MLcvDCFmZPP_sQh8', 2, '1.71428571429', 2, 3, 'chnv', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122175', '77.0432337', '----', '08:20', 'Saturday, Oct 7, 2017', 11, 0, 7, 2, 0, 1, 2, 0, '2017-09-22', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(12, 0, 4, 'testab', 'testab@gmail.com', '+919632698566', '', 'qwerty', 'up2ra40DGiKGhwqA', '0', '0', '', '0B48B0AEFC9FD242959E74E30AB0D4D5A5128F0735718523122C50C32FAE5795', 1, '', 2, 3, '5677', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '01:11', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-09-22', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(11, 0, 5, 'vuhf', 'ghi@gmail.com', '9 688890898', '', 'cyydzvkh', 'Xt5noYa0JmSVr9mU', '0', '0', '', '', 0, '', 3, 2, 'I tried cccv', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(10, 2, 5, 'ggy', 'abc@gmail.com', '868089888', '', 'cyhgvxcg', 'jvkrIPXjUrSexkBF', '0', '0', '', '', 0, '', 3, 31, 'gghv', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(16, 2, 0, 'aladriverDXB', 'noorabbasi74@gmail.com', '+923352550228', '', 'forgotten', 'CnctFFFW2tc47fAn', '480', '0', '', '957942F32ED56DBF01A71F3D6F1E4791828C3BC23FACBDEF2F91363701990CCF', 1, '4.83333333333', 4, 11, '1234567', 120, 'Tuesday, Sep 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.0895845424618', '55.1523580123253', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai , United Arab Emirates', '14:04', 'Thursday, Oct 5, 2017', 6, 1, 0, 1, 0, 1, 2, 0, '2017-09-26', 1, '', '2017-09-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(9, 0, 5, 'anurag', 'a@gmail.com', '8871231856', '', 'asdfghjkl', 'j6MnFM0Do09raIKW', '394.25', '0', '', '0B48B0AEFC9FD242959E74E30AB0D4D5A5128F0735718523122C50C32FAE5795', 1, '3.5', 3, 2, '1234', 56, 'Tuesday, Sep 19', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120850107506', '77.0432677003859', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:34', 'Thursday, Sep 21, 2017', 4, 1, 1, 2, 0, 2, 2, 0, '2017-09-19', 1, '', '2017-09-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(8, 0, 0, 'Ashar', 'Asher@hotmail.com', '0501234567', '', 'forgotten', 'eswhJVJ94Qosky2S', '1005', '0', '', '2BF98F1B1232B4D1C8248F764F4C1C39FC5B158BEB9F02C17E68AF8E3FCE6D7B', 1, '4.5', 4, 29, '44332211', 122, 'Monday, Sep 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3007527507703', '55.4427291898055', 'Muwailih Commercial - Sharjah , United Arab Emirates', '22:15', 'Thursday, Sep 21, 2017', 12, 1, 3, 2, 0, 1, 2, 0, '2017-09-18', 1, '', '2017-09-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(6, 0, 5, 'karan', 'karan@gmail.com', '7988782109', '', 'qwerty', 'lEBD2QZJOiugnq4L', '209', '0', '', '0B48B0AEFC9FD242959E74E30AB0D4D5A5128F0735718523122C50C32FAE5795', 1, '4', 3, 20, '7766', 56, 'Monday, Sep 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121147540664', '77.0431782432922', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:21', 'Friday, Sep 22, 2017', 2, 1, 3, 2, 0, 2, 2, 0, '2017-09-18', 1, '', '2017-09-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(14, 0, 4, 'new new', 'new234@gmail.com', '+927988782109', '', 'qwerty', 'kyMmzn2KkkxkQCtX', '1190.4', '0', '', 'F2D666C0A4115B71366BF259B2DE99ABDDBBAD50FDBEACB72E81F9F103DD652B', 1, '1.36363636364', 2, 3, '5645', 56, 'Monday, Sep 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122863329644', '77.0434439327479', '322B, Sohna Road, Sector 49 , Gurugram, Haryana 122018', '08:04', 'Saturday, Oct 7, 2017', 12, 0, 5, 2, 0, 2, 2, 0, '2017-09-25', 1, '', '2017-09-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(15, 2, 4, 'vhh', 'jkn@gmail.com', '+928282828282', '', '123456', 'CESoOS0Gn0GW6s7h', '0', '0', '', 'ccJUGgyhNXM:APA91bFeW7Ujy0rnso63HBsWIalt-Ah4FCXvVhUI4K4p3gMbVPz_hvUtZyQDa1VpG-jNlleUcSP1fL4ZcLQYjsrNrykUaohDBPiRTdoW5mmboiGNBtjX9v-xpW445gqfWiQYAi646RVI', 2, '', 2, 3, 'xhjj', 56, 'Monday, Sep 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122189', '77.0432347', '----', '01:50', 'Monday, Sep 25, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '2017-09-25', 1, '', '2017-09-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(17, 2, 10, 'rentaldriverDXB', 'alatechnologiespk@gmail.com', '+921234567', '', 'forgotten', 'yjMvLaee4fidHyAD', '360', '0', '', 'f_SxeXXrJiw:APA91bHwFKuG9hvsv_WeYB0WTpkWrN8PYgWeJA6ixFk53pLieMniU9OllhbbCeYQgRwkdntvuy9VDbIhy2HIvb5XXE7FpEzhGouG7-isI4NsBgnfTVsiCMlzlrHSPepC3SbBc4WcHMiY', 2, '3', 3, 20, '987654321', 120, 'Tuesday, Sep 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3006805526328', '55.442559951968', 'Muwailih Commercial - Sharjah , United Arab Emirates', '16:33', 'Tuesday, Oct 3, 2017', 5, 2, 2, 2, 0, 2, 2, 0, '2017-09-26', 1, '', '2017-09-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(19, 2, 1, 'aladriverShj', 'abc@hotmail.com', '+971501122334', '', 'forgotten', 'E99dYGrXiOD6ZyaE', '297', '0', '', 'A8355DAA950850CCB8066C3E094C8DB1CBDE64625B7E39BDA2F06399B183AEBE', 1, '1.5', 4, 11, '987654', 122, 'Tuesday, Sep 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.0896679843078', '55.1525756065316', 'Dubai Internet City - Dubai , United Arab Emirates', '07:06', 'Wednesday, Oct 4, 2017', 3, 1, 2, 2, 0, 2, 2, 0, '2017-09-26', 1, '', '2017-09-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(18, 0, 10, 'Saad', 'saad@gmail.com', '+920331310300', '', '123456', '1FA1RntvBbLRvtxT', '0', '0', '', '', 0, '', 2, 3, '1234', 121, 'Tuesday, Sep 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-09-26', 1, '', '2017-09-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 2),
(20, 2, 12, 'Test ', 'test@demo.com', '23232434', '', '123', '', '0', '0', '', '', 0, '', 2, 3, '2331', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 0, 0, '', 0, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 3),
(21, 2, 12, 'Test ', 'test2@demo.com', '1234232432', '', '(tZhmZyUd)TD9$g%8V', '', '0', '0', '', '', 0, '', 2, 3, '3111', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-10-03', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(22, 2, 1, 'newshj', 'noorabbasishj@gmail.com', '+92501234567', '', 'forgotten', '6yI3dd2KZeOElwVF', '1831.5', '0', '396', 'fe0l8MApUUo:APA91bGVjcWmA984LHvRZYtOdnUpIYkM6wnPH7qcf74qxeSIjLkMTaIyWxqpLHjz876FhEIW0Orijli_5CZ3RJETeu2dSKbMi6dMiubiB2Mac3URONtZAXTWFUo8AR39G4dEAy4j1kNy', 2, '3.83333333333', 3, 20, '5648392', 122, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3007493', '55.4427158', '----', '08:40', 'Saturday, Oct 7, 2017', 9, 0, 3, 2, 0, 1, 2, 0, '2017-10-03', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(23, 2, 1, 'second she', 'secndshj@gmail.com', '+971551234567', '', 'forgotten', 'zt92nDi3vaPH7KGE', '396', '0', '', '847FE2599D335954D269127294DA750E597418ACE906C0233EA9DC15DF0D5B72', 1, '5', 3, 2, 'e34234', 122, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3007538087356', '55.4425515682645', 'Muwailih Commercial - Sharjah , United Arab Emirates', '05:13', 'Tuesday, Oct 3, 2017', 2, 0, 1, 2, 0, 2, 2, 0, '2017-10-03', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(25, 0, 10, 'M Saad', 'msaad.dilshad@gmail.com', '+923333222142', '', '123456', 'PNknzwsGVEsb1cVl', '0', '0', '', 'cvaPN7uFo1Y:APA91bGOXBXwj7rVXUybrp6gKs83eG4vBDaHh9qJoduSk5mDP9KvOwu3a62lDIz_qRUy-r8tE9GrCt5_Nj1aN5IMAWrL87rRUkJsk6U23O_OIrOnNrE--Oysqy1z7_iTo_q3GD34Dfty', 2, '', 2, 3, '123', 121, 'Thursday, Oct 5', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '24.9391674', '67.1492901', '----', '16:19', 'Friday, Oct 6, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '2017-10-05', 1, '', '2017-10-05', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 2),
(24, 2, 6, 'Manish', 'mkyj@gmail.com', '+928285469033', '', '123456', 'QZkSsxoJe9rPUdOG', '1160.9', '0', '', 'ccJUGgyhNXM:APA91bFeW7Ujy0rnso63HBsWIalt-Ah4FCXvVhUI4K4p3gMbVPz_hvUtZyQDa1VpG-jNlleUcSP1fL4ZcLQYjsrNrykUaohDBPiRTdoW5mmboiGNBtjX9v-xpW445gqfWiQYAi646RVI', 2, '3.5', 4, 11, 'xhvc', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122189', '77.0432347', '----', '07:23', 'Thursday, Oct 5, 2017', 5, 2, 4, 1, 0, 1, 2, 0, '2017-10-04', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(26, 2, 50, 'testweb', 'testweb@gmail.com', '+919991537046', '', 'qwerty', 'bdni7VwUVuGjtbOX', '130', '500', '', 'eUdLuyRpIM0:APA91bHVzOG3wgSzSaaywXanLVM3rZ4CzKS9c-jCZ82A8iU3OvQWkMeZgLkY9_hWhmbL1RhmK0OCA5Gf6hE0mZUZRmzKmqjxucpSsCxWbzQWiKfwkirNBnU-rNp4MLcvDCFmZPP_sQh8', 2, '0', 3, 20, '1234', 56, 'Saturday, Oct 7', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123237', '77.0433922', '----', '05:52', 'Saturday, Oct 28, 2017', 2, 0, 1, 2, 0, 1, 2, 0, '2017-10-07', 1, '', '2017-10-07', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your application is in process. You will be notified soon.', 3, 1, 3),
(27, 2, 1, 'Abba', 'abba@gmail.com', '+923333222731', '', 'forgotten', 'tQ42GX5Rm1aopnut', '150.48', '0', '', '212FB6374E0F1CB201503ECC668C6D88097A2619E8D86B6CA3D1B93F9D253718', 1, '', 4, 11, 'forgotten', 122, 'Saturday, Oct 7', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3011221029154', '55.4428512479677', 'Muwailih Commercial - Sharjah , United Arab Emirates', '18:48', 'Saturday, Oct 7, 2017', 1, 2, 8, 1, 0, 1, 2, 0, '2017-10-07', 1, '', '2017-10-07', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your application is in process. You will be notified soon.', 3, 1, 3),
(28, 2, 1, 'Faariz', 'fariz@gmail.com', '+923313103888', '', 'forgotten', '9IGCcquT37sjFAo9', '348.53', '500', '', 'e4KSI89vd3Y:APA91bFYE4sX2e__heBg25gp_oQJMARyec1xiEBzkuS6q0B9MdSKWYAXUm-NBed36BjNKqkKQl4-NKttiYSnGoIQQErVZwOV4Cq23WeZktbsb6YEiXo5233BOi5GR-CuMMW02rez6TPD', 2, '5', 3, 2, 'F121212', 122, 'Saturday, Oct 7', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.0886873', '55.1532126', '----', '09:32', 'Wednesday, Oct 11, 2017', 2, 3, 2, 1, 0, 1, 2, 0, '2017-10-07', 1, '', '2017-10-07', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your application is in process. You will be notified soon.', 3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(12, 8, '100', 3, '100', '0', '2017-09-20'),
(11, 8, '708', 7, '706', '2', '2017-09-19'),
(10, 9, '315', 3, '299.25', '15.75', '2017-09-19'),
(9, 8, '100', 1, '99.00', '1.00', '2017-09-18'),
(8, 7, '80', 1, '80', '0.00', '2017-09-18'),
(7, 6, '100', 1, '95.00', '5.00', '2017-09-18'),
(13, 9, '100', 1, '95.00', '5.00', '2017-09-21'),
(14, 8, '100', 1, '100', '0.00', '2017-09-21'),
(15, 6, '120', 1, '114.00', '6.00', '2017-09-22'),
(16, 16, '160', 2, '160', '0', '2017-09-26'),
(17, 13, '835', 7, '801.6', '33.4', '2017-09-26'),
(18, 17, '400', 5, '360', '40', '2017-09-26'),
(19, 19, '100', 1, '99.00', '1.00', '2017-09-26'),
(20, 19, '100', 1, '99.00', '1.00', '2017-09-27'),
(21, 19, '100', 1, '99.00', '1.00', '2017-10-03'),
(22, 23, '400', 2, '396', '4', '2017-10-03'),
(23, 14, '1240', 12, '1190.4', '49.6', '2017-10-04'),
(24, 13, '330', 3, '316.8', '13.2', '2017-10-04'),
(25, 24, '1235', 5, '1160.9', '74.1', '2017-10-04'),
(26, 16, '240', 3, '240', '0', '2017-10-04'),
(27, 22, '850', 4, '841.5', '8.5', '2017-10-04'),
(28, 22, '1000', 5, '990', '-390', '2017-10-05'),
(29, 16, '80', 1, '80', '0.00', '2017-10-05'),
(30, 13, '445', 1, '427.20', '17.80', '2017-10-07'),
(31, 26, '260', 2, '130', '130', '2017-10-07'),
(32, 27, '152', 1, '150.48', '1.52', '2017-10-07'),
(33, 28, '352.05', 2, '348.53', '3.52', '2017-10-07');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`, `ride_status`) VALUES
(6, 6, 69, 1, 1),
(7, 7, 37, 1, 1),
(8, 8, 106, 1, 1),
(9, 9, 60, 1, 1),
(10, 13, 185, 1, 0),
(11, 14, 185, 1, 0),
(12, 16, 177, 1, 0),
(13, 17, 91, 1, 0),
(14, 19, 104, 1, 0),
(15, 23, 109, 1, 0),
(16, 24, 156, 1, 0),
(17, 22, 181, 1, 0),
(18, 26, 200, 1, 0),
(19, 27, 206, 1, 0),
(20, 28, 213, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 3, 1, 'Monday', '11:35', '11:35', '12:35', '10:35', 1, '23'),
(18, 56, 1, 'Monday', '17:19', '18:20', '17:20', '18:20', 1, '55'),
(22, 56, 2, 'Night', '17:00', '23.45', '', '', 1, '99'),
(32, 112, 1, 'Sunday', '07:30', '09:30', '14:30', '18:0', 2, '1.2'),
(33, 112, 2, 'Night', '23:30', '06:30', '', '', 2, '1.5'),
(34, 3, 1, 'Wednesday', '14:28', '23:45', '14:29', '23:59', 1, '10'),
(37, 56, 1, 'Sunday', '08:30', '09:30', '10:30', '11:30', 2, '23'),
(38, 56, 1, 'Saturday', '12:31', '12:31', '12:31', '12:31', 2, '20'),
(39, 79, 1, 'Monday', '16:19', '20:19', '22:19', '01:19', 2, '10'),
(40, 56, 1, 'Thursday', '22:30', '23:45', '23:45', '23:45', 1, '100'),
(41, 56, 1, 'Tuesday', '09:13 PM', '10:14 PM', '10:14 PM', '11:14 PM', 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(30, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', '', '10:10:05', '10:10:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(29, 7, '', '28.4120678971421', '77.0432692926945', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 25', '14:37:19', '02:37:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120678971421,77.0432692926945&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(28, 7, '', '28.4120678971421', '77.0432692926945', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 25', '14:37:12', '02:37:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120678971421,77.0432692926945&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(27, 10, '', '28.592756929959013', '77.31003530323505', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '28.459496499999997', '77.0266383', 'Gurugram', 'Saturday, Sep 23', '09:26:17', '09:26:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.592756929959013,77.31003530323505&markers=color:red|label:D|28.459496499999997,77.0266383&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(26, 10, '', '28.592756929959013', '77.31003530323505', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '', '', 'Set your drop point', 'Saturday, Sep 23', '09:25:58', '09:25:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.592756929959013,77.31003530323505&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(25, 10, '', '28.592671263301867', '77.30998165905476', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '', '', 'Set your drop point', 'Saturday, Sep 23', '09:10:00', '09:10:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.592671263301867,77.30998165905476&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(24, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:26:02', '04:26:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(23, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:24:07', '04:24:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(22, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:23:38', '04:23:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(21, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:20:24', '04:20:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(20, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:19:43', '04:19:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(19, 8, 'SHILPA', '25.0897023724457', '55.1523378118873', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:12:58', '04:12:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0897023724457,55.1523378118873&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(18, 8, 'SHILPA', '25.0897023724457', '55.1523378118873', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:12:52', '04:12:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0897023724457,55.1523378118873&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(16, 8, '', '24.815438724124', '67.4015146493912', 'Eastern Zone\nKarachi, Pakistan', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 19', '07:51:15', '07:51:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|24.815438724124,67.4015146493912&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(17, 8, '', '25.1019333377634', '55.1677816361189', 'Abdullah Omran Taryam Street - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Wednesday, Sep 20', '12:24:15', '12:24:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.1019333377634,55.1677816361189&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(31, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:20:07', '10:20:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(32, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', '', '10:20:17', '10:20:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(33, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', '', '10:21:11', '10:21:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(34, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:21:21', '10:21:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(35, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:31:40', '10:31:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(36, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', '', '12:23:39', '12:23:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(37, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Tuesday, Sep 26', '12:52:18', '12:52:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(38, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', '', '12:53:02', '12:53:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(39, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', '', '13:46:42', '01:46:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(40, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', '', '07:11:21', '07:11:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.08841462352676,55.153589732944965&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(41, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Wednesday, Sep 27', '07:11:29', '07:11:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.08841462352676,55.153589732944965&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(42, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Wednesday, Sep 27', '09:30:26', '09:30:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.08841462352676,55.153589732944965&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(43, 14, '', '25.3013608330319', '55.4434058539977', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Oct 3', '16:53:25', '04:53:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3013608330319,55.4434058539977&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(44, 14, '', '25.3013608330319', '55.4434058539977', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Oct 3', '16:54:30', '04:54:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3013608330319,55.4434058539977&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(45, 14, '', '25.3029774052688', '55.4437440386229', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Tuesday, Oct 3', '16:59:03', '04:59:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3029774052688,55.4437440386229&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(46, 14, '', '25.3009980394806', '55.4428398236632', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', '', '06:55:11', '06:55:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3009980394806,55.4428398236632&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(47, 14, '', '25.3009980394806', '55.4428398236632', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', '', '06:55:41', '06:55:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3009980394806,55.4428398236632&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(48, 14, '', '25.3009980394806', '55.4428398236632', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Wednesday, Oct 4', '06:55:52', '06:55:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3009980394806,55.4428398236632&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(49, 14, '', '25.3009980394806', '55.4428398236632', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Wednesday, Oct 4', '07:07:36', '07:07:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3009980394806,55.4428398236632&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(50, 14, '', '25.3009980394806', '55.4428398236632', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', '', '07:07:44', '07:07:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3009980394806,55.4428398236632&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(51, 7, '', '28.4120967026013', '77.043273858726', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', '', '07:30:49', '07:30:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4120967026013,77.043273858726&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(52, 7, '', '28.4120967026013', '77.043273858726', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', '', '07:31:04', '07:31:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4120967026013,77.043273858726&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(53, 7, '', '28.4122895614617', '77.0433553308249', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '12:00:18', '12:00:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122895614617,77.0433553308249&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 1, 1, 1, 0, 1, 0, 1),
(54, 14, '', '25.0919760163003', '55.1539930701256', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Oct 4', '15:10:52', '03:10:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0919760163003,55.1539930701256&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(55, 14, '', '25.0900111751119', '55.1527508720756', 'Dubai Internet City - Dubai\nUnited Arab Emirates', '25.2986037120294', '55.4415185004473', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', 'Wednesday, Oct 4', '15:47:58', '03:47:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0900111751119,55.1527508720756&markers=color:red|label:D|25.2986037120294,55.4415185004473&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(56, 17, '', '25.088515737054028', '55.15343885868787', 'Al Naseem St - Dubai - United Arab Emirates', '25.30277821962582', '55.44526521116495', 'Unnamed Road - Sharjah - United Arab Emirates', 'Thursday, Oct 5', '21:12:05', '09:12:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088515737054028,55.15343885868787&markers=color:red|label:D|25.30277821962582,55.44526521116495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(57, 17, '', '25.3006052020636', '55.4425079002976', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Friday, Oct 6', '16:59:12', '04:59:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3006052020636,55.4425079002976&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(58, 17, '', '25.3006052020636', '55.4425079002976', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Friday, Oct 6', '17:00:12', '05:00:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3006052020636,55.4425079002976&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(59, 17, '', '25.3006052020636', '55.4425079002976', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', '', '17:00:31', '05:00:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3006052020636,55.4425079002976&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(60, 17, '', '25.3006052020636', '55.4425079002976', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', '', '17:00:39', '05:00:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3006052020636,55.4425079002976&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(61, 17, '', '25.3003210108963', '55.4422585387299', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Friday, Oct 6', '17:25:08', '05:25:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3003210108963,55.4422585387299&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(62, 17, '', '25.3003210108963', '55.4422585387299', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', '', '17:25:21', '05:25:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3003210108963,55.4422585387299&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(63, 17, '', '25.3003210108963', '55.4422585387299', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Friday, Oct 6', '17:25:41', '05:25:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3003210108963,55.4422585387299&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(64, 19, '', '25.3007919212416', '55.4426852613688', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.3136721220319', '55.455112606287', 'University City Road - Sharjah\nUnited Arab Emirates', '', '11:07:22', '11:07:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3007919212416,55.4426852613688&markers=color:red|label:D|25.3136721220319,55.455112606287&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(65, 19, '', '25.3009083174667', '55.443092957139', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '25.3136721220319', '55.455112606287', 'University City Road - Sharjah\nUnited Arab Emirates', '', '11:08:12', '11:08:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.3009083174667,55.443092957139&markers=color:red|label:D|25.3136721220319,55.455112606287&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(66, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.406582924896647', '55.40044851601124', '8 Ø´Ø§Ø±Ø¹ Ø³Ø¹Ø¯ Ø¨Ù† Ù…Ø¹Ø§Ø° - Ajman - United Arab Emirates', '', '13:03:28', '01:03:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|25.406582924896647,55.40044851601124&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(67, 19, '', '25.0889335506231', '55.1502118259668', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.3009483', '55.4526995', 'Muwailih Commercial', '', '16:57:45', '04:57:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.0889335506231,55.1502118259668&markers=color:red|label:D|25.3009483,55.4526995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(68, 19, '', '25.0889335506231', '55.1502118259668', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.3009483', '55.4526995', 'Muwailih Commercial', '', '17:00:47', '05:00:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.0889335506231,55.1502118259668&markers=color:red|label:D|25.3009483,55.4526995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(69, 19, '', '25.0889335506231', '55.1502118259668', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.3009483', '55.4526995', 'Muwailih Commercial', '', '17:02:42', '05:02:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.0889335506231,55.1502118259668&markers=color:red|label:D|25.3009483,55.4526995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" 0px;=\"\" sans\";=\"\" open=\"\" initial;\"=\"\" initial;=\"\" 255);=\"\" 255,=\"\" rgb(255,=\"\" none;=\"\" start;=\"\" 2;=\"\" normal;=\"\" 14px;=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" !important;=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\" normal;=\"\" sans\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img width=\"232\" height=\"163\" class=\"alignnone size-full wp-image-434\" alt=\"iso_apporio\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_date`, `payment_status`) VALUES
(1, 1, 2, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '0000-00-00', '1'),
(2, 2, 2, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '0000-00-00', '1'),
(3, 5, 4, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '0000-00-00', '1'),
(4, 4, 3, '1', 'Cash', 'Admin', '115', 'Thursday, Sep 14', '0000-00-00', '1'),
(5, 6, 4, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '0000-00-00', '1'),
(6, 7, 4, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '0000-00-00', '1'),
(7, 8, 4, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '0000-00-00', '1'),
(8, 9, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(9, 10, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(10, 10, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(11, 11, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(12, 12, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(13, 13, 3, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(14, 14, 3, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(15, 15, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(16, 15, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(17, 16, 3, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(18, 17, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(19, 18, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(20, 19, 4, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '0000-00-00', '1'),
(21, 20, 4, '1', 'Cash', 'Admin', '178', 'Friday, Sep 15', '0000-00-00', '1'),
(22, 21, 4, '1', 'Cash', 'Admin', '178', 'Friday, Sep 15', '0000-00-00', '1'),
(23, 22, 4, '1', 'Cash', 'Admin', '178', 'Friday, Sep 15', '0000-00-00', '1'),
(24, 23, 4, '1', 'Cash', 'Admin', '178', 'Friday, Sep 15', '0000-00-00', '1'),
(25, 24, 6, '1', 'Cash', 'Admin', '277', 'Friday, Sep 15', '0000-00-00', '1'),
(26, 25, 6, '1', 'Cash', 'Admin', '347', 'Friday, Sep 15', '0000-00-00', '1'),
(27, 26, 6, '1', 'Cash', 'Admin', '277', 'Friday, Sep 15', '0000-00-00', '1'),
(28, 27, 6, '4', 'Cash', 'Android', '277', 'Anything', '0000-00-00', 'Anything'),
(29, 28, 7, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '0000-00-00', '1'),
(30, 29, 8, '1', 'Cash', 'Admin', '80', 'Monday, Sep 18', '0000-00-00', '1'),
(31, 30, 8, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '0000-00-00', '1'),
(32, 31, 9, '1', 'Cash', 'Admin', '115', 'Tuesday, Sep 19', '0000-00-00', '1'),
(33, 32, 9, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(34, 33, 9, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(35, 34, 8, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(36, 35, 8, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(37, 36, 8, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(38, 37, 8, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(39, 38, 8, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(40, 39, 8, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '0000-00-00', '1'),
(41, 40, 8, '1', 'Cash', 'Admin', '108', 'Tuesday, Sep 19', '0000-00-00', '1'),
(42, 45, 8, '1', 'Cash', 'Admin', '100', 'Wednesday, Sep 20', '0000-00-00', '1'),
(43, 46, 8, '1', 'Cash', 'Admin', '0.00', 'Wednesday, Sep 20', '0000-00-00', '1'),
(44, 47, 8, '1', 'Cash', 'Admin', '0.00', 'Wednesday, Sep 20', '0000-00-00', '1'),
(45, 48, 9, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '0000-00-00', '1'),
(46, 49, 8, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '0000-00-00', '1'),
(47, 50, 9, '1', 'Cash', 'Admin', '120', 'Friday, Sep 22', '0000-00-00', '1'),
(48, 52, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(49, 53, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(50, 51, 10, '1', 'Cash', 'Admin', '235', 'Tuesday, Sep 26', '0000-00-00', '1'),
(51, 54, 10, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(52, 54, 10, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(53, 55, 10, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(54, 55, 10, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(55, 55, 10, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(56, 56, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(57, 57, 10, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(58, 58, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(59, 59, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(60, 60, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(61, 61, 14, '1', 'Cash', 'Admin', '80', 'Tuesday, Sep 26', '0000-00-00', '1'),
(62, 62, 14, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '0000-00-00', '1'),
(63, 63, 14, '1', 'Cash', 'Admin', '100', 'Wednesday, Sep 27', '0000-00-00', '1'),
(64, 64, 14, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 3', '0000-00-00', '1'),
(65, 65, 14, '1', 'Cash', 'Admin', '200', 'Tuesday, Oct 3', '0000-00-00', '1'),
(66, 66, 14, '1', 'Cash', 'Admin', '200', 'Tuesday, Oct 3', '0000-00-00', '1'),
(67, 67, 7, '1', 'Cash', 'Admin', '130', 'Wednesday, Oct 4', '0000-00-00', '1'),
(68, 68, 10, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(69, 69, 10, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(70, 70, 10, '1', 'Cash', 'Admin', '130', 'Wednesday, Oct 4', '0000-00-00', '1'),
(71, 72, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(72, 73, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(73, 74, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(74, 75, 10, '1', 'Cash', 'Admin', '478', 'Wednesday, Oct 4', '0000-00-00', '1'),
(75, 76, 10, '1', 'Cash', 'Admin', '223', 'Wednesday, Oct 4', '0000-00-00', '1'),
(76, 77, 7, '1', 'Cash', 'Admin', '178', 'Wednesday, Oct 4', '0000-00-00', '1'),
(77, 78, 10, '1', 'Cash', 'Admin', '110', 'Wednesday, Oct 4', '0000-00-00', '1'),
(78, 79, 10, '1', 'Cash', 'Admin', '178', 'Wednesday, Oct 4', '0000-00-00', '1'),
(79, 80, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(80, 81, 10, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(81, 82, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(82, 83, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(83, 84, 10, '1', 'Cash', 'Admin', '178', 'Wednesday, Oct 4', '0000-00-00', '1'),
(84, 85, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(85, 86, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(86, 87, 7, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '0000-00-00', '1'),
(87, 88, 14, '1', 'Cash', 'Admin', '80', 'Wednesday, Oct 4', '0000-00-00', '1'),
(88, 89, 14, '1', 'Cash', 'Admin', '80', 'Wednesday, Oct 4', '0000-00-00', '1'),
(89, 90, 14, '1', 'Cash', 'Admin', '80', 'Wednesday, Oct 4', '0000-00-00', '1'),
(90, 91, 14, '1', 'Cash', 'Admin', '200', 'Wednesday, Oct 4', '0000-00-00', '1'),
(91, 92, 14, '1', 'Cash', 'Admin', '200', 'Wednesday, Oct 4', '0000-00-00', '1'),
(92, 93, 14, '1', 'Cash', 'Admin', '250', 'Wednesday, Oct 4', '0000-00-00', '1'),
(93, 96, 14, '1', 'Cash', 'Admin', '200', 'Wednesday, Oct 4', '0000-00-00', '1'),
(94, 98, 14, '1', 'Cash', 'Admin', '200', 'Thursday, Oct 5', '0000-00-00', '1'),
(95, 99, 17, '1', 'Cash', 'Admin', '200', 'Thursday, Oct 5', '0000-00-00', '1'),
(96, 100, 17, '1', 'Cash', 'Admin', '200', 'Thursday, Oct 5', '0000-00-00', '1'),
(97, 101, 14, '1', 'Cash', 'Admin', '80', 'Thursday, Oct 5', '0000-00-00', '1'),
(98, 102, 17, '3', 'Credit Card', 'Stripe', '200', 'Thursday, Oct 5', '0000-00-00', 'Payment complete.'),
(99, 103, 17, '3', 'Credit Card', 'Stripe', '200', 'Thursday, Oct 5', '0000-00-00', 'Payment complete.'),
(100, 105, 10, '1', 'Cash', 'Admin', '445', 'Saturday, Oct 7', '0000-00-00', '1'),
(101, 108, 18, '1', 'Cash', 'Admin', '100', 'Saturday, Oct 7', '0000-00-00', '1'),
(102, 110, 20, '1', 'Cash', 'Admin', '152', 'Saturday, Oct 7', '0000-00-00', '1'),
(103, 114, 20, '1', 'Cash', 'Admin', '200', 'Saturday, Oct 7', '0000-00-00', '1'),
(104, 115, 10, '1', 'Cash', 'Admin', '160', 'Saturday, Oct 7', '0000-00-00', '1'),
(105, 117, 21, '1', 'Cash', 'Admin', '152.05', 'Saturday, Oct 7', '0000-00-00', '1'),
(106, 119, 19, '1', 'Cancel Charges', 'Ride Cancel', '500', 'Monday, Oct 9', '2017-10-09', '1'),
(107, 121, 10, '1', 'Cancel Charges', 'Ride Cancel', '500', 'Tuesday, Oct 10', '2017-10-10', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(12, 56, 'Miles', '&#8377', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '&#8377', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '&#8377', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(43, 120, 'Km', '$', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(44, 120, 'Km', '$', 3, 10, 0, 0, 0, 0, 0, 0, '4', '80', '10', '10', '2', '10', '3'),
(45, 120, 'Km', '$', 4, 10, 0, 0, 0, 0, 0, 0, '3', '80', '10', '0', '0', '0', '0'),
(46, 121, 'Km', '$', 2, 0, 0, 0, 0, 0, 0, 0, '1', '80', '5.5', '1', '2', '1', '2'),
(47, 121, 'Km', '$', 3, 10, 0, 0, 0, 0, 0, 0, '5', '200', '10', '0', '0', '0', '0'),
(48, 122, 'Km', '$', 4, 1, 0, 0, 0, 0, 0, 0, '5', '80', '5', '0', '2', '0', '2'),
(49, 122, 'Km', '$', 3, 1, 0, 0, 0, 0, 0, 0, '5', '80', '5', '0', '2', '0', '2');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'Hi', 'Log off', 'uploads/notification/1500380956793.jpg', '', 0, 19, '2017-10-03', 2),
(2, 'Hi', 'Pljgfvjk jjg', 'uploads/notification/1500380956793.jpg', '', 14, 0, '2017-10-03', 1),
(3, 'xcvxcv', 'xcvcxcvxcv', 'uploads/notification/1500380956793.jpg', '', 0, 8, '2017-10-03', 2),
(4, 'Hi  there', 'Pls log off from this device', 'uploads/notification/1500380956793.jpg', '', 0, 8, '2017-10-03', 2),
(5, 'hi ', 'this msg is only for sharjah customer named alauser so why is it going to all customers', 'uploads/notification/1500380956793.jpg', '', 14, 0, '2017-10-03', 1),
(6, 'hi ', 'did u get this', 'uploads/notification/1500380956793.jpg', '', 10, 0, '2017-10-03', 1),
(7, 'Hi How are you', 'Hi How are you', 'uploads/notification/1500380956793.jpg', '', 7, 0, '2017-10-04', 1),
(8, 'sjdsjdsa', 'askjsdsadksa', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-10-04', 1),
(9, 'Hi  there', 'kjhkjhlk.07/10/2017', 'uploads/notification/1500380956793.jpg', '', 0, 27, '2017-10-07', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_booking`
--

INSERT INTO `rental_booking` (`rental_booking_id`, `user_id`, `rentcard_id`, `payment_option_id`, `coupan_code`, `car_type_id`, `booking_type`, `driver_id`, `pickup_lat`, `pickup_long`, `pickup_location`, `start_meter_reading`, `start_meter_reading_image`, `end_meter_reading`, `end_meter_reading_image`, `booking_date`, `booking_time`, `user_booking_date_time`, `last_update_time`, `booking_status`, `payment_status`, `pem_file`, `booking_admin_status`) VALUES
(1, 8, 2, 1, '', 4, 1, 0, '25.0896710974026', '55.1522295176983', '???? ????? - Dubai\nUnited Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 19', '01:36 PM', 'Tuesday, Sep 19, 01:36 PM', '01:36:51 PM', 10, 0, 2, 1),
(2, 8, 2, 1, '', 4, 1, 0, '25.3006664710355', '55.4424804915259', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0', '', '0', '', 'Wednesday, Sep 20', '01:03 AM', 'Wednesday, Sep 20, 01:03 AM', '10:54:10 AM', 19, 0, 2, 1),
(3, 14, 3, 1, '', 4, 1, 0, '25.08465605603049', '55.16110360622407', 'Unnamed Road - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '01:44 PM', 'Tuesday, Sep 26, 01:44 PM', '01:44:51 PM', 15, 0, 1, 1),
(4, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '01:52 PM', 'Tuesday, Sep 26, 01:52 PM', '01:52:47 PM', 15, 0, 1, 1),
(5, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '01:55 PM', 'Tuesday, Sep 26, 01:55 PM', '01:56:08 PM', 15, 0, 1, 1),
(6, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '04:53 PM', 'Tuesday, Sep 26, 04:53 PM', '04:53:33 PM', 15, 0, 1, 1),
(7, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '04:54 PM', 'Tuesday, Sep 26, 04:54 PM', '04:55:14 PM', 15, 0, 1, 1),
(8, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '04:57 PM', 'Tuesday, Sep 26, 04:57 PM', '04:57:23 PM', 15, 0, 1, 1),
(9, 14, 1, 1, '', 3, 1, 0, '25.089042862077306', '55.15322998166084', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '05:20 PM', 'Tuesday, Sep 26, 05:20 PM', '05:20:42 PM', 15, 0, 1, 1),
(10, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '05:24 PM', 'Tuesday, Sep 26, 05:24 PM', '05:24:48 PM', 15, 0, 1, 1),
(11, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '05:27 PM', 'Tuesday, Sep 26, 05:27 PM', '05:27:49 PM', 15, 0, 1, 1),
(12, 14, 1, 1, '', 3, 1, 0, '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Tuesday, Sep 26', '06:17 PM', 'Tuesday, Sep 26, 06:17 PM', '06:17:29 PM', 15, 0, 1, 1),
(13, 14, 2, 1, '', 4, 1, 0, '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '0', '', '0', '', 'Wednesday, Sep 27', '12:37 AM', 'Wednesday, Sep 27, 12:37 AM', '12:37:40 AM', 15, 0, 1, 1),
(14, 14, 3, 1, '', 4, 1, 0, '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Wednesday, Sep 27', '02:25 PM', 'Wednesday, Sep 27, 02:25 PM', '02:25:55 PM', 15, 0, 1, 1),
(15, 14, 1, 1, '', 3, 1, 0, '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '0', '', '0', '', 'Wednesday, Sep 27', '02:28 PM', 'Wednesday, Sep 27, 02:28 PM', '02:28:14 PM', 15, 0, 1, 1),
(16, 14, 2, 1, '', 4, 1, 0, '25.3034896175237', '55.4440303891897', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0', '', '0', '', 'Tuesday, Oct 3', '08:37 PM', 'Tuesday, Oct 3, 08:37 PM', '08:37:56 PM', 10, 0, 2, 1),
(17, 14, 2, 1, '', 4, 1, 0, '25.3015042401391', '55.4437360167503', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '0', '', '0', '', 'Tuesday, Oct 3', '09:49 PM', 'Tuesday, Oct 3, 09:49 PM', '09:49:37 PM', 10, 0, 2, 1),
(18, 14, 2, 1, '', 4, 1, 0, '25.3015042401391', '55.4437360167503', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '0', '', '0', '', 'Tuesday, Oct 3', '09:51 PM', 'Tuesday, Oct 3, 09:51 PM', '09:51:09 PM', 10, 0, 2, 1),
(19, 7, 5, 1, '', 2, 1, 0, '28.4121054708446', '77.0432563719824', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '10:52 AM', 'Wednesday, Oct 4, 10:52 AM', '10:52:21 AM', 10, 0, 2, 1),
(20, 7, 5, 1, '', 2, 1, 14, '28.4120707521468', '77.0432376489043', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '21', 'http://apporio.org/alaride/alaride/uploads/1507109894783.jpg', '28', 'http://apporio.org/alaride/alaride/uploads/1507109908192.jpg', 'Wednesday, Oct 4', '03:07 PM', 'Wednesday, Oct 4, 03:07 PM', '03:08:33 PM', 16, 1, 2, 1),
(21, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323429614305', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '20', 'http://apporio.org/alaride/alaride/uploads/1507112488501.jpg', '50', 'http://apporio.org/alaride/alaride/uploads/1507112500769.jpg', 'Wednesday, Oct 4', '03:50 PM', 'Wednesday, Oct 4, 03:50 PM', '03:51:43 PM', 16, 1, 1, 1),
(22, 10, 5, 1, '', 2, 1, 14, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '22', 'http://apporio.org/alaride/alaride/uploads/swift_file66.jpeg', '28', 'http://apporio.org/alaride/alaride/uploads/swift_file67.jpeg', 'Wednesday, Oct 4', '03:53 PM', 'Wednesday, Oct 4, 03:53 PM', '03:54:42 PM', 16, 1, 1, 1),
(23, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', 'Wednesday, Oct 4', '03:57 PM', 'Wednesday, Oct 4, 03:57 PM', '03:58:04 PM', 15, 0, 1, 1),
(24, 10, 5, 1, '', 2, 1, 14, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '25', 'http://apporio.org/alaride/alaride/uploads/swift_file68.jpeg', '30', 'http://apporio.org/alaride/alaride/uploads/swift_file69.jpeg', 'Wednesday, Oct 4', '04:00 PM', 'Wednesday, Oct 4, 04:00 PM', '04:01:35 PM', 16, 1, 1, 1),
(25, 7, 6, 1, '', 4, 1, 24, '28.4122095066001', '77.0433480693962', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:04 PM', 'Wednesday, Oct 4, 04:04 PM', '04:04:39 PM', 15, 0, 2, 1),
(26, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '5', 'http://apporio.org/alaride/alaride/uploads/1507113439516.jpg', '23', 'http://apporio.org/alaride/alaride/uploads/1507113452626.jpg', 'Wednesday, Oct 4', '04:06 PM', 'Wednesday, Oct 4, 04:06 PM', '04:07:37 PM', 16, 1, 1, 1),
(27, 7, 5, 1, '', 2, 1, 0, '28.4120824279698', '77.0432663774517', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:09 PM', 'Wednesday, Oct 4, 04:09 PM', '04:09:43 PM', 10, 0, 2, 1),
(28, 7, 5, 1, '', 2, 1, 14, '28.4120872660731', '77.0433033630252', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '20', 'http://apporio.org/alaride/alaride/uploads/swift_file70.jpeg', '50', 'http://apporio.org/alaride/alaride/uploads/swift_file71.jpeg', 'Wednesday, Oct 4', '04:11 PM', 'Wednesday, Oct 4, 04:11 PM', '04:12:22 PM', 16, 1, 2, 1),
(29, 7, 5, 1, '', 2, 1, 0, '28.4121048211545', '77.0432868903396', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:14 PM', 'Wednesday, Oct 4, 04:14 PM', '04:14:00 PM', 10, 0, 2, 1),
(30, 7, 5, 1, '', 2, 1, 14, '28.41220983182', '77.0431937124131', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:16 PM', 'Wednesday, Oct 4, 04:16 PM', '04:16:52 PM', 18, 0, 2, 1),
(31, 7, 5, 1, '', 2, 1, 0, '28.4121766808091', '77.0432604829949', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:17 PM', 'Wednesday, Oct 4, 04:17 PM', '04:17:39 PM', 14, 0, 2, 1),
(32, 7, 5, 1, '', 2, 1, 14, '28.4121179429205', '77.0433042481261', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:19 PM', 'Wednesday, Oct 4, 04:19 PM', '04:20:17 PM', 18, 0, 2, 1),
(33, 7, 5, 1, '', 2, 1, 14, '28.4122547700454', '77.0433343105636', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:21 PM', 'Wednesday, Oct 4, 04:21 PM', '04:21:28 PM', 11, 0, 2, 1),
(34, 7, 5, 1, '', 2, 1, 14, '28.4122054628973', '77.0433322093556', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:22 PM', 'Wednesday, Oct 4, 04:22 PM', '04:22:53 PM', 15, 0, 2, 1),
(35, 7, 5, 1, '', 2, 1, 14, '28.4121984555011', '77.0431822089946', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:23 PM', 'Wednesday, Oct 4, 04:23 PM', '04:28:40 PM', 18, 0, 2, 1),
(36, 7, 5, 1, '', 2, 1, 0, '28.412829063795', '77.0436935763285', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:27 PM', 'Wednesday, Oct 4, 04:27 PM', '04:27:37 PM', 10, 0, 2, 1),
(37, 7, 5, 1, '', 2, 1, 0, '28.412246424855', '77.0433237434609', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:28 PM', 'Wednesday, Oct 4, 04:28 PM', '04:28:26 PM', 10, 0, 2, 1),
(38, 7, 5, 1, '', 2, 1, 0, '28.4122504826634', '77.0432975431209', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:29 PM', 'Wednesday, Oct 4, 04:29 PM', '04:29:34 PM', 10, 0, 2, 1),
(39, 7, 5, 1, '', 2, 1, 14, '28.4122895614617', '77.0433553308249', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:30 PM', 'Wednesday, Oct 4, 04:30 PM', '04:31:37 PM', 18, 0, 2, 1),
(40, 7, 5, 1, '', 2, 1, 14, '28.4120929259536', '77.0432911428666', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:31 PM', 'Wednesday, Oct 4, 04:31 PM', '04:32:14 PM', 18, 0, 2, 1),
(41, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', 'Wednesday, Oct 4', '04:33 PM', 'Wednesday, Oct 4, 04:33 PM', '04:33:37 PM', 18, 0, 1, 1),
(42, 7, 5, 1, '', 2, 1, 0, '28.412106775666', '77.0432500183686', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0', '', '0', '', 'Wednesday, Oct 4', '04:33 PM', 'Wednesday, Oct 4, 04:33 PM', '04:34:12 PM', 14, 0, 2, 1),
(43, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '9', 'http://apporio.org/alaride/alaride/uploads/1507115106235.jpg', '23', 'http://apporio.org/alaride/alaride/uploads/1507115124794.jpg', 'Wednesday, Oct 4', '04:34 PM', 'Wednesday, Oct 4, 04:34 PM', '04:35:28 PM', 16, 1, 1, 1),
(44, 7, 5, 1, '', 2, 1, 14, '28.4120681813892', '77.0431962017336', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '21', 'http://apporio.org/alaride/alaride/uploads/swift_file72.jpeg', '36', 'http://apporio.org/alaride/alaride/uploads/swift_file73.jpeg', 'Wednesday, Oct 4', '04:44 PM', 'Wednesday, Oct 4, 04:44 PM', '04:45:24 PM', 16, 1, 2, 1),
(45, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '96', 'http://apporio.org/alaride/alaride/uploads/1507116333291.jpg', '99', 'http://apporio.org/alaride/alaride/uploads/1507116351571.jpg', 'Wednesday, Oct 4', '04:55 PM', 'Wednesday, Oct 4, 04:55 PM', '04:55:59 PM', 16, 1, 1, 1),
(46, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '23', 'http://apporio.org/alaride/alaride/uploads/1507116455599.jpg', '29', 'http://apporio.org/alaride/alaride/uploads/1507116467325.jpg', 'Wednesday, Oct 4', '04:57 PM', 'Wednesday, Oct 4, 04:57 PM', '04:57:52 PM', 16, 1, 1, 1),
(47, 10, 6, 1, '', 4, 1, 24, '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '20', 'http://apporio.org/alaride/alaride/uploads/1507116548895.jpg', '50', 'http://apporio.org/alaride/alaride/uploads/1507116565388.jpg', 'Wednesday, Oct 4', '04:58 PM', 'Wednesday, Oct 4, 04:58 PM', '04:59:28 PM', 16, 1, 1, 1),
(48, 14, 4, 1, '', 3, 1, 22, '25.3006645981014', '55.4426404489871', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '123456', 'http://apporio.org/alaride/alaride/uploads/1507147607931.jpg', '258965', 'http://apporio.org/alaride/alaride/uploads/1507147643185.jpg', 'Thursday, Oct 5', '01:36 AM', 'Thursday, Oct 5, 01:36 AM', '01:38:22 AM', 16, 1, 2, 1),
(49, 10, 6, 1, '', 4, 1, 0, '28.41221701826183', '77.04323261976242', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', 'Saturday, Oct 7', '01:04 PM', 'Saturday, Oct 7, 01:04 PM', '01:05:23 PM', 15, 0, 1, 1),
(50, 10, 6, 1, '', 4, 2, 0, '28.41221701826183', '77.04323295503855', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', '8-OCTOBER-2017', '13:5', 'Saturday, Oct 7, 01:06 PM', '01:06:08 PM', 10, 0, 1, 1),
(51, 20, 2, 1, '', 4, 1, 27, '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '0', '', '0', '', 'Saturday, Oct 7', '04:56 PM', 'Saturday, Oct 7, 04:56 PM', '04:56:41 PM', 15, 0, 1, 1),
(52, 20, 2, 1, '', 4, 1, 27, '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '0', '', '0', '', 'Saturday, Oct 7', '04:57 PM', 'Saturday, Oct 7, 04:57 PM', '04:58:24 PM', 18, 0, 1, 1),
(53, 20, 2, 1, '', 4, 1, 27, '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '0', '', '0', '', 'Saturday, Oct 7', '05:04 PM', 'Saturday, Oct 7, 05:04 PM', '05:04:31 PM', 15, 0, 1, 1),
(54, 20, 2, 1, '', 4, 1, 27, '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '0', '', '0', '', 'Saturday, Oct 7', '05:04 PM', 'Saturday, Oct 7, 05:04 PM', '06:33:45 PM', 18, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_category`
--

INSERT INTO `rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_distance_unit`, `rental_category_description`, `rental_category_admin_status`) VALUES
(1, 'DubaiRental', '5', '50', 'Km', '<p>This package is only for dubai. The package can be availed by any one.</p><p>This package is only for dubai. The package can be availed by any one.</p><p>This package is only for dubai. The package can be availed by any one.</p><p>This package is only for dubai. The package can be availed by any one.</p><p>This package is only for dubai. The package can be availed by any one.</p><p>This package is only for dubai. The package can be availed by any one.<br></p>', 1),
(2, 'Sharjah Package', '10', '500', 'Km', '<p>This is sharjah package. This is sharjah package.</p><p>This is sharjah package.This is sharjah package.</p><p>This is sharjah package.This is sharjah package.</p><p>This is sharjah package.This is sharjah package.</p><p>This is sharjah package.This is sharjah package.</p><p>This is sharjah package.This is sharjah package.<br></p>', 1),
(3, 'mini', '10', '100', 'Km', '<p>Mini is the new package this package is very economical and it is used with packages.<span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span></p><p><span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span><span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span><span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span></p><p><span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span><span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span><span style=\"font-size: 14px;\">Mini is the new package this package is very economical and it is used with packages.</span><span style=\"font-size: 14px;\"><br></span><span style=\"font-size: 14px;\"><br></span></p>', 1),
(4, 'luxuryinShj', '2', '20', 'Km', '<p>a luxury ride awaits you.&nbsp;<span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.</span></p><p><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.</span></p><p><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.&nbsp;</span><span style=\"font-size: 14px;\">a luxury ride awaits you.</span><span style=\"font-size: 14px;\"><br></span><span style=\"font-size: 14px;\"><br></span><span style=\"font-size: 14px;\"><br></span></p>', 1),
(5, 'ALARIDE', '5', '100', 'Miles', 'SDKJSJKDSKDSKDKSDKDJKWJDWJDWJLDJWLEJLWOEWOEWQDWQWEWWW', 1),
(6, 'ALARIDE1', '1', '20', 'Miles', 'SDNSDSDNKSDNKSNDKSNKDSKDSDSD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rental_payment`
--

INSERT INTO `rental_payment` (`rental_payment_id`, `rental_booking_id`, `amount_paid`, `payment_status`) VALUES
(1, 20, '800', 'Done'),
(2, 21, '300', 'Done'),
(3, 22, '800', 'Done'),
(4, 24, '800', 'Done'),
(5, 26, '100', 'Done'),
(6, 28, '800', 'Done'),
(7, 43, '100', 'Done'),
(8, 44, '800', 'Done'),
(9, 45, '100', 'Done'),
(10, 46, '100', 'Done'),
(11, 47, '300', 'Done'),
(12, 48, '6775450', 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentcard`
--

INSERT INTO `rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `price_per_hrs`, `price_per_kms`, `rentcard_admin_status`) VALUES
(1, 120, 3, 1, '500', 100, 10, 1),
(2, 122, 4, 2, '200', 20, 20, 1),
(3, 120, 4, 3, '500', 50, 5, 1),
(4, 122, 3, 4, '1000', 100, 50, 1),
(5, 56, 2, 5, '800', 10, 30, 1),
(6, 56, 4, 6, '100', 10, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(100, 87, 17, 0, '2017-09-26'),
(99, 86, 17, 1, '2017-09-26'),
(98, 85, 17, 0, '2017-09-26'),
(97, 84, 17, 0, '2017-09-26'),
(96, 83, 17, 1, '2017-09-26'),
(95, 82, 17, 1, '2017-09-26'),
(94, 81, 17, 1, '2017-09-26'),
(93, 80, 14, 0, '2017-09-26'),
(92, 80, 13, 1, '2017-09-26'),
(91, 79, 17, 1, '2017-09-26'),
(90, 78, 14, 0, '2017-09-26'),
(89, 78, 13, 1, '2017-09-26'),
(88, 77, 14, 0, '2017-09-26'),
(87, 77, 13, 1, '2017-09-26'),
(86, 76, 16, 1, '2017-09-26'),
(85, 75, 16, 1, '2017-09-26'),
(84, 74, 14, 0, '2017-09-25'),
(83, 72, 13, 1, '2017-09-23'),
(82, 69, 6, 1, '2017-09-22'),
(81, 68, 8, 1, '0000-00-00'),
(80, 67, 8, 0, '0000-00-00'),
(79, 66, 8, 1, '2017-09-21'),
(78, 62, 6, 0, '2017-09-21'),
(77, 61, 6, 1, '2017-09-21'),
(76, 60, 9, 1, '2017-09-21'),
(75, 59, 8, 1, '2017-09-20'),
(74, 58, 8, 0, '2017-09-20'),
(73, 57, 8, 1, '2017-09-20'),
(72, 56, 8, 1, '2017-09-20'),
(71, 55, 8, 1, '0000-00-00'),
(70, 54, 8, 0, '0000-00-00'),
(69, 53, 8, 1, '2017-09-19'),
(68, 52, 8, 1, '2017-09-19'),
(67, 51, 8, 0, '2017-09-19'),
(66, 50, 8, 1, '2017-09-19'),
(65, 49, 8, 1, '2017-09-19'),
(64, 48, 8, 1, '2017-09-19'),
(63, 47, 8, 1, '2017-09-19'),
(62, 46, 8, 1, '2017-09-19'),
(61, 45, 9, 1, '2017-09-19'),
(60, 44, 9, 0, '2017-09-19'),
(59, 43, 9, 0, '2017-09-19'),
(58, 42, 9, 1, '2017-09-19'),
(57, 41, 9, 1, '2017-09-19'),
(56, 40, 9, 1, '2017-09-19'),
(55, 39, 8, 1, '2017-09-18'),
(54, 38, 8, 0, '2017-09-18'),
(53, 37, 7, 1, '2017-09-18'),
(52, 36, 7, 0, '2017-09-18'),
(51, 35, 6, 1, '2017-09-18'),
(101, 88, 17, 0, '2017-09-26'),
(102, 89, 17, 0, '2017-09-26'),
(103, 90, 17, 1, '0000-00-00'),
(104, 91, 17, 1, '0000-00-00'),
(105, 93, 8, 0, '2017-09-26'),
(106, 94, 8, 0, '2017-09-26'),
(107, 95, 8, 0, '2017-09-26'),
(108, 96, 8, 0, '2017-09-26'),
(109, 97, 8, 0, '2017-09-26'),
(110, 97, 19, 0, '2017-09-26'),
(111, 97, 8, 0, '0000-00-00'),
(112, 98, 8, 0, '2017-09-26'),
(113, 98, 19, 1, '2017-09-26'),
(114, 99, 19, 1, '2017-09-27'),
(115, 102, 19, 1, '2017-09-27'),
(116, 101, 19, 1, '0000-00-00'),
(117, 104, 8, 0, '2017-10-03'),
(118, 104, 19, 1, '2017-10-03'),
(119, 105, 8, 0, '2017-10-03'),
(120, 106, 8, 0, '2017-10-03'),
(121, 107, 23, 1, '2017-10-03'),
(122, 108, 23, 1, '2017-10-03'),
(123, 109, 23, 1, '2017-10-03'),
(124, 110, 14, 1, '2017-10-04'),
(125, 111, 13, 1, '2017-10-04'),
(126, 111, 14, 0, '2017-10-04'),
(127, 112, 13, 1, '2017-10-04'),
(128, 112, 14, 0, '2017-10-04'),
(129, 113, 13, 1, '2017-10-04'),
(130, 113, 14, 0, '2017-10-04'),
(131, 114, 13, 1, '2017-10-04'),
(132, 114, 14, 0, '2017-10-04'),
(133, 115, 13, 1, '2017-10-04'),
(134, 115, 14, 0, '2017-10-04'),
(135, 116, 13, 1, '2017-10-04'),
(136, 116, 14, 0, '2017-10-04'),
(137, 117, 13, 1, '2017-10-04'),
(138, 117, 14, 0, '2017-10-04'),
(139, 118, 14, 1, '2017-10-04'),
(140, 119, 14, 0, '2017-10-04'),
(141, 120, 14, 1, '2017-10-04'),
(142, 121, 14, 1, '2017-10-04'),
(143, 122, 24, 1, '2017-10-04'),
(144, 123, 24, 1, '2017-10-04'),
(145, 124, 24, 1, '2017-10-04'),
(146, 125, 14, 1, '2017-10-04'),
(147, 126, 24, 1, '2017-10-04'),
(148, 127, 14, 1, '2017-10-04'),
(149, 128, 24, 0, '2017-10-04'),
(150, 129, 24, 0, '2017-10-04'),
(151, 130, 14, 1, '2017-10-04'),
(152, 131, 24, 0, '2017-10-04'),
(153, 132, 24, 0, '2017-10-04'),
(154, 133, 24, 0, '2017-10-04'),
(155, 134, 14, 1, '2017-10-04'),
(156, 135, 24, 0, '2017-10-04'),
(157, 136, 24, 1, '2017-10-04'),
(158, 137, 24, 0, '2017-10-04'),
(159, 138, 24, 0, '2017-10-04'),
(160, 139, 24, 0, '2017-10-04'),
(161, 140, 14, 0, '2017-10-04'),
(162, 141, 24, 0, '2017-10-04'),
(163, 142, 14, 1, '2017-10-04'),
(164, 143, 24, 0, '2017-10-04'),
(165, 144, 14, 0, '2017-10-04'),
(166, 145, 14, 1, '2017-10-04'),
(167, 146, 24, 0, '2017-10-04'),
(168, 147, 24, 0, '2017-10-04'),
(169, 148, 14, 1, '2017-10-04'),
(170, 149, 24, 1, '2017-10-04'),
(171, 150, 24, 1, '2017-10-04'),
(172, 151, 24, 0, '2017-10-04'),
(173, 152, 14, 1, '2017-10-04'),
(174, 153, 14, 1, '2017-10-04'),
(175, 154, 24, 1, '2017-10-04'),
(176, 155, 24, 0, '2017-10-04'),
(177, 156, 24, 1, '2017-10-04'),
(178, 157, 14, 1, '2017-10-04'),
(179, 158, 14, 1, '2017-10-04'),
(180, 159, 14, 1, '2017-10-04'),
(181, 160, 16, 1, '2017-10-04'),
(182, 161, 16, 1, '2017-10-04'),
(183, 162, 16, 0, '2017-10-04'),
(184, 163, 16, 1, '2017-10-04'),
(185, 164, 22, 1, '2017-10-04'),
(186, 165, 22, 1, '2017-10-04'),
(187, 166, 22, 1, '2017-10-04'),
(188, 167, 22, 1, '2017-10-04'),
(189, 168, 22, 1, '2017-10-04'),
(190, 169, 22, 1, '2017-10-04'),
(191, 170, 22, 1, '2017-10-05'),
(192, 171, 22, 0, '2017-10-05'),
(193, 172, 22, 0, '2017-10-05'),
(194, 173, 16, 0, '2017-10-05'),
(195, 175, 22, 1, '2017-10-05'),
(196, 174, 22, 0, '2017-10-05'),
(197, 176, 22, 1, '2017-10-05'),
(198, 177, 16, 1, '2017-10-05'),
(199, 178, 22, 1, '2017-10-05'),
(200, 179, 22, 1, '2017-10-05'),
(201, 180, 22, 1, '2017-10-05'),
(202, 181, 22, 1, '2017-10-05'),
(203, 182, 13, 1, '2017-10-07'),
(204, 182, 14, 0, '2017-10-07'),
(205, 183, 14, 0, '2017-10-07'),
(206, 184, 13, 1, '2017-10-07'),
(207, 184, 14, 0, '2017-10-07'),
(208, 185, 13, 1, '2017-10-07'),
(209, 185, 14, 0, '2017-10-07'),
(210, 186, 26, 0, '2017-10-07'),
(211, 187, 26, 1, '2017-10-07'),
(212, 188, 27, 0, '2017-10-07'),
(213, 189, 27, 0, '2017-10-07'),
(214, 190, 27, 1, '2017-10-07'),
(215, 191, 27, 1, '2017-10-07'),
(216, 192, 27, 1, '2017-10-07'),
(217, 193, 27, 1, '2017-10-07'),
(218, 195, 27, 0, '0000-00-00'),
(219, 196, 27, 1, '0000-00-00'),
(220, 197, 27, 1, '0000-00-00'),
(221, 198, 27, 1, '0000-00-00'),
(222, 199, 28, 1, '2017-10-07'),
(223, 200, 26, 1, '2017-10-07'),
(224, 201, 27, 0, '2017-10-07'),
(225, 202, 27, 1, '2017-10-07'),
(226, 203, 28, 0, '2017-10-07'),
(227, 204, 27, 1, '2017-10-07'),
(228, 205, 27, 0, '2017-10-07'),
(229, 206, 27, 0, '2017-10-07'),
(230, 207, 28, 0, '2017-10-07'),
(231, 208, 28, 0, '2017-10-07'),
(232, 209, 28, 1, '2017-10-07'),
(233, 210, 28, 1, '2017-10-07'),
(234, 211, 28, 1, '2017-10-08'),
(235, 212, 28, 1, '2017-10-09'),
(236, 213, 28, 1, '2017-10-09'),
(237, 214, 24, 8, '0000-00-00'),
(238, 214, 26, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(18, 173, 16),
(17, 155, 24),
(16, 137, 24),
(15, 97, 19),
(14, 84, 17),
(13, 84, 17),
(12, 62, 6),
(11, 58, 8),
(10, 40, 9),
(19, 188, 27),
(20, 201, 27),
(21, 203, 28),
(22, 203, 28),
(23, 208, 28);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) CHARACTER SET utf8 NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_accept_time` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_accept_time`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(73, 9, 'APPLAUNCH', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Monday, Sep 25', '08:28:34', '08:28:52 AM', '', '', '', '', 0, 2, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-25'),
(72, 10, '', '28.59273779480111', '77.3101744428277', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '28.459496499999997', '77.0266383', 'Gurugram', 'Saturday, Sep 23', '09:24:36', '09:15:03 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.59273779480111,77.3101744428277&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-23'),
(70, 10, '', '28.592672735238146', '77.30998400598764', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '28.459496499999997', '77.0266383', 'Gurugram', 'Saturday, Sep 23', '09:11:23', '09:12:16 AM', '', '', '28/9/2017', '13:41', 0, 2, 2, 1, 2, 0, 4, 1, 0, 1, 1, '2017-09-23'),
(71, 10, '', '28.592672735238146', '77.30998400598764', '272, Gali Number 28, Block A, New Ashok Nagar, Delhi, Uttar Pradesh 110096, India', '28.459496499999997', '77.0266383', 'Gurugram', 'Saturday, Sep 23', '09:18:37', '09:18:37 AM', '', '', '23/9/2017', '13:48', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-23'),
(69, 9, '', '28.4120675083395', '77.043286934495', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Sep 22', '11:47:47', '11:50:41 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120675083395,77.043286934495&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(67, 8, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Thursday, Sep 21', '22:19:20', '10:22:30 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-21'),
(68, 8, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Thursday, Sep 21', '22:20:06', '10:22:22 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-21'),
(65, 8, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Thursday, Sep 21', '22:04:11', '10:15:02 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-21'),
(66, 8, '', '25.3009459037238', '55.4426577687263', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Sep 21', '22:12:14', '10:12:56 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3009459037238,55.4426577687263&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(63, 8, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2519659', '55.3328027', 'Deira City Centre - Deira - Dubai - United Arab Emirates', 'Thursday, Sep 21', '21:51:08', '09:51:45 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-21'),
(64, 8, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2519659', '55.3328027', 'Deira City Centre - Deira - Dubai - United Arab Emirates', 'Thursday, Sep 21', '21:58:29', '10:00:55 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-21'),
(62, 9, '', '28.4120822046489', '77.0432572425313', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '08:27:37', '11:51:47 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120822046489,77.0432572425313&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 2, 0, 3, 1, 0, 1, 1, '2017-09-21'),
(61, 9, '', '28.4121067289115', '77.043255418539', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '07:34:22', '08:23:15 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121067289115,77.043255418539&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-21'),
(60, 9, '', '28.4120683930142', '77.0432671532035', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '07:06:23', '07:07:16 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120683930142,77.0432671532035&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(59, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:26:31', '04:26:59 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(58, 8, 'nominalashar', '25.0889778825024', '55.152487680316', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:24:33', '04:24:33 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889778825024,55.152487680316&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-20'),
(57, 8, 'SHILPA', '25.0897023724457', '55.1523378118873', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 20', '16:13:14', '04:13:37 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0897023724457,55.1523378118873&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(56, 8, '', '25.1019333377634', '55.1677816361189', 'Abdullah Omran Taryam Street - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Wednesday, Sep 20', '12:24:43', '12:25:49 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.1019333377634,55.1677816361189&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(54, 8, '', '25.089332', '55.152450000000044', 'Shatha Tower - Al Sufouh - Dubai - United Arab Emirates', '25.3426164', '55.43003390000001', 'Muweilah Medical Centre - Halwan Suburb - Sharjah - United Arab Emirates', 'Tuesday, Sep 19', '04:41 PM', '05:37:13 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-09-19'),
(55, 8, '', '25.089332', '55.152450000000044', 'Shatha Tower - Al Sufouh - Dubai - United Arab Emirates', '25.3253113', '55.39309960000003', 'City Centre Sharjah - Industrial Area - Sharjah - United Arab Emirates', 'Tuesday, Sep 19', '04:46 PM', '04:48:44 PM', '', '', '', '', 8, 4, 1, 1, 9, 0, 8, 1, 0, 2, 1, '2017-09-19'),
(53, 8, '', '25.0895499444904', '55.1524494588375', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.3284352', '55.5122577', 'Sharjah International Airport', 'Tuesday, Sep 19', '16:10:15', '04:20:32 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0895499444904,55.1524494588375&markers=color:red|label:D|25.3284352,55.5122577&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(52, 8, '', '25.0893965101816', '55.1527642145254', 'Dubai Internet City - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 19', '15:41:03', '03:42:05 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0893965101816,55.1527642145254&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(51, 8, '', '25.0893965101816', '55.1527642145254', 'Dubai Internet City - Dubai\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Tuesday, Sep 19', '09:09:36', '09:09:36 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0893965101816,55.1527642145254&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-19'),
(50, 8, '', '25.0890577817569', '55.1523612812675', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 19', '08:58:56', '08:59:18 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0890577817569,55.1523612812675&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(49, 8, '', '25.0890577817569', '55.1523612812675', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 19', '08:39:17', '08:39:59 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0890577817569,55.1523612812675&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 2, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(48, 8, '', '25.0891142181128', '55.1516079157591', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 19', '08:30:19', '08:30:55 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0891142181128,55.1516079157591&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(47, 8, '', '25.0896416441994', '55.1522741094232', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.3284352', '55.5122577', 'Sharjah International Airport', 'Tuesday, Sep 19', '08:07:05', '08:07:50 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0896416441994,55.1522741094232&markers=color:red|label:D|25.3284352,55.5122577&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(46, 8, '', '25.0896662391428', '55.1524762809277', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 19', '07:52:03', '07:55:27 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0896662391428,55.1524762809277&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(45, 9, '', '28.4120378846472', '77.0432489000043', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4263245561182', '77.0572039112449', 'Nirvana Central Road, Block N, Mayfield Garden, Sector 50\nGurugram, Haryana 122018', 'Tuesday, Sep 19', '06:51:49', '06:52:56 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120378846472,77.0432489000043&markers=color:red|label:D|28.4263245561182,77.0572039112449&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(44, 9, '', '28.4120997630781', '77.0432741111519', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Tuesday, Sep 19', '06:50:06', '09:00:56 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120997630781,77.0432741111519&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 17, 0, 0, 1, 0, 1, 1, '2017-09-19'),
(43, 9, '', '28.4120596118099', '77.04330535806', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Tuesday, Sep 19', '06:49:24', '06:49:24 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120596118099,77.04330535806&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-19'),
(42, 9, '', '28.4119731411883', '77.0432622910267', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4393747797357', '77.0105344802141', '36, Pace City I, Sector 10A\nGurugram, Haryana 122001', 'Tuesday, Sep 19', '06:30:14', '06:33:53 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4119731411883,77.0432622910267&markers=color:red|label:D|28.4393747797357,77.0105344802141&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(41, 9, '', '28.4120680981226', '77.0432014390826', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4503095113862', '77.0355507731438', '116/15, Kirti Nagar, Sector 15 Part 1, Sector 15\nGurugram, Haryana 122022', 'Tuesday, Sep 19', '06:25:24', '06:27:01 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120680981226,77.0432014390826&markers=color:red|label:D|28.4503095113862,77.0355507731438&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-19'),
(40, 9, '', '28.4120994662827', '77.043196506923', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4363962167189', '77.0674878358841', 'Matrachaya Road, Sector 51\nGurugram, Haryana 122003', 'Tuesday, Sep 19', '06:14:52', '06:20:28 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994662827,77.043196506923&markers=color:red|label:D|28.4363962167189,77.0674878358841&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(39, 8, '', '25.3005776185243', '55.4427710920572', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Monday, Sep 18', '19:25:49', '07:27:35 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3005776185243,55.4427710920572&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(38, 8, '', '25.3005776185243', '55.4427710920572', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Monday, Sep 18', '19:25:13', '07:25:13 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3005776185243,55.4427710920572&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(75, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Sep 26', '08:36:09', '08:41:10 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(36, 8, '', '25.3006400603735', '55.4426081478596', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Monday, Sep 18', '19:00:09', '07:00:09 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3006400603735,55.4426081478596&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(35, 7, '', '28.4122319928642', '77.0430684580639', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 18', '15:37:30', '03:39:20 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122319928642,77.0430684580639&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(74, 7, '', '28.4120678971421', '77.0432692926945', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 25', '14:37:46', '02:38:06 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120678971421,77.0432692926945&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 17, 0, 12, 1, 0, 1, 1, '2017-09-25'),
(76, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '09:02:33', '09:04:19 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(77, 10, '', '28.412210530656182', '77.0432423427701', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 26', '09:18:42', '09:18:59 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412210530656182,77.0432423427701&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(78, 10, '', '28.412210530656182', '77.0432423427701', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 26', '09:33:30', '09:33:43 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412210530656182,77.0432423427701&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(79, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '09:50:47', '09:52:50 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(80, 10, '', '28.41221347956789', '77.04324200749397', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 26', '10:08:06', '10:08:22 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221347956789,77.04324200749397&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(81, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:10:18', '10:12:39 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(82, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:16:26', '10:16:59 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(83, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:32:06', '10:32:27 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 3, 1, 1, 2, 0, 8, 1, 0, 1, 1, '2017-09-26'),
(84, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:33:36', '10:33:36 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(85, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:35:27', '10:35:27 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(86, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '10:35:57', '10:36:25 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-09-26'),
(87, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '12:24:09', '12:24:09 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(88, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319000000002', '55.407794900000006', 'City Centre Mirdif', 'Tuesday, Sep 26', '12:26:41', '12:26:41 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|25.216319000000002,55.407794900000006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(89, 14, '', '25.088573429419537', '55.15335269272327', 'Al Naseem St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Tuesday, Sep 26', '12:54:14', '12:54:14 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088573429419537,55.15335269272327&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(90, 14, '', '25.0837692', '55.15403040000001', 'Al Naseem St - Dubai - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Tuesday, Sep 26', '14:17:34', '02:18:52 PM', '', '', '', '', 17, 3, 1, 1, 7, 1, 0, 1, 0, 2, 1, '2017-09-26'),
(91, 14, '', '25.0837692', '55.15403040000001', 'Al Naseem St - Dubai - United Arab Emirates', '25.216319', '55.4077949', 'Mirdif City Centre - Mirdif - Dubai - United Arab Emirates', 'Tuesday, Sep 26', '14:21:32', '02:22:08 PM', '', '', '', '', 17, 3, 1, 1, 7, 1, 0, 1, 0, 2, 1, '2017-09-26'),
(92, 15, '', '24.93905306674793', '67.14936554431915', 'University Rd, Karachi, Pakistan', '24.9064621', '67.0301065', 'Nazimabad No. 1', 'Tuesday, Sep 26', '19:44:56', '07:44:56 PM', '', '', '27/9/2017', '00:05', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(93, 14, '', '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Tuesday, Sep 26', '20:07:52', '08:07:52 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300753728705818,55.4427120834589&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(94, 14, '', '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Tuesday, Sep 26', '20:07:59', '08:07:59 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300753728705818,55.4427120834589&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(95, 14, '', '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Tuesday, Sep 26', '20:08:02', '08:08:02 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300753728705818,55.4427120834589&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(96, 14, '', '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Tuesday, Sep 26', '20:08:28', '08:08:28 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300753728705818,55.4427120834589&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(97, 14, '', '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Tuesday, Sep 26', '20:20:36', '08:20:36 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300753728705818,55.4427120834589&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-26'),
(98, 14, '', '25.300753728705818', '55.4427120834589', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Tuesday, Sep 26', '20:23:53', '08:24:42 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300753728705818,55.4427120834589&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 19, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(99, 14, '', '25.089647413384114', '55.15268549323082', 'Al Shatha Tower - Dubai - United Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Wednesday, Sep 27', '09:31:35', '09:32:05 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.089647413384114,55.15268549323082&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 19, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-09-27'),
(100, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '25.254225999999996', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 27', '10:00:20', '10:00:20 AM', '', '', '27/9/2017', '01:05', 0, 4, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-27'),
(101, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '25.254225999999996', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Sep 27', '10:04:26', '10:20:48 AM', '', '', '27/9/2017', '13:20', 19, 4, 2, 1, 9, 0, 8, 1, 0, 1, 1, '2017-09-27'),
(102, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '25.2547879', '55.3298467', 'Deira City Centre', 'Wednesday, Sep 27', '10:12:14', '10:12:54 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.08841462352676,55.153589732944965&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 19, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-27'),
(103, 14, '', '25.08841462352676', '55.153589732944965', 'Al Naseem St - Dubai - United Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Wednesday, Sep 27', '10:15:23', '10:15:23 AM', '', '', '27/9/2017', '10:40', 0, 4, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-27'),
(104, 14, '', '25.3034896175237', '55.4440303891897', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2583713961585', '55.3266019374132', 'Clock Tower Roundabout - Dubai\nUnited Arab Emirates', 'Tuesday, Oct 3', '16:09:33', '04:11:57 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3034896175237,55.4440303891897&markers=color:red|label:D|25.2583713961585,55.3266019374132&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 19, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(105, 14, '', '25.300922866987', '55.4423744603992', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Oct 3', '16:18:45', '04:18:45 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300922866987,55.4423744603992&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(106, 14, '', '25.3007356952246', '55.4425124733186', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Oct 3', '16:41:03', '04:41:03 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3007356952246,55.4425124733186&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(107, 14, '', '25.3013608330319', '55.4434058539977', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2519659', '55.3328027', 'City Centre Deira', 'Tuesday, Oct 3', '16:54:36', '04:56:36 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3013608330319,55.4434058539977&markers=color:red|label:D|25.2519659,55.3328027&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 23, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(108, 14, '', '25.3006015779078', '55.4424037673178', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Tuesday, Oct 3', '16:57:47', '04:58:24 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3006015779078,55.4424037673178&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 23, 3, 1, 2, 2, 0, 2, 1, 0, 1, 1, '2017-10-03'),
(109, 14, '', '25.3029774052688', '55.4437440386229', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2547879', '55.3298467', 'Deira City Centre', 'Tuesday, Oct 3', '16:59:14', '05:04:07 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3029774052688,55.4437440386229&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 23, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(110, 7, '', '28.4120993478055', '77.0432943374375', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '06:23:38', '06:27:45 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120993478055,77.0432943374375&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(111, 10, '', '28.4123213017957', '77.0432359263753', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '06:35:09', '06:35:42 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123213017957,77.0432359263753&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(112, 10, '', '28.4120664622897', '77.0431581863823', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '06:36:18', '06:36:49 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120664622897,77.0431581863823&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(113, 10, '', '28.4122912055085', '77.043239332748', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '06:38:41', '06:38:59 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122912055085,77.043239332748&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 2, 0, 3, 1, 0, 1, 1, '2017-10-04'),
(114, 10, '', '28.41212830982', '77.0432109406369', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '06:41:34', '06:42:18 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41212830982,77.0432109406369&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(115, 10, '', '28.4121836962748', '77.0432621797156', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '06:43:22', '06:48:15 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121836962748,77.0432621797156&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(116, 10, '', '28.4121467719069', '77.0432316402979', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '06:49:24', '06:49:37 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121467719069,77.0432316402979&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(117, 10, '', '28.4121467719069', '77.0432316402979', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '06:51:09', '06:51:24 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121467719069,77.0432316402979&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(118, 7, '', '28.4120967026013', '77.043273858726', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '07:31:19', '07:32:02 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120967026013,77.043273858726&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(119, 7, '', '28.4120844594803', '77.0432010622089', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '07:38:54', '07:38:54 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120844594803,77.0432010622089&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(120, 7, '', '28.4120844594803', '77.0432010622089', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '07:39:32', '07:40:29 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120844594803,77.0432010622089&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(121, 7, '', '28.4126714445606', '77.0423022285104', '1102, Sohna Road, Block S, Sispal Vihar, Sector 47\nGurugram, Haryana 122004', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '07:45:55', '07:47:29 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4126714445606,77.0423022285104&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(122, 10, '', '28.412212300003205', '77.04316724091768', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '09:53:39', '10:19:38 AM', '09:53:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412212300003205,77.04316724091768&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(123, 10, '', '28.412211415329708', '77.04316925257444', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Wednesday, Oct 4', '10:20:36', '10:26:57 AM', '10:20:47 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412211415329708,77.04316925257444&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(124, 7, '', '28.4121109125808', '77.043365262783', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '10:27:37', '10:29:29 AM', '10:27:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121109125808,77.043365262783&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(125, 10, '', '28.451662566024275', '77.0076360180974', '1022-22, Pataudi Rd, Shakti Park Colony, Sector 10A, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Wednesday, Oct 4', '10:30:38', '10:32:44 AM', '10:30:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.451662566024275,77.0076360180974&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(126, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '11:14:00', '11:18:50 AM', '11:14:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(127, 7, '', '28.4121839904471', '77.0432668179274', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '11:16:59', '11:20:11 AM', '11:17:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121839904471,77.0432668179274&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(128, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Set your drop point', 'Wednesday, Oct 4', '11:22:34', '11:22:34 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(129, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:23:04', '11:23:04 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(130, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:25:16', '11:25:30 AM', '11:25:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(131, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:25:47', '11:25:47 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(132, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:26:21', '11:26:21 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(133, 7, '', '28.4122095066001', '77.0433480693962', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '11:28:30', '11:28:30 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122095066001,77.0433480693962&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(134, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:29:50', '11:30:25 AM', '11:29:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(135, 7, '', '28.4122095066001', '77.0433480693962', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '11:32:23', '11:32:23 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122095066001,77.0433480693962&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_accept_time`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(136, 7, '', '28.4121784856131', '77.0433557544869', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4593637407267', '77.0726980268955', '712, Sector 30 M Wide Main Road, Sushant Lok Phase I, Sector 27\nGurugram, Haryana 122022', 'Wednesday, Oct 4', '11:35:00', '11:35:18 AM', '11:35:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121784856131,77.0433557544869&markers=color:red|label:D|28.4593637407267,77.0726980268955&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(137, 7, '', '28.4121784856131', '77.0433557544869', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4593637407267', '77.0726980268955', '712, Sector 30 M Wide Main Road, Sushant Lok Phase I, Sector 27\nGurugram, Haryana 122022', 'Wednesday, Oct 4', '11:36:11', '11:36:11 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121784856131,77.0433557544869&markers=color:red|label:D|28.4593637407267,77.0726980268955&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(138, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:38:08', '11:38:08 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(139, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:42:10', '11:42:10 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(140, 7, '', '28.4121852739568', '77.0431780728645', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '11:43:08', '11:43:08 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121852739568,77.0431780728645&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(141, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:44:41', '11:44:41 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(142, 7, '', '28.4122115535541', '77.0433900325664', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '11:45:08', '11:45:57 AM', '11:45:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122115535541,77.0433900325664&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 2, 0, 2, 1, 0, 1, 1, '2017-10-04'),
(143, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:47:55', '11:47:55 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(144, 7, '', '28.4121506226425', '77.0432250627853', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '11:48:13', '11:48:13 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121506226425,77.0432250627853&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(145, 7, '', '28.4122549762166', '77.0432648923078', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '11:48:48', '11:49:28 AM', '11:48:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122549762166,77.0432648923078&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 2, 0, 2, 1, 0, 1, 1, '2017-10-04'),
(146, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:48:52', '11:48:52 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(147, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '11:49:33', '11:49:33 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(148, 7, '', '28.41223444697', '77.0433298314203', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '11:50:33', '11:50:58 AM', '11:50:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41223444697,77.0433298314203&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 2, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(149, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '12:02:39', '12:02:50 PM', '12:02:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(150, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '12:03:57', '12:04:24 PM', '12:04:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(151, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '12:08:57', '12:08:57 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(152, 7, '', '28.4121282559863', '77.0429204776883', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '12:12:15', '12:13:25 PM', '12:12:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121282559863,77.0429204776883&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(153, 7, '', '28.4120660181653', '77.0432604283417', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '12:20:16', '12:20:51 PM', '12:20:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120660181653,77.0432604283417&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(154, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '12:26:26', '12:26:38 PM', '12:26:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-04'),
(155, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '12:28:12', '12:28:12 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(156, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.415119887032454', '77.04673625528812', '171, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Wednesday, Oct 4', '12:29:47', '12:30:22 PM', '12:30:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.415119887032454,77.04673625528812&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 24, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(157, 7, '', '28.4120687261116', '77.0431876962891', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '12:36:08', '12:36:41 PM', '12:36:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120687261116,77.0431876962891&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(158, 7, '', '28.4121675606995', '77.0434341447046', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4577791002329', '77.072612196207', 'Netaji Subhash Marg, Sector 44\nGurugram, Haryana 122003', 'Wednesday, Oct 4', '13:27:48', '01:28:20 PM', '01:27:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121675606995,77.0434341447046&markers=color:red|label:D|28.4577791002329,77.072612196207&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(159, 7, '', '28.4122169439758', '77.0434626861', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '13:31:53', '01:33:07 PM', '01:32:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122169439758,77.0434626861&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(160, 14, '', '25.0919760163003', '55.1539930701256', 'Ø´Ø§Ø±Ø¹ Ø§Ù„ÙÙ„Ùƒ - Dubai\nUnited Arab Emirates', '25.254226', '55.330306', 'Deira City Centre Metro Station 2', 'Wednesday, Oct 4', '15:11:00', '03:13:56 PM', '03:11:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0919760163003,55.1539930701256&markers=color:red|label:D|25.254226,55.330306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(161, 14, '', '25.0897413133342', '55.1527698363731', 'Dubai Internet City - Dubai\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Wednesday, Oct 4', '15:15:16', '03:16:09 PM', '03:15:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0897413133342,55.1527698363731&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(162, 14, '', '25.0900111751119', '55.1527508720756', 'Dubai Internet City - Dubai\nUnited Arab Emirates', '25.2986037120294', '55.4415185004473', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', 'Wednesday, Oct 4', '17:16:28', '05:16:28 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0900111751119,55.1527508720756&markers=color:red|label:D|25.2986037120294,55.4415185004473&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(163, 14, '', '25.0900111751119', '55.1527508720756', 'Dubai Internet City - Dubai\nUnited Arab Emirates', '25.2986037120294', '55.4415185004473', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', 'Wednesday, Oct 4', '17:17:41', '06:49:06 PM', '05:17:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0900111751119,55.1527508720756&markers=color:red|label:D|25.2986037120294,55.4415185004473&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(164, 14, '', '25.3008995271307', '55.442511588335', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.089332', '55.15245', 'Shatha Tower', 'Wednesday, Oct 4', '19:19:16', '07:19:56 PM', '07:19:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3008995271307,55.442511588335&markers=color:red|label:D|25.089332,55.15245&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(165, 14, '', '25.300998948825', '55.44284183532', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Wednesday, Oct 4', '20:10:44', '08:12:22 PM', '08:10:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300998948825,55.44284183532&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(166, 14, '', '25.3015329546834', '55.4437542266733', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Wednesday, Oct 4', '20:13:21', '08:29:07 PM', '08:13:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3015329546834,55.4437542266733&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(167, 14, '', '25.3007115956564', '55.4424827545881', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '20:57:31', '08:59:08 PM', '08:57:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3007115956564,55.4424827545881&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 2, 0, 2, 1, 0, 1, 1, '2017-10-04'),
(168, 14, '', '25.3007946492794', '55.4429212957621', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '20:59:24', '08:59:58 PM', '08:59:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3007946492794,55.4429212957621&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 2, 0, 4, 1, 0, 1, 1, '2017-10-04'),
(169, 14, '', '25.3005665353698', '55.4425968071851', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 4', '21:04:28', '09:05:39 PM', '09:04:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3005665353698,55.4425968071851&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(170, 14, '', '25.3007673688986', '55.4423452913761', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', '25.089332', '55.15245', 'Shatha Tower', 'Thursday, Oct 5', '05:30:20', '07:32:45 AM', '05:30:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3007673688986,55.4423452913761&markers=color:red|label:D|25.089332,55.15245&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 17, 0, 12, 1, 0, 1, 1, '2017-10-05'),
(171, 17, '', '25.089591543373544', '55.15255507081747', 'Al Falak St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Thursday, Oct 5', '13:31:05', '01:31:05 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.089591543373544,55.15255507081747&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(172, 17, '', '25.089591543373544', '55.15255507081747', 'Al Falak St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Thursday, Oct 5', '13:31:40', '01:31:40 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.089591543373544,55.15255507081747&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(173, 17, '', '25.089591543373544', '55.15255507081747', 'Al Falak St - Dubai - United Arab Emirates', '', '', 'Set your drop point', 'Thursday, Oct 5', '13:32:05', '01:32:05 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.089591543373544,55.15255507081747&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(174, 17, '', '25.088515737054028', '55.15343885868787', 'Al Naseem St - Dubai - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Oct 5', '13:38:21', '01:38:21 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088515737054028,55.15343885868787&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(175, 14, '', '25.0896331422425', '55.154094658792', 'Al Sufouh 2 - Dubai\nUnited Arab Emirates', '0.0', '0.0', 'No drop off point', 'Thursday, Oct 5', '13:38:21', '01:38:52 PM', '01:38:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0896331422425,55.154094658792&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(176, 17, '', '25.088515737054028', '55.15343885868787', 'Al Naseem St - Dubai - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Oct 5', '13:39:49', '01:45:11 PM', '01:39:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088515737054028,55.15343885868787&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(177, 14, '', '25.0889071336737', '55.1561519131064', 'Abu Dhabi - Ghweifat International Highway - Dubai\nUnited Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Oct 5', '13:49:47', '01:51:49 PM', '01:49:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0889071336737,55.1561519131064&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(178, 17, '', '25.08885187061171', '55.15346735715867', 'Al Naseem St - Dubai - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Oct 5', '13:49:59', '01:51:46 PM', '01:50:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.08885187061171,55.15346735715867&markers=color:red|label:D|25.2788468,55.3309395&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(179, 17, '', '25.088515737054028', '55.15343885868787', 'Al Naseem St - Dubai - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Oct 5', '17:03:30', '05:04:42 PM', '05:03:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088515737054028,55.15343885868787&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-10-05'),
(180, 17, '', '25.088515737054028', '55.15343885868787', 'Al Naseem St - Dubai - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Thursday, Oct 5', '17:05:40', '05:07:54 PM', '05:05:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088515737054028,55.15343885868787&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-10-05'),
(181, 17, '', '25.088515737054028', '55.15343885868787', 'Al Naseem St - Dubai - United Arab Emirates', '25.30277821962582', '55.44526521116495', 'Unnamed Road - Sharjah - United Arab Emirates', 'Thursday, Oct 5', '17:44:13', '09:14:17 PM', '05:44:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.088515737054028,55.15343885868787&markers=color:red|label:D|25.30277821962582,55.44526521116495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 1, 17, 0, 12, 1, 0, 1, 1, '2017-10-05'),
(182, 10, '', '28.41221200511205', '77.04324334859848', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 7', '06:35:32', '07:00:35 AM', '06:35:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221200511205,77.04324334859848&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(183, 10, '', '28.41221200511205', '77.04324334859848', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 7', '07:00:17', '07:00:17 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221200511205,77.04324334859848&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(184, 10, '', '28.41221200511205', '77.04324334859848', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 7', '07:01:01', '07:01:46 AM', '07:01:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221200511205,77.04324334859848&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(185, 10, '', '28.412209645982635', '77.04324670135975', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Set your drop point', 'Saturday, Oct 7', '07:02:15', '07:15:27 AM', '07:02:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412209645982635,77.04324670135975&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(186, 18, '', '28.4120126584926', '77.0311197638512', 'E-14, CD Marg, E-Block, Tatvam Villas, Vipul World, Sector 48\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 7', '08:29:55', '08:29:55 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120126584926,77.0311197638512&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(187, 18, '', '28.4120126584926', '77.0311197638512', 'E-14, CD Marg, E-Block, Tatvam Villas, Vipul World, Sector 48\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 7', '08:30:58', '08:31:47 AM', '08:31:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120126584926,77.0311197638512&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 26, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(188, 19, '', '25.3009083174667', '55.443092957139', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '25.3136721220319', '55.455112606287', 'University City Road - Sharjah\nUnited Arab Emirates', 'Saturday, Oct 7', '11:09:24', '11:09:24 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3009083174667,55.443092957139&markers=color:red|label:D|25.3136721220319,55.455112606287&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(189, 19, '', '25.3009083174667', '55.443092957139', 'Unnamed Road - Sharjah\nUnited Arab Emirates', '25.3136721220319', '55.455112606287', 'University City Road - Sharjah\nUnited Arab Emirates', 'Saturday, Oct 7', '11:09:57', '11:09:57 AM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.3009083174667,55.443092957139&markers=color:red|label:D|25.3136721220319,55.455112606287&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(190, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.309748950908393', '55.45915335416794', 'University City Rd - Sharjah - United Arab Emirates', 'Saturday, Oct 7', '11:36:18', '11:38:12 AM', '11:36:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|25.309748950908393,55.45915335416794&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(191, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.309748950908393', '55.45915335416794', 'University City Rd - Sharjah - United Arab Emirates', 'Saturday, Oct 7', '11:38:45', '12:12:21 PM', '11:38:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|25.309748950908393,55.45915335416794&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(192, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Saturday, Oct 7', '12:28:57', '12:29:18 PM', '12:29:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-07'),
(193, 20, '', '25.300740694742455', '55.442714765667915', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Saturday, Oct 7', '12:29:27', '12:30:03 PM', '12:29:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300740694742455,55.442714765667915&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(194, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Saturday, Oct 7', '12:32:56', '12:32:56 PM', '', '', '7/10/2017', '01:05', 0, 4, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(195, 20, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Saturday, Oct 7', '12:40:00', '12:41:42 PM', '', '', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-10-07'),
(196, 20, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Saturday, Oct 7', '12:43:58', '12:45:05 PM', '12:44:09 PM', '', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 2, 1, '2017-10-07'),
(197, 20, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Saturday, Oct 7', '12:46:26', '12:48:13 PM', '12:46:31 PM', '', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 2, 1, '2017-10-07'),
(198, 19, '', '25.3009483', '55.452699499999994', 'Muwailih Commercial - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira - Dubai - United Arab Emirates', 'Saturday, Oct 7', '12:49:21', '12:49:58 PM', '12:49:30 PM', '', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 2, 1, '2017-10-07'),
(199, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.406582924896647', '55.40044851601124', '8 Ø´Ø§Ø±Ø¹ Ø³Ø¹Ø¯ Ø¨Ù† Ù…Ø¹Ø§Ø° - Ajman - United Arab Emirates', 'Saturday, Oct 7', '13:04:03', '01:04:55 PM', '01:04:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|25.406582924896647,55.40044851601124&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(200, 10, '', '28.40952521757179', '77.04266902059317', 'P-103, Sohna Rd, Parsvnath Greenville, Tatvam Villas, Uppal Southend, Sector 48, Gurugram, Haryana 122004, India', '28.458581445356582', '77.03103695064783', '536-16, Civil Lines, Gurugram, Haryana 122001, India', 'Saturday, Oct 7', '13:18:39', '01:23:58 PM', '01:18:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.40952521757179,77.04266902059317&markers=color:red|label:D|28.458581445356582,77.03103695064783&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 26, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(201, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Saturday, Oct 7', '14:06:17', '02:06:17 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(202, 20, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Saturday, Oct 7', '14:19:24', '02:19:48 PM', '02:19:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(203, 20, '', '25.300740694742455', '55.442714765667915', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '', '', 'Set your drop point', 'Saturday, Oct 7', '14:36:47', '02:36:47 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300740694742455,55.442714765667915&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(204, 20, '', '25.300740694742455', '55.442714765667915', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.405216499999995', '55.5136433', 'Ajman', 'Saturday, Oct 7', '14:39:20', '02:42:31 PM', '02:39:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300740694742455,55.442714765667915&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(205, 21, '', '25.301369051427606', '55.44293772429228', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.287186100000003', '55.4486237', 'United Hypermarket', 'Saturday, Oct 7', '18:51:08', '06:51:08 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.301369051427606,55.44293772429228&markers=color:red|label:D|25.287186100000003,55.4486237&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(206, 21, '', '25.301369051427606', '55.44293772429228', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.287186100000003', '55.4486237', 'United Hypermarket', 'Saturday, Oct 7', '18:51:52', '06:51:52 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.301369051427606,55.44293772429228&markers=color:red|label:D|25.287186100000003,55.4486237&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(207, 21, '', '25.301369051427606', '55.44293772429228', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.287186100000003', '55.4486237', 'United Hypermarket', 'Saturday, Oct 7', '18:55:35', '06:55:35 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.301369051427606,55.44293772429228&markers=color:red|label:D|25.287186100000003,55.4486237&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(208, 21, '', '25.301369051427606', '55.44293772429228', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.287186100000003', '55.4486237', 'United Hypermarket', 'Saturday, Oct 7', '18:55:57', '06:55:57 PM', '', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.301369051427606,55.44293772429228&markers=color:red|label:D|25.287186100000003,55.4486237&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-07'),
(209, 21, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.2867120034877', '55.44833600521088', 'Maliha Rd - Sharjah - United Arab Emirates', 'Saturday, Oct 7', '18:59:24', '07:01:13 PM', '06:59:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|25.2867120034877,55.44833600521088&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-07'),
(210, 21, '', '25.30074978820543', '55.442715771496296', 'Sheikh Mohammed Bin Zayed Rd - Sharjah - United Arab Emirates', '25.286973925252028', '55.44858209788799', 'Maliha Rd - Sharjah - United Arab Emirates', 'Saturday, Oct 7', '19:01:50', '07:35:06 PM', '07:01:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.30074978820543,55.442715771496296&markers=color:red|label:D|25.286973925252028,55.44858209788799&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(211, 21, '', '25.300729479470537', '55.442715771496296', 'Unnamed Road - Sharjah - United Arab Emirates', '25.2788468', '55.3309395', 'Deira', 'Sunday, Oct 8', '05:47:06', '05:00:09 PM', '05:47:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.300729479470537,55.442715771496296&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 1, 17, 0, 12, 1, 0, 1, 1, '2017-10-08'),
(212, 19, '', '25.0891175581815', '55.1514828577638', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.3015885056722', '55.4530000314116', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', 'Monday, Oct 9', '17:03:40', '05:06:27 PM', '05:04:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0891175581815,55.1514828577638&markers=color:red|label:D|25.3015885056722,55.4530000314116&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-10-09'),
(213, 19, '', '25.0891175581815', '55.1514828577638', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø®ÙŠØ§ÙŠ - Dubai\nUnited Arab Emirates', '25.3015885056722', '55.4530000314116', 'Muwailih Commercial - Sharjah\nUnited Arab Emirates', 'Monday, Oct 9', '17:09:03', '10:18:19 AM', '05:09:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.0891175581815,55.1514828577638&markers=color:red|label:D|25.3015885056722,55.4530000314116&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 3, 1, 2, 17, 0, 12, 1, 0, 1, 1, '2017-10-09'),
(214, 10, '', '28.41221878760878', '77.04323396086694', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.414313673637633', '77.04668194055557', 'C-128, S City Rd, South City II, Sector 49, Gurugram, Haryana 122018, India', 'Tuesday, Oct 10', '06:33:15', '06:34:43 AM', '06:34:18 AM', '', '19/10/2017', '17:02', 26, 3, 2, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-10');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(1, 3, 1, 1),
(2, 3, 2, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(8, 120, 1, 1),
(9, 120, 2, 1),
(10, 120, 3, 1),
(11, 121, 1, 1),
(12, 121, 2, 1),
(13, 122, 1, 1),
(14, 122, 2, 1),
(15, 122, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_done_rental_booking`
--

INSERT INTO `table_done_rental_booking` (`done_rental_booking_id`, `rental_booking_id`, `driver_id`, `driver_arive_time`, `begin_lat`, `begin_long`, `begin_location`, `begin_date`, `begin_time`, `end_lat`, `end_long`, `end_location`, `end_date`, `end_time`, `total_distance_travel`, `total_time_travel`, `rental_package_price`, `rental_package_hours`, `extra_hours_travel`, `extra_hours_travel_charge`, `rental_package_distance`, `extra_distance_travel`, `extra_distance_travel_charge`, `total_amount`, `coupan_price`, `final_bill_amount`, `payment_status`) VALUES
(1, 20, 14, '03:07:56 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '03:08:18 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '03:08:33 PM', '7 Miles', '0 Hr 0 Min', '800', '5', '0', '0', '100 Miles', '-93 Miles', '0', '800', '00.00', '800', 1),
(2, 21, 24, '03:51:14 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '03:51:33 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '03:51:43 PM', '30 Miles', '0 Hr 0 Min', '100', '1', '0', '0', '20 Miles', '10 Miles', '200', '300', '00.00', '300', 1),
(3, 22, 14, '03:54:04 PM', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '03:54:25 PM', '28.412213855437', '77.0433876339368', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '03:54:42 PM', '6 Miles', '0 Hr 0 Min', '800', '5', '0', '0', '100 Miles', '-94 Miles', '0', '800', '00.00', '800', 1),
(4, 24, 14, '04:01:04 PM', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:01:19 PM', '28.4123609596537', '77.0432902486866', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:01:35 PM', '5 Miles', '0 Hr 0 Min', '800', '5', '0', '0', '100 Miles', '-95 Miles', '0', '800', '00.00', '800', 1),
(5, 26, 24, '04:07:10 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:07:25 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:07:37 PM', '18 Miles', '0 Hr 0 Min', '100', '1', '0', '0', '20 Miles', '-2 Miles', '0', '100', '00.00', '100', 1),
(6, 28, 14, '04:11:56 PM', '28.4120872660731', '77.0433033630252', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:12:08 PM', '28.4123577319154', '77.043312071117', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:12:22 PM', '30 Miles', '0 Hr 0 Min', '800', '5', '0', '0', '100 Miles', '-70 Miles', '0', '800', '00.00', '800', 1),
(7, 43, 24, '04:34:54 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:35:11 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:35:28 PM', '14 Miles', '0 Hr 0 Min', '100', '1', '0', '0', '20 Miles', '-6 Miles', '0', '100', '00.00', '100', 1),
(8, 44, 14, '04:44:40 PM', '28.4120681813892', '77.0431962017336', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:45:03 PM', '28.4121191400699', '77.0432445133621', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:45:24 PM', '15 Miles', '0 Hr 0 Min', '800', '5', '0', '0', '100 Miles', '-85 Miles', '0', '800', '00.00', '800', 1),
(9, 45, 24, '04:55:11 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:55:36 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:55:59 PM', '3 Miles', '0 Hr 0 Min', '100', '1', '0', '0', '20 Miles', '-17 Miles', '0', '100', '00.00', '100', 1),
(10, 46, 24, '04:57:27 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:57:39 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:57:52 PM', '6 Miles', '0 Hr 0 Min', '100', '1', '0', '0', '20 Miles', '-14 Miles', '0', '100', '00.00', '100', 1),
(11, 47, 24, '04:58:59 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:59:12 PM', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '2017-10-04', '04:59:28 PM', '30 Miles', '0 Hr 0 Min', '100', '1', '0', '0', '20 Miles', '10 Miles', '200', '300', '00.00', '300', 1),
(12, 48, 22, '01:37:03 AM', '25.300455', '55.4426725', 'Unnamed Road - Sharjah - United Arab Emirates', '2017-10-05', '01:37:48 AM', '25.300455', '55.4426725', 'Unnamed Road - Sharjah - United Arab Emirates', '2017-10-05', '01:38:22 AM', '135509 Km', '0 Hr 0 Min', '1000', '2', '0', '0', '20 Km', '135489 Km', '6774450', '6775450', '00.00', '6775450', 1),
(13, 52, 27, '04:57:22 PM', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '0.00', '0.00', '0', 0),
(14, 54, 27, '05:04:59 PM', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '0.00', '0.00', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-10-04 00.00.01 AM', '2017-10-05 03:59:46 PM', 24, '2074.1', '0000-00-00', 0),
(2, '2017-10-03 00.00.01 AM', '2017-10-05 03:59:46 PM', 23, '4', '0000-00-00', 0),
(3, '2017-10-03 00.00.01 AM', '2017-10-05 03:59:46 PM', 22, '514.5', '0000-00-00', 0),
(4, '2017-10-03 00.00.01 AM', '2017-10-05 03:59:46 PM', 21, '0', '0000-00-00', 0),
(5, '2017-10-03 00.00.01 AM', '2017-10-05 03:59:46 PM', 20, '0', '0000-00-00', 0),
(6, '2017-09-26 00.00.01 AM', '2017-10-05 03:59:46 PM', 19, '501', '0000-00-00', 0),
(7, '2017-09-26 00.00.01 AM', '2017-10-05 03:59:46 PM', 18, '0', '0000-00-00', 0),
(8, '2017-09-26 00.00.01 AM', '2017-10-05 03:59:46 PM', 17, '516', '0000-00-00', 0),
(9, '2017-09-26 00.00.01 AM', '2017-10-05 03:59:46 PM', 16, '0', '0000-00-00', 0),
(10, '2017-09-25 00.00.01 AM', '2017-10-05 03:59:46 PM', 15, '0', '0000-00-00', 0),
(11, '2017-09-25 00.00.01 AM', '2017-10-05 03:59:46 PM', 14, '1549.6', '0000-00-00', 0),
(12, '2017-09-22 00.00.01 AM', '2017-10-05 03:59:46 PM', 13, '500', '0000-00-00', 0),
(13, '2017-09-22 00.00.01 AM', '2017-10-05 03:59:46 PM', 12, '0', '0000-00-00', 0),
(14, '2017-09-22 00.00.01 AM', '2017-10-05 03:59:46 PM', 11, '0', '0000-00-00', 0),
(15, '2017-09-22 00.00.01 AM', '2017-10-05 03:59:46 PM', 10, '0', '0000-00-00', 0),
(16, '2017-09-19 00.00.01 AM', '2017-10-05 03:59:46 PM', 9, '15', '0000-00-00', 0),
(17, '2017-09-18 00.00.01 AM', '2017-10-05 03:59:46 PM', 8, '0', '0000-00-00', 0),
(18, '2017-09-18 00.00.01 AM', '2017-10-05 03:59:46 PM', 6, '6', '0000-00-00', 0),
(19, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 24, '0', '0000-00-00', 0),
(20, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 23, '0', '0000-00-00', 0),
(21, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 22, '-396', '0000-00-00', 0),
(22, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 21, '0', '0000-00-00', 0),
(23, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 20, '0', '0000-00-00', 0),
(24, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 19, '0', '0000-00-00', 0),
(25, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 18, '0', '0000-00-00', 0),
(26, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 17, '0', '0000-00-00', 0),
(27, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 16, '0', '0000-00-00', 0),
(28, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 15, '0', '0000-00-00', 0),
(29, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 14, '0', '0000-00-00', 0),
(30, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 13, '0', '0000-00-00', 0),
(31, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 12, '0', '0000-00-00', 0),
(32, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 11, '0', '0000-00-00', 0),
(33, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 10, '0', '0000-00-00', 0),
(34, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 9, '0', '0000-00-00', 0),
(35, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 8, '0', '0000-00-00', 0),
(36, '2017-10-05 04:00:46 PM', '2017-10-05 05:11:55 PM', 6, '0', '0000-00-00', 0),
(37, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 27, '501.52', '0000-00-00', 0),
(38, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 26, '50', '0000-00-00', 0),
(39, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 25, '0', '0000-00-00', 0),
(40, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 24, '0', '0000-00-00', 0),
(41, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 23, '0', '0000-00-00', 0),
(42, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 22, '0', '0000-00-00', 0),
(43, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 21, '0', '0000-00-00', 0),
(44, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 20, '0', '0000-00-00', 0),
(45, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 19, '0', '0000-00-00', 0),
(46, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 18, '0', '0000-00-00', 0),
(47, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 17, '0', '0000-00-00', 0),
(48, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 16, '0', '0000-00-00', 0),
(49, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 15, '0', '0000-00-00', 0),
(50, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 14, '0', '0000-00-00', 0),
(51, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 13, '517.8', '0000-00-00', 0),
(52, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 12, '0', '0000-00-00', 0),
(53, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 11, '0', '0000-00-00', 0),
(54, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 10, '0', '0000-00-00', 0),
(55, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 9, '0', '0000-00-00', 0),
(56, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 8, '0', '0000-00-00', 0),
(57, '2017-10-05 05:12:55 PM', '2017-10-07 12:26:54 PM', 6, '0', '0000-00-00', 0),
(58, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 28, '503.52', '0000-00-00', 0),
(59, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 27, '3000', '0000-00-00', 0),
(60, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 26, '80', '0000-00-00', 0),
(61, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 25, '0', '0000-00-00', 0),
(62, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 24, '0', '0000-00-00', 0),
(63, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 23, '0', '0000-00-00', 0),
(64, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 22, '0', '0000-00-00', 0),
(65, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 21, '0', '0000-00-00', 0),
(66, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 20, '0', '0000-00-00', 0),
(67, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 19, '0', '0000-00-00', 0),
(68, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 18, '0', '0000-00-00', 0),
(69, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 17, '0', '0000-00-00', 0),
(70, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 16, '0', '0000-00-00', 0),
(71, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 15, '0', '0000-00-00', 0),
(72, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 14, '0', '0000-00-00', 0),
(73, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 13, '0', '0000-00-00', 0),
(74, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 12, '0', '0000-00-00', 0),
(75, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 11, '0', '0000-00-00', 0),
(76, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 10, '0', '0000-00-00', 0),
(77, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 9, '0', '0000-00-00', 0),
(78, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 8, '0', '0000-00-00', 0),
(79, '2017-10-07 12:27:54 PM', '2017-10-09 02:52:27 PM', 6, '0', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1505390794document_image_13.jpg', '2018-05-01', 2),
(2, 1, 2, 'uploads/driver/1505390808document_image_12.jpg', '2018-03-01', 2),
(3, 1, 4, 'uploads/driver/1505390820document_image_14.jpg', '2018-03-01', 2),
(4, 2, 3, 'uploads/driver/1505394086document_image_23.jpg', '2017-12-20', 2),
(5, 2, 2, 'uploads/driver/1505394099document_image_22.jpg', '2017-12-20', 2),
(6, 2, 4, 'uploads/driver/1505394110document_image_24.jpg', '2017-11-23', 2),
(7, 3, 3, 'uploads/driver/1505397837document_image_33.jpg', '17-1-2019', 2),
(8, 3, 2, 'uploads/driver/1505397850document_image_32.jpg', '20-12-2018', 2),
(9, 3, 4, 'uploads/driver/1505397862document_image_34.jpg', '26-4-2018', 2),
(10, 5, 3, 'uploads/driver/1505488619document_image_53.jpg', '2017-11-30', 2),
(11, 5, 2, 'uploads/driver/1505488635document_image_52.jpg', '2017-12-19', 2),
(12, 5, 4, 'uploads/driver/1505488648document_image_54.jpg', '2018-02-21', 2),
(13, 6, 3, 'uploads/driver/1505745186document_image_63.jpg', '2017-12-22', 2),
(14, 6, 2, 'uploads/driver/1505745200document_image_62.jpg', '2018-01-25', 2),
(15, 6, 4, 'uploads/driver/1505745217document_image_64.jpg', '2017-12-21', 2),
(16, 7, 1, 'uploads/driver/1505757256document_image_71.jpg', '2017-12-20', 2),
(17, 7, 2, 'uploads/driver/1505757267document_image_72.jpg', '2017-11-23', 2),
(18, 7, 3, 'uploads/driver/1505757280document_image_73.jpg', '2018-05-24', 2),
(19, 8, 1, 'uploads/driver/1505758984document_image_81.jpg', '2017-10-18', 2),
(20, 8, 2, 'uploads/driver/1505758996document_image_82.jpg', '2017-11-23', 2),
(21, 8, 5, 'uploads/driver/1505759008document_image_85.jpg', '2017-11-21', 2),
(22, 9, 3, 'uploads/driver/1505797681document_image_93.jpg', '2017-09-19', 2),
(23, 9, 2, 'uploads/driver/1505797698document_image_92.jpg', '2017-10-18', 2),
(24, 9, 4, 'uploads/driver/1507104009Capture.PNGdocument_image_24.png', '2017-10-18', 2),
(25, 12, 3, 'uploads/driver/1506081764document_image_123.jpg', '2017-10-19', 2),
(26, 12, 2, 'uploads/driver/1506081774document_image_122.jpg', '2017-10-27', 2),
(27, 12, 4, 'uploads/driver/1506081783document_image_124.jpg', '2017-11-23', 2),
(28, 13, 3, 'uploads/driver/1506084627document_image_133.jpg', '21-2-2019', 2),
(29, 13, 2, 'uploads/driver/1506084652document_image_132.jpg', '21-2-2019', 2),
(30, 13, 4, 'uploads/driver/1506084667document_image_134.jpg', '2-5-2019', 2),
(31, 14, 3, 'uploads/driver/1506342817document_image_143.jpg', '2017-11-24', 2),
(32, 14, 2, 'uploads/driver/1506342826document_image_142.jpg', '2017-10-27', 2),
(33, 14, 4, 'uploads/driver/1506342838document_image_144.jpg', '2017-11-23', 2),
(34, 15, 3, 'uploads/driver/1506343296document_image_153.jpg', '10-10-2018', 2),
(35, 15, 2, 'uploads/driver/1506343379document_image_152.jpg', '30-8-2018', 2),
(36, 15, 4, 'uploads/driver/1506343438document_image_154.jpg', '16-8-2018', 2),
(37, 16, 1, 'uploads/driver/1506410677document_image_161.jpg', '25-7-2018', 2),
(38, 16, 2, 'uploads/driver/1506410701document_image_162.jpg', '21-2-2018', 2),
(39, 16, 3, 'uploads/driver/1506410730document_image_163.jpg', '26-6-2019', 2),
(40, 17, 1, 'uploads/driver/1506413972document_image_171.jpg', '8-2-2018', 2),
(41, 17, 2, 'uploads/driver/1506413990document_image_172.jpg', '19-7-2018', 2),
(42, 17, 3, 'uploads/driver/1506414007document_image_173.jpg', '18-4-2018', 2),
(43, 18, 1, 'uploads/driver/1506449957document_image_181.jpg', '26-4-2018', 2),
(44, 18, 2, 'uploads/driver/1506449998document_image_182.jpg', '30-9-2020', 2),
(45, 19, 1, 'uploads/driver/1506453526document_image_191.jpg', '2018-01-25', 2),
(46, 19, 2, 'uploads/driver/1506453541document_image_192.jpg', '2017-12-21', 2),
(47, 19, 5, 'uploads/driver/1506453557document_image_195.jpg', '2018-03-23', 2),
(48, 20, 4, 'uploads/driver/150701920601500624539995.jpgdocument_image_20.jpg', '', 2),
(49, 20, 2, 'uploads/driver/150701920611500517914588.jpgdocument_image_20.jpg', '', 2),
(50, 20, 3, 'uploads/driver/150701920621500947842371.jpgdocument_image_20.jpg', '', 2),
(51, 21, 4, 'uploads/driver/150701945101501121060395.jpgdocument_image_21.jpg', '', 2),
(52, 21, 2, 'uploads/driver/150701945111501121060395.jpgdocument_image_21.jpg', '', 2),
(53, 21, 3, 'uploads/driver/150701945121501121046571.jpgdocument_image_21.jpg', '', 2),
(54, 22, 1, 'uploads/driver/1507044466document_image_221.jpg', '17-1-2018', 2),
(55, 22, 2, 'uploads/driver/1507044484document_image_222.jpg', '15-2-2018', 2),
(56, 22, 5, 'uploads/driver/1507044502document_image_225.jpg', '22-3-2018', 2),
(57, 23, 1, 'uploads/driver/1507044978document_image_231.jpg', '2018-01-24', 2),
(58, 23, 2, 'uploads/driver/1507044991document_image_232.jpg', '2018-02-22', 2),
(59, 23, 5, 'uploads/driver/1507045020document_image_235.jpg', '2018-02-21', 2),
(60, 24, 3, 'uploads/driver/1507107032document_image_243.jpg', '31-1-2019', 2),
(61, 24, 2, 'uploads/driver/1507107044document_image_242.jpg', '6-12-2018', 2),
(62, 24, 4, 'uploads/driver/1507107058document_image_244.jpg', '23-1-2019', 2),
(63, 25, 1, 'uploads/driver/1507223919document_image_251.jpg', '5-10-2019', 2),
(64, 25, 2, 'uploads/driver/1507223939document_image_252.jpg', '22-10-2020', 2),
(65, 26, 4, 'uploads/driver/15073606030hotspot-1.pngdocument_image_26.png', '2017-12-22', 2),
(66, 26, 2, 'uploads/driver/15073606031ios_logo_customer.pngdocument_image_26.png', '2017-12-25', 2),
(67, 26, 3, 'uploads/driver/15073606032logo_100.pngdocument_image_26.png', '2017-12-27', 2),
(68, 27, 1, 'uploads/driver/1507370584document_image_271.jpg', '2017-11-30', 2),
(69, 27, 2, 'uploads/driver/1507370598document_image_272.jpg', '2017-12-20', 2),
(70, 27, 5, 'uploads/driver/1507370612document_image_275.jpg', '2017-12-20', 2),
(71, 28, 5, 'uploads/driver/15073772810back.jpgdocument_image_28.jpg', '2017-01-12', 2),
(72, 28, 2, 'uploads/driver/15073772811back2.pngdocument_image_28.png', '0007-01-12', 2),
(73, 28, 1, '', '2014-01-12', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-09-14 13:10:10', '2017-09-14 13:57:28', '0 Hours 47 Minutes', 0, 47, '2017-09-14'),
(2, 2, '2017-09-14 15:00:13', '2017-09-15 14:10:37', '23 Hours 12 Minutes', 23, 12, '2017-09-14'),
(3, 3, '2017-09-14 15:05:15', '', '', 0, 0, '2017-09-14'),
(4, 4, '2017-09-15 16:08:56', '', '', 0, 0, '2017-09-15'),
(5, 5, '2017-09-15 16:34:27', '2017-09-16 11:39:24', '38 Hours 8 Minutes', 38, 8, '2017-09-15'),
(6, 5, '2017-09-18 12:25:33', '', '', 0, 0, '2017-09-18'),
(7, 6, '2017-09-18 15:36:39', '2017-09-19 06:04:51', '14 Hours 28 Minutes', 14, 28, '2017-09-18'),
(8, 7, '2017-09-18 19:14:00', '2017-09-20 13:48:39', '18 Hours 48 Minutes', 18, 48, '2017-09-18'),
(9, 8, '2017-09-18 19:24:35', '2017-09-20 12:24:28', '30 Hours 65 Minutes', 30, 65, '2017-09-18'),
(10, 9, '2017-09-19 06:14:30', '2017-09-21 07:27:14', '1 Hours 12 Minutes', 1, 12, '2017-09-19'),
(11, 8, '2017-09-20 16:26:22', '2017-09-21 21:51:31', '10 Hours 131 Minutes', 10, 131, '2017-09-20'),
(12, 9, '2017-09-21 07:27:19', '2017-09-21 07:32:53', '0 Hours 10 Minutes', 0, 10, '2017-09-21'),
(13, 6, '2017-09-21 07:33:27', '2017-09-22 11:44:15', '4 Hours 10 Minutes', 4, 10, '2017-09-21'),
(14, 8, '2017-09-21 21:51:34', '', '', 0, 0, '2017-09-21'),
(15, 6, '2017-09-22 11:44:17', '2017-09-22 12:21:26', '0 Hours 73 Minutes', 0, 73, '2017-09-22'),
(16, 13, '2017-09-23 09:13:05', '', '', 0, 0, '2017-09-23'),
(17, 14, '2017-09-25 14:37:34', '2017-10-04 06:06:39', '30 Hours 57 Minutes', 30, 57, '2017-09-25'),
(18, 16, '2017-09-26 08:31:14', '', '', 0, 0, '2017-09-26'),
(19, 17, '2017-09-26 12:54:04', '2017-09-26 20:14:43', '8 Hours 102 Minutes', 8, 102, '2017-09-26'),
(20, 19, '2017-09-26 20:20:17', '2017-09-26 20:44:38', '0 Hours 24 Minutes', 0, 24, '2017-09-26'),
(21, 19, '2017-09-27 09:29:25', '', '', 0, 0, '2017-09-27'),
(22, 17, '2017-09-27 09:54:43', '', '', 0, 0, '2017-09-27'),
(23, 19, '2017-10-03 17:19:11', '2017-10-04 06:54:21', '13 Hours 43 Minutes', 13, 43, '2017-10-03'),
(24, 17, '2017-10-03 16:20:06', '2017-10-03 16:33:39', '0 Hours 15 Minutes', 0, 15, '2017-10-03'),
(25, 22, '2017-10-03 16:31:42', '2017-10-05 13:48:29', '21 Hours 16 Minutes', 21, 16, '2017-10-03'),
(26, 23, '2017-10-03 16:54:07', '2017-10-03 17:13:22', '0 Hours 32 Minutes', 0, 32, '2017-10-03'),
(27, 16, '2017-10-03 17:17:02', '2017-10-03 17:18:27', '0 Hours 1 Minutes', 0, 1, '2017-10-03'),
(28, 14, '2017-10-04 11:09:09', '2017-10-07 08:04:48', '26 Hours 190 Minutes', 26, 190, '2017-10-04'),
(29, 13, '2017-10-04 06:34:48', '2017-10-04 06:53:59', '0 Hours 38 Minutes', 0, 38, '2017-10-04'),
(30, 19, '2017-10-04 06:56:53', '2017-10-04 07:06:43', '0 Hours 9 Minutes', 0, 9, '2017-10-04'),
(31, 16, '2017-10-04 15:37:33', '2017-10-04 19:50:01', '12 Hours 42 Minutes', 12, 42, '2017-10-04'),
(32, 24, '2017-10-04 09:53:13', '', '', 0, 0, '2017-10-04'),
(33, 16, '2017-10-05 13:48:25', '2017-10-05 13:48:22', '6 Hours 45 Minutes', 6, 45, '2017-10-05'),
(34, 22, '2017-10-05 13:48:33', '', '', 0, 0, '2017-10-05'),
(35, 25, '2017-10-06 16:19:05', '', '', 0, 0, '2017-10-06'),
(36, 13, '2017-10-07 06:34:52', '', '', 0, 0, '2017-10-07'),
(37, 26, '2017-10-07 10:08:19', '2017-10-07 08:33:23', '0 Hours 8 Minutes', 0, 8, '2017-10-07'),
(38, 27, '2017-10-07 14:39:04', '2017-10-07 14:38:36', '8 Hours 46 Minutes', 8, 46, '2017-10-07'),
(39, 28, '2017-10-07 13:03:34', '2017-10-09 17:02:50', '3 Hours 59 Minutes', 3, 59, '2017-10-07'),
(40, 28, '2017-10-09 17:02:54', '', '', 0, 0, '2017-10-09');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login Successfull'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfull'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Successfull'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User does not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully saved'),
(47, 24, 'en', 'Email Sent Successfully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Booked Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancelled By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `cartype_rating_star` varchar(255) NOT NULL DEFAULT '0',
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `car_type_id`, `cartype_rating_star`, `rating_date`) VALUES
(12, 14, 4, 2, '', 3, 4.5, '', 3, '4.0', '2017-09-15'),
(13, 15, 4, 3, '', 3, 4.5, '', 3, '5.0', '2017-09-15'),
(14, 16, 3, 3, '', 2, 4, '', 2, '4.0', '2017-09-15'),
(15, 17, 3, 4, '', 2, 3.5, '', 2, '4.0', '2017-09-15'),
(16, 19, 3, 4, '', 2, 4, '', 2, '4.0', '2017-09-15'),
(17, 18, 4, 0, '', 3, 4, '', 0, '0', '2017-09-15'),
(18, 20, 4, 4, '', 3, 4, '', 3, '2.5', '2017-09-15'),
(19, 21, 4, 3.5, '', 3, 4, '', 3, '3.0', '2017-09-15'),
(20, 22, 4, 0, '', 3, 3.5, '', 0, '0', '2017-09-15'),
(21, 23, 4, 0, '', 5, 5, '', 0, '0', '2017-09-15'),
(22, 26, 4, 0, '', 5, 5, '', 0, '0', '2017-09-15'),
(23, 27, 4, 0, '', 5, 5, '', 0, '0', '2017-09-15'),
(24, 28, 4, 0, '', 5, 3.5, '', 0, '0', '2017-09-15'),
(25, 30, 6, 0, '', 5, 3.5, '', 0, '0', '2017-09-15'),
(26, 31, 6, 0, '', 5, 5, '', 4, '0.0', '2017-09-15'),
(27, 33, 6, 5, '', 5, 5, '', 4, '5.0', '2017-09-15'),
(28, 34, 6, 5, '', 5, 5, '', 4, '5.0', '2017-09-15'),
(29, 35, 7, 4, '', 6, 4.5, '', 3, '4.0', '2017-09-18'),
(30, 37, 8, 5, '', 7, 5, '', 4, '0', '2017-09-18'),
(31, 39, 8, 4.5, '', 8, 5, '', 4, '4.5', '2017-09-18'),
(32, 40, 9, 5, '', 9, 5, '', 3, '4.0', '2017-09-19'),
(33, 42, 9, 0, '', 9, 5, '', 0, '0', '2017-09-19'),
(34, 45, 9, 5, '', 9, 4, '', 3, '5.0', '2017-09-19'),
(35, 46, 8, 5, '', 8, 5, '', 4, '5.0', '2017-09-19'),
(36, 47, 8, 5, '', 8, 5, '', 4, '5.0', '2017-09-19'),
(37, 48, 8, 5, '', 8, 5, '', 4, '5.0', '2017-09-19'),
(38, 49, 8, 0, '', 8, 4.5, '', 0, '0', '2017-09-19'),
(39, 50, 8, 5, '', 8, 5, '', 4, '5.0', '2017-09-19'),
(40, 52, 8, 5, '', 8, 4.5, '', 4, '5.0', '2017-09-19'),
(41, 53, 8, 4.5, '', 8, 4.5, '', 4, '5.0', '2017-09-19'),
(42, 56, 8, 5, '', 8, 5, '', 4, '4.5', '2017-09-20'),
(43, 57, 8, 5, '', 8, 4.5, '', 4, '5.0', '2017-09-20'),
(44, 59, 8, 5, '', 8, 5, '', 4, '5.0', '2017-09-20'),
(45, 60, 9, 4, '', 9, 4, '', 3, '4.0', '2017-09-21'),
(46, 66, 8, 5, '', 8, 5, '', 4, '5.0', '2017-09-21'),
(47, 69, 9, 4, '', 6, 3.5, '', 3, '4.0', '2017-09-22'),
(48, 75, 14, 5, '', 16, 5, '', 4, '5.0', '2017-09-26'),
(49, 76, 14, 5, '', 16, 5, '', 4, '5.0', '2017-09-26'),
(50, 72, 10, 0, '', 13, 4, '', 0, '0', '2017-09-26'),
(51, 77, 10, 4, '', 13, 3, '', 2, '3.5', '2017-09-26'),
(52, 78, 10, 0, '', 13, 4, '', 0, '0', '2017-09-26'),
(53, 79, 14, 0, '', 17, 5, '', 0, '0', '2017-09-26'),
(54, 80, 10, 3, '', 13, 4, '', 2, '4.5', '2017-09-26'),
(55, 81, 14, 5, '', 17, 5, '', 3, '5.0', '2017-09-26'),
(56, 82, 14, 5, '', 17, 4.5, '', 3, '5.0', '2017-09-26'),
(57, 90, 14, 0, '', 17, 4.5, '', 0, '0', '2017-09-26'),
(58, 91, 14, 5, '', 17, 4.5, '', 3, '5.0', '2017-09-26'),
(59, 98, 14, 0, '', 19, 5, '', 0, '0', '2017-09-26'),
(60, 102, 14, 0, '', 19, 5, '', 0, '0', '2017-09-27'),
(61, 104, 14, 4.5, '', 19, 4, '', 4, '0', '2017-10-03'),
(62, 107, 14, 5, '', 23, 4.5, '', 3, '5.0', '2017-10-03'),
(63, 109, 14, 5, '', 23, 5, '', 3, '5.0', '2017-10-03'),
(64, 110, 7, 3, '', 14, 5, '', 2, '4.5', '2017-10-04'),
(65, 111, 10, 0, '', 13, 4.5, '', 2, '0', '2017-10-04'),
(66, 112, 10, 5, '', 13, 4, '', 2, '4.5', '2017-10-04'),
(67, 115, 10, 0, '', 13, 5, '', 2, '5.0', '2017-10-04'),
(68, 118, 7, 0, '', 14, 3.5, '', 2, '4.0', '2017-10-04'),
(69, 120, 7, 0, '', 14, 4, '', 2, '4.0', '2017-10-04'),
(70, 121, 7, 0, '', 14, 4, '', 2, '4.0', '2017-10-04'),
(71, 122, 10, 4, '', 24, 4.5, '', 4, '3.5', '2017-10-04'),
(72, 123, 10, 0, '', 24, 4.5, '', 0, '0', '2017-10-04'),
(73, 124, 7, 4, '', 24, 4, '', 4, '4.0', '2017-10-04'),
(74, 125, 10, 3, '', 14, 4, '', 2, '3.0', '2017-10-04'),
(75, 126, 10, 4.5, '', 24, 5, '', 4, '4.5', '2017-10-04'),
(76, 127, 7, 0, '', 14, 4, '', 0, '0', '2017-10-04'),
(77, 134, 10, 5, '', 14, 4, '', 2, '5.0', '2017-10-04'),
(78, 152, 7, 4, '', 14, 4.5, '', 2, '4.0', '2017-10-04'),
(79, 153, 7, 0, '', 14, 4.5, '', 0, '0', '2017-10-04'),
(80, 156, 10, 5, '', 24, 5, '', 4, '5.0', '2017-10-04'),
(81, 157, 7, 0, '', 14, 4.5, '', 0, '0', '2017-10-04'),
(82, 158, 7, 0, '', 14, 4, '', 2, '0', '2017-10-04'),
(83, 159, 7, 0, '', 14, 4, 'Fghhh', 0, '0', '2017-10-04'),
(84, 160, 14, 5, '', 16, 5, '', 4, '5.0', '2017-10-04'),
(85, 161, 14, 4, '', 16, 5, '', 4, '3.5', '2017-10-04'),
(86, 163, 14, 5, '', 16, 5, '', 4, '5.0', '2017-10-04'),
(87, 164, 14, 5, '', 22, 5, '', 3, '5.0', '2017-10-04'),
(88, 165, 14, 5, '', 22, 5, '', 3, '5.0', '2017-10-04'),
(89, 166, 14, 5, '', 22, 5, '', 3, '5.0', '2017-10-04'),
(90, 169, 14, 0, '', 22, 5, '', 0, '0', '2017-10-04'),
(91, 175, 14, 0, '', 22, 5, '', 3, '5.0', '2017-10-05'),
(92, 176, 17, 5, '', 22, 5, '', 3, '5.0', '2017-10-05'),
(93, 177, 14, 5, '', 16, 5, '', 4, '5.0', '2017-10-05'),
(94, 178, 17, 5, '', 22, 0, '', 3, '5.0', '2017-10-05'),
(95, 179, 17, 4.5, '', 22, 5, '', 3, '5.0', '2017-10-05'),
(96, 180, 17, 5, '', 22, 5, '', 3, '5.0', '2017-10-05'),
(97, 182, 10, 0, '', 13, 5, '', 0, '0', '2017-10-07'),
(98, 187, 18, 0, '', 26, 2, '', 3, '4.5', '2017-10-07'),
(99, 191, 20, 0, '', 27, 5, '', 0, '0', '2017-10-07'),
(100, 199, 20, 5, '', 28, 5, '', 3, '5.0', '2017-10-07'),
(101, 200, 10, 0, '', 26, 4, '', 0, '0', '2017-10-07'),
(102, 210, 21, 0, '', 28, 5, '', 0, '0', '2017-10-07');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_rental_rating`
--

INSERT INTO `table_rental_rating` (`rating_id`, `rating_star`, `rental_booking_id`, `comment`, `user_id`, `driver_id`, `app_id`) VALUES
(1, '4', 20, '', 7, 14, 1),
(2, '3.0', 20, '', 7, 14, 1),
(3, '4', 21, '', 10, 24, 1),
(4, '5.0', 21, '', 10, 24, 1),
(5, '4.0', 22, '', 10, 14, 2),
(6, '5.0', 22, '', 10, 14, 1),
(7, '4.0', 24, '', 10, 14, 2),
(8, '5.0', 24, '', 10, 14, 1),
(9, '4', 26, '', 10, 24, 1),
(10, '4.5', 26, '', 10, 24, 1),
(11, '5.0', 28, '', 7, 14, 2),
(12, '5.0', 28, '', 7, 14, 1),
(13, '4', 43, '', 10, 24, 1),
(14, '3.5', 43, '', 10, 24, 1),
(15, '4.5', 44, '', 7, 14, 1),
(16, '4.5', 44, '', 7, 14, 2),
(17, '4', 45, '', 10, 24, 1),
(18, '4.0', 45, '', 10, 24, 1),
(19, '4', 46, '', 10, 24, 1),
(20, '4.0', 46, '', 10, 24, 1),
(21, '4', 47, '', 10, 24, 1),
(22, '5.0', 47, '', 10, 24, 1),
(23, '5.0', 48, '', 14, 22, 1),
(24, '4', 48, '', 14, 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 2, 1, 1),
(2, 1, 2, 1, 2),
(3, 1, 3, 0, 3),
(4, 1, 3, 0, 4),
(5, 1, 3, 0, 5),
(6, 1, 3, 2, 6),
(7, 1, 3, 2, 7),
(8, 1, 4, 3, 8),
(9, 1, 4, 3, 9),
(10, 1, 4, 3, 10),
(11, 1, 4, 3, 11),
(12, 1, 4, 3, 12),
(13, 1, 4, 3, 13),
(14, 1, 4, 3, 14),
(15, 1, 4, 3, 15),
(16, 1, 3, 2, 16),
(17, 1, 3, 2, 17),
(18, 1, 4, 3, 18),
(19, 1, 3, 2, 19),
(20, 1, 4, 3, 20),
(21, 1, 4, 3, 21),
(22, 1, 4, 3, 22),
(23, 1, 6, 0, 32),
(24, 1, 7, 6, 35),
(25, 1, 8, 0, 36),
(26, 1, 8, 7, 37),
(27, 1, 8, 0, 38),
(28, 1, 8, 8, 39),
(29, 1, 9, 9, 40),
(30, 1, 9, 9, 41),
(31, 1, 9, 9, 42),
(32, 1, 9, 0, 43),
(33, 1, 9, 0, 44),
(34, 1, 9, 9, 45),
(35, 1, 8, 8, 46),
(36, 1, 8, 8, 47),
(37, 1, 8, 8, 48),
(38, 1, 8, 8, 49),
(39, 1, 8, 8, 50),
(40, 2, 8, 0, 1),
(41, 1, 8, 0, 51),
(42, 1, 8, 8, 52),
(43, 1, 8, 8, 53),
(44, 1, 8, 0, 54),
(45, 1, 8, 8, 55),
(46, 2, 8, 0, 2),
(47, 1, 8, 8, 56),
(48, 1, 8, 8, 57),
(49, 1, 8, 0, 58),
(50, 1, 8, 8, 59),
(51, 1, 9, 9, 60),
(52, 1, 9, 6, 61),
(53, 1, 9, 0, 62),
(54, 1, 8, 0, 63),
(55, 1, 8, 0, 64),
(56, 1, 8, 0, 65),
(57, 1, 8, 8, 66),
(58, 1, 8, 0, 67),
(59, 1, 8, 0, 68),
(60, 1, 9, 6, 69),
(61, 1, 10, 0, 70),
(62, 1, 10, 0, 71),
(63, 1, 10, 13, 72),
(64, 1, 9, 0, 73),
(65, 1, 7, 0, 74),
(66, 1, 14, 16, 75),
(67, 1, 14, 16, 76),
(68, 2, 14, 0, 3),
(69, 2, 14, 0, 4),
(70, 2, 14, 0, 5),
(71, 1, 14, 17, 79),
(72, 1, 14, 17, 81),
(73, 1, 14, 17, 82),
(74, 1, 14, 17, 83),
(75, 1, 14, 0, 84),
(76, 1, 14, 0, 85),
(77, 1, 14, 17, 86),
(78, 2, 14, 0, 6),
(79, 1, 14, 0, 87),
(80, 2, 14, 0, 7),
(81, 1, 14, 0, 88),
(82, 2, 14, 0, 8),
(83, 2, 14, 0, 9),
(84, 1, 14, 0, 89),
(85, 2, 14, 0, 10),
(86, 2, 14, 0, 11),
(87, 2, 14, 0, 12),
(88, 1, 14, 17, 90),
(89, 1, 14, 17, 91),
(90, 1, 15, 0, 92),
(91, 2, 14, 0, 13),
(92, 2, 14, 0, 14),
(93, 2, 14, 0, 15),
(94, 1, 14, 0, 100),
(95, 1, 14, 19, 101),
(96, 1, 14, 0, 103),
(97, 2, 14, 0, 16),
(98, 1, 14, 19, 104),
(99, 1, 14, 0, 105),
(100, 1, 14, 0, 106),
(101, 1, 14, 23, 107),
(102, 1, 14, 23, 108),
(103, 1, 14, 23, 109),
(104, 2, 14, 0, 17),
(105, 2, 14, 0, 18),
(106, 2, 7, 0, 19),
(107, 1, 7, 14, 110),
(108, 1, 10, 13, 111),
(109, 1, 10, 13, 112),
(110, 1, 10, 13, 113),
(111, 1, 10, 13, 114),
(112, 1, 10, 13, 115),
(113, 1, 10, 13, 116),
(114, 1, 10, 13, 117),
(115, 1, 7, 14, 118),
(116, 1, 7, 0, 119),
(117, 1, 7, 14, 120),
(118, 1, 7, 14, 121),
(119, 1, 10, 24, 122),
(120, 1, 10, 24, 123),
(121, 1, 7, 24, 124),
(122, 1, 10, 14, 125),
(123, 2, 7, 14, 20),
(124, 1, 10, 24, 126),
(125, 1, 7, 14, 127),
(126, 2, 10, 24, 21),
(127, 1, 10, 0, 128),
(128, 1, 10, 0, 129),
(129, 2, 10, 14, 22),
(130, 1, 10, 14, 130),
(131, 1, 10, 0, 131),
(132, 1, 10, 0, 132),
(133, 2, 10, 24, 23),
(134, 1, 7, 0, 133),
(135, 1, 10, 14, 134),
(136, 2, 10, 14, 24),
(137, 1, 7, 0, 135),
(138, 2, 7, 24, 25),
(139, 1, 7, 24, 136),
(140, 1, 7, 0, 137),
(141, 2, 10, 24, 26),
(142, 1, 10, 0, 138),
(143, 2, 7, 0, 27),
(144, 2, 7, 14, 28),
(145, 1, 10, 0, 139),
(146, 1, 7, 0, 140),
(147, 2, 7, 0, 29),
(148, 1, 10, 0, 141),
(149, 1, 7, 14, 142),
(150, 2, 7, 14, 30),
(151, 2, 7, 0, 31),
(152, 1, 10, 0, 143),
(153, 1, 7, 0, 144),
(154, 1, 7, 14, 145),
(155, 1, 10, 0, 146),
(156, 1, 10, 0, 147),
(157, 2, 7, 14, 32),
(158, 1, 7, 14, 148),
(159, 2, 7, 14, 33),
(160, 2, 7, 14, 34),
(161, 2, 7, 14, 35),
(162, 2, 7, 0, 36),
(163, 2, 7, 0, 37),
(164, 2, 7, 0, 38),
(165, 2, 7, 14, 39),
(166, 2, 7, 14, 40),
(167, 1, 10, 24, 149),
(168, 2, 10, 24, 41),
(169, 2, 7, 0, 42),
(170, 1, 10, 24, 150),
(171, 2, 10, 24, 43),
(172, 1, 10, 0, 151),
(173, 1, 7, 14, 152),
(174, 2, 7, 14, 44),
(175, 1, 7, 14, 153),
(176, 2, 10, 24, 45),
(177, 1, 10, 24, 154),
(178, 2, 10, 24, 46),
(179, 1, 10, 0, 155),
(180, 2, 10, 24, 47),
(181, 1, 10, 24, 156),
(182, 1, 7, 14, 157),
(183, 1, 7, 14, 158),
(184, 1, 7, 14, 159),
(185, 1, 14, 16, 160),
(186, 1, 14, 16, 161),
(187, 1, 14, 0, 162),
(188, 1, 14, 16, 163),
(189, 1, 14, 22, 164),
(190, 1, 14, 22, 165),
(191, 1, 14, 22, 166),
(192, 1, 14, 22, 167),
(193, 1, 14, 22, 168),
(194, 1, 14, 22, 169),
(195, 2, 14, 22, 48),
(196, 1, 14, 22, 170),
(197, 1, 17, 0, 171),
(198, 1, 17, 0, 172),
(199, 1, 17, 0, 173),
(200, 1, 14, 22, 175),
(201, 1, 17, 0, 174),
(202, 1, 17, 22, 176),
(203, 1, 14, 16, 177),
(204, 1, 17, 22, 178),
(205, 1, 17, 22, 179),
(206, 1, 17, 22, 180),
(207, 1, 17, 22, 181),
(208, 1, 10, 13, 182),
(209, 1, 10, 0, 183),
(210, 1, 10, 13, 184),
(211, 1, 10, 13, 185),
(212, 1, 18, 0, 186),
(213, 1, 18, 26, 187),
(214, 2, 10, 0, 49),
(215, 2, 10, 0, 50),
(216, 1, 19, 0, 188),
(217, 1, 19, 0, 189),
(218, 1, 20, 27, 190),
(219, 1, 20, 27, 191),
(220, 2, 20, 27, 51),
(221, 2, 20, 27, 52),
(222, 1, 20, 27, 192),
(223, 1, 20, 27, 193),
(224, 1, 20, 0, 194),
(225, 2, 20, 27, 53),
(226, 2, 20, 27, 54),
(227, 1, 20, 0, 195),
(228, 1, 20, 27, 196),
(229, 1, 20, 27, 197),
(230, 1, 19, 27, 198),
(231, 1, 20, 28, 199),
(232, 1, 10, 26, 200),
(233, 1, 20, 0, 201),
(234, 1, 20, 27, 202),
(235, 1, 20, 0, 203),
(236, 1, 20, 27, 204),
(237, 1, 21, 0, 205),
(238, 1, 21, 0, 206),
(239, 1, 21, 0, 207),
(240, 1, 21, 0, 208),
(241, 1, 21, 28, 209),
(242, 1, 21, 28, 210),
(243, 1, 21, 28, 211),
(244, 1, 19, 28, 212),
(245, 1, 19, 28, 213),
(246, 1, 10, 26, 214);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `previous_outstanding` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `previous_outstanding`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(16, 1, 'Noor', 'noorabbasi@hotmail.com', '+92509876543', 'forgotten', '', '', 0, '0', '0', ' Wednesday, Oct 4', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1),
(10, 1, 'manish .', 'mkyg@gmail.com', '+928285469069', 'qwerty', '', '', 0, '0', '0.00', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.3', 0, '', 1, '2017-09-22', 1),
(15, 1, 'Muhammad Saad Dilshad .', '', '+923313103888', '', '', '', 0, '0', '0', 'Tuesday, Sep 26', '', 0, 0, 0, 0, 0, '', '', '', '', '', '100975841191005799974', 'Muhammad Saad Dilshad', 'm.saad.dilshad@gmail.com', 'https://lh3.googleusercontent.com/-oUwIwi5LpTg/AAAAAAAAAAI/AAAAAAAAABo/Gjg3dZX_MIM/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-09-26', 1),
(9, 1, 'ami .', 'a@gmail.com', '+918871531856', 'asdfghjkl', 'http://apporio.org/Alaride/alaride/uploads/swift_file65.jpeg', '', 0, '0', '0.00', 'Tuesday, Sep 19', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.3', 0, '', 1, '2017-09-19', 1),
(7, 1, 'Nikita .', 'nikita@apporio.com', '+919215851756', 'qwerty', '', '', 0, '0', '0.00', 'Monday, Sep 18', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.20833333333', 0, '', 1, '2017-09-18', 1),
(14, 1, 'alauser .', 'noorabbasi@hotmail.com', '+92336284291', 'forgotten', '', '', 0, '0', '0.00', 'Tuesday, Sep 26', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.85714285714', 0, '', 1, '2017-09-26', 1),
(17, 1, 'dxbuser .', 'noorabbasi74@gmail.com', '+923362842919', 'forgotten', '', '', 0, '100', '0.00', 'Thursday, Oct 5', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '3.75', 0, '', 1, '2017-10-05', 1),
(18, 1, 'Test test', 'test123@gmail.com', '+919253026622', 'qwerty', '', '', 0, '0', '0.00', ' Saturday, Oct 7', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '2', 0, '', 1, '0000-00-00', 1),
(19, 1, 'shayan .', 'noorabbasi74@gmail.com', '+923333222403', 'forgotten', '', '', 0, '0', '0', 'Saturday, Oct 7', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-07', 1),
(20, 1, 'sameena', 'sameena@gmail.com', '+923032380158', 'forgotten', '', '', 0, '0', '0.00', ' Saturday, Oct 7', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '0000-00-00', 1),
(21, 1, 'fariha .', 'fariha@gmail.com', '+923333222142', 'forgotten', '', '', 0, '0', '0.00', 'Saturday, Oct 7', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-10-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, 'd090LatIv5A:APA91bEH_ub2yuOUnvIfAD2-LWDHaGsWu0e-eRVb0Xj5utZbOFKIDGCBIjlz_q_mxuAiKkGY-uVzl0k8JY8jU73VCxIfi4a2kZb5AZfEnAjrFZsWJx_3X_g7ly-4INcz-l6BLN37qlf7', 0, 'c06c40037632ed39', 1),
(2, 10, 'f2BeKyHzDyo:APA91bGPPJVA54xleXGo8kL9w7xJdhGbrH4cqRgsjDF6ljuLzGLCvBw5j84kRSY1c4FT8ygDR2mpkA3oezv1saah5cRj44Wnu-Pd0SSZ4CXeOHqWCaF4PQvekoP3PKAJ1CACJdIRI_xb', 0, '32c87e564d926edc', 0),
(3, 18, 'A95B68A992C8514DC49EEF97E73769BFFF81492C8266C1F88097CF71147E9538', 1, '7E0FC7E6-4A15-4C6B-BACD-AF6564A0B419', 1),
(4, 5, '4B8EABA65EDD5FA75CF0257869649B409D0ABF790EB4B5ACAB59E87DDAC3A41D', 1, '8C347474-D4EC-411E-9F9E-BC4C0043AE5C', 1),
(5, 19, 'ffbAY9gOVQk:APA91bFQ5wjCOuwkK-J5GoEj0G4YS8b2_wvbHQBlQCRc8phEmavzblz9nrx5PFd-sp3tQMubFdJvKpzyr5O-QRa7y5JQmuJy3UcB2Mub_YHPjOF53hlDYROiZs1htnM-lddrTcFHtq7f', 0, '814e6df7aaacc1c4', 1),
(6, 8, '895BAF5743BDE73F739CED3495576E4738EC6966F5C41B54F5A7C3F20A4FC8C1', 1, 'DBF60787-55BA-4D77-BB04-447056106B2F', 1),
(7, 8, 'A6C5E6956D1C59E21ADA75EDE4C753985813547733E8D15196F41FD24F85CEA9', 1, '37C7351D-D30B-422A-BD51-31FD4611FF04', 1),
(8, 12, '52061D8360A995C5A90149FC6F51B96B804FB40265C3C41E020E5148637DBCC4', 1, '3CF5313F-190C-42CC-8D07-846C2C7A3D4B', 1),
(9, 10, 'cy2wEorFFF8:APA91bHbL7UMeNC7I9nLvT6653_hCv3XYLcAolnqxQIqnEIxgWOEJ21AwHd6hQH44bDjIW_x_6yLnuQ6S3tg-8lVF2oRI9UmEJu2ekDJjhvNdDfaB2B4dsxFUw3PgKNN1IfgLFMuuVvJ', 0, '66bbd3edc0b0850f', 1),
(10, 15, 'fcyOuVl4H4s:APA91bFogDpHAIGiDBLm60X4nvP4i_gZkdSKQBhyaDqPf0J-7nsTllQN8uNny6sCi4ppL1v2XFW1midu0WwI7p7TJxSl3YujnBGw_HH7E5tLeKHH-RgsAyVOlay4eZyW2vAAZvGGr9xJ', 0, 'c068337ac1c1efd', 1),
(11, 21, 'eHhfqWyp-Ww:APA91bFOjAZEaA5OJ4Diw04BPoLm71tClbmXNOgUkDSNne8tqskgDOgHiEtdMs5R3cI6JAfm_4zW0E-GX8a7a6Oj3aZySRUuhdkid6xPDMZKeoR-UYI4tW2-f7QNT85-KWjEizf2D-cd', 0, 'e568666d3808ef7c', 1),
(12, 14, '4593490FC1B581997E2457D2683B204013448DE9D9B64861F817A44112174F7E', 1, '18EEDEA4-4931-4819-B0D9-26ACF98CFD0F', 1),
(13, 7, 'B2A9438082E6BED771908DDE7764A06B89D4A375A81F52F976FA7DE5BC31DA54', 1, '3E453AEB-2E0E-4314-9C97-EA3359190850', 1),
(14, 19, '50E90A3EC67D9B9C4CCD40A6167FD931CEA895732994C6A58A69D58C89F5264F', 1, '61C7BEC1-7A58-4F45-867A-4DBB77500953', 1),
(15, 17, 'c93pMGNf4NQ:APA91bEXVLT-n_nPR5d6SSWy2w7pJC4S3Nuh-0aHHYDYtrrMlHYX_8PHhxU9W2gX0-ru2NagPujH_8yaLCiz4N7gZZOgMpehD_j2XRfUvj-y3R-ZFNkczbszBuAFdIX7dqWuL9F2JL3-', 0, '86a234727868a77c', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
