<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

$ride_id = $_REQUEST['ride_id'];
$driver_id = $_REQUEST['driver_id'];
$driver_token = $_REQUEST['driver_token'];
//$language_id = $_REQUEST['language_id'];
$language_id=1;

if($ride_id !="" && $driver_id !="" && $driver_token != "")
{

    $last_time_stamp = date("h:i:s A");
    $arrived_time = date("h:i:s A");
    $query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
	$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
	$data=$dt->format('M j'); 
	$day=date("l");
	$date=$day.", ".$data ;
	$new_time=date("h:i");

	$query1="UPDATE ride_table SET last_time_stamp='$last_time_stamp', ride_status='5' WHERE ride_id='$ride_id'" ;
	$db->query($query1);
	
	$query2="INSERT INTO done_ride (ride_id,driver_id,arrived_time) VALUES('$ride_id','$driver_id','$arrived_time')";
    $db->query($query2);
			
	$last_id = $db->getLastId();
	$query3="select * from done_ride where done_ride_id='$last_id'";
	$result3 = $db->query($query3);
	$list=$result3->row;

    $query4 = "select * from ride_table where ride_id='$ride_id'";
	$result4 = $db->query($query4);
	$list4 = $result4->row;
    $user_id = $list4['user_id'];
    $ride_status = $list4['ride_status'];
    $pem_file = $list4['pem_file'];
    $query5="select * from user_device where user_id='$user_id' AND login_logout=1";
    $result5 = $db->query($query5);
	$list5=$result5->rows;

    $language="select * from messages where language_id='$language_id' and message_id=26";
	$lang_result = $db->query($language);
    $lang_list=$lang_result->row;
    $message=$lang_list['message_name'];
    $ride_id= (String) $ride_id;
    $ride_status= (String) $ride_status;

        if (!empty($list5))
        {
            foreach ($list5 as $user)
            {
                $device_id = $user['device_id'];
                $flag = $user['flag'];
                if($flag == 1)
                {
                    IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                }
                else
                {
                    AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                }
            }
        }else{
		    $query5 = "select * from user where user_id='$user_id'";
		    $result5 = $db->query($query5);
			$list5 = $result5->row;
		    $device_id = $list5['device_id'];
			if($device_id!="")
		    {
				if($list5['flag'] == 1)
		        {
                    IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
			    } 
		        else 
		        {  
				    AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
			    } 
		    }	
        }
	$re = array('result'=> 1,'msg'=> "Driver Has Been Reached",'details'	=> $list);	
	
	}
	else {
			$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>