<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$done_ride_id=$_REQUEST['done_ride_id'];
//$language_id=$_REQUEST['language_id'];
$language_id=1;

if($done_ride_id != "")
{
    $query="select * from done_ride INNER JOIN driver ON done_ride.driver_id=driver.driver_id where done_ride.done_ride_id='$done_ride_id'";
    $result = $db->query($query);
    $list = $result->row;
    $ride_id = $list['ride_id'];
    $waiting_price = $list['waiting_price'];
    $amount =  $list['amount'];
    $driver_image = $list['driver_image'];
    $total_payable_amount = $list['total_payable_amount'];
    $ride_time_price =  $list['ride_time_price'];
    $total_amount =  $list['total_amount'];
    $coupan_price = $list['coupan_price'];
    $payment_status = $list['payment_status'];
    $wallet_deducted_amount = $list['wallet_deducted_amount'];
    $payment_falied_message = $list['payment_falied_message'];
$previous_outstanding = $list['previous_outstanding'];


    $begin_lat = $list['begin_lat'];
    $begin_long = $list['begin_long'];
    $end_lat = $list['end_lat'];
    $end_long = $list['end_long'];



    $peak_time_charge = $list['peak_time_charge'];
    $night_time_charge = $list['night_time_charge'];
    $query123 ="select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id where ride_table.ride_id='$ride_id'";
    $result123 = $db->query($query123);
    $list123 = $result123->row;
    $user_id =  $list123['user_id'];
    $username =  $list123['user_name'];
    $ride_image = $list123['ride_image'];
    $user_phone =  $list123['user_phone'];
    $user_image =  $list123['user_image'];
    $payment_option_id = $list123['payment_option_id'];
    $query1234 ="select * from payment_option where payment_option_id='$payment_option_id'";
    $result1234 = $db->query($query1234);
    $list1234 = $result1234->row;
    $payment_option_name = $list1234['payment_option_name'];

    $ride_date = $list123['ride_date'];
    $coupan = $list123['coupon_code'];
    $car_type_id = $list123['car_type_id'];
    $query12 ="select * from car_type where car_type_id='$car_type_id'";
    $result12 = $db->query($query12);
    $list12 = $result12->row;
    $car_type_name = $list12['car_type_name'];
    $car_type_image = $list12['car_type_image'];
    if($coupan == "")
    {
        $coupan = "";
    }

    $total_amount =  (string) $total_amount;
    $coupan_price = (string) $coupan_price;
    $c=array(
        'done_ride_id'=> $list['done_ride_id'],
        'begin_location'=> $list['begin_location'],
        'end_location'=> $list['end_location'],
        'waiting_time'=>$list['waiting_time']." "."Min",
        'waiting_price'=>$waiting_price,
        'ride_time'=>$list['tot_time']." "."Min",
        'ride_time_price'=>$ride_time_price,
        'distance'=> $list['distance'],
        'amount'=> $amount,
        'tot_time'=> $list['tot_time'],
        'user_id'=>$user_id,
        'username'=>$username,
        'user_phone'=>$user_phone,
        'user_image'=>$user_image,
        'driver_id'=> $list['driver_id'],
        'driver_name'=> $list['driver_name'],
        'driver_image'=> $list['driver_image'],
        'payment_option_id'=>$payment_option_id,
        'payment_option_name'=>$payment_option_name,
        'ride_id'=>$ride_id,
        'ride_date'=>$ride_date,
        'car_type_id'=>$car_type_id,
        'car_type_name'=>$car_type_name,
        'car_type_image'=>$car_type_image,
        'coupons_code'=>$coupan,
        'coupons_price'=>$coupan_price,
        'total_amount'=>$total_amount,
        'peak_time_charge'=>$peak_time_charge,
        'night_time_charge'=>$night_time_charge,
        'wallet_deducted_amount' => $wallet_deducted_amount,
        'payment_falied_message' => $payment_falied_message,
        'payment_status'=>$payment_status,
        'ride_image'=>$ride_image,
        'total_payable_amount'=>$total_payable_amount,
        'begin_lat' => $begin_lat,
        'begin_long' => $begin_long,
        'end_lat' => $end_lat,
        'end_long' => $end_long,
        'previous_outstanding'=>$previous_outstanding
    );
    $re = array('result'=> 1,'msg'	=> $c);
}
else
{
    $re = array('result'=> 0,'msg'=> "Require fields Missing!!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>