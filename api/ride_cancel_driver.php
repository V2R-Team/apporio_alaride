<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

$ride_id=$_REQUEST['ride_id'];
$reason_id=$_REQUEST['reason_id'];
$language_id=1;

if($ride_id!= ""  && $reason_id!= "" )
{
    $last_time_stamp = date("h:i:s A");
    $query2="select * from ride_table WHERE ride_id='$ride_id'" ;
    $result2 = $db->query($query2);
    $list2=$result2->row;
    $user_id=$list2['user_id'];
    $pem_file = $list2['pem_file'];
    $ride_status = $list2['ride_status'];
    $driver_id = $list2['driver_id'];
    switch ($ride_status)
	{
		case "3":
             $amount_charge = 500;
		    break;
        case "5":
            $query2="select * from done_ride WHERE ride_id='$ride_id'" ;
            $result2 = $db->query($query2);
            $list1=$result2->row;
            $arrived_time = $list1['arrived_time'];
            $minutes_to_add = 10;
            $time = new DateTime($arrived_time);
            $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
            $stamp = $time->format('h:i:s A');
            $time1 = strtotime($last_time_stamp);
            $time2 = strtotime($stamp);
            if ($time2 > $time1)
            {
                $amount_charge = 500;
            }else{
                $amount_charge = 0;
            }
            break;
        case "6":
              $amount_charge = 500;
            break;
        default :
            $amount_charge = 0;
	}

    $query2="select * from done_ride WHERE ride_id='$ride_id'" ;
    $result2 = $db->query($query2);
    $list2123=$result2->row;
    $done_ride_id = $list2123['done_ride_id'];
    if (empty($list2123))
    {
        $query2="INSERT INTO done_ride (ride_id,driver_id,company_commision) VALUES('$ride_id','$driver_id','$amount_charge')";
        $db->query($query2);
        $done_ride_id = $db->getLastId();
    }
    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
    $data=$dt->format('M j');
    $day=date("l");
    $date=$day.", ".$data ;
    $payment_id = 1;
    $payment_method = "Cancel Charges";
    $payment_platform = "Ride Cancel";
    $payment_date = date("Y-m-d");
    $query2="INSERT INTO payment_confirm (order_id,user_id, payment_id, payment_method,payment_platform,payment_amount,payment_date_time,payment_status,payment_date) 
VALUES('$done_ride_id',$user_id,'$payment_id','$payment_method','$payment_platform','$amount_charge','$date','1','$payment_date')";
    $db->query($query2);

	        $query1="UPDATE ride_table SET ride_status='9',last_time_stamp='$last_time_stamp', reason_id='$reason_id' WHERE ride_id='$ride_id'" ;
	        $db->query($query1);
		
                $query2="select * from ride_table WHERE ride_id='$ride_id'" ;
                $result2 = $db->query($query2);
                $list2=$result2->row;
                $user_id=$list2['user_id'];
                $pem_file = $list2['pem_file'];
                $ride_status = $list2['ride_status'];
				$driver_id = $list2['driver_id'];
				$query3="select * from driver WHERE driver_id='$driver_id'" ;
                $result3 = $db->query($query3);	
                $list3=$result3->row;
                $cancelled_rides=$list3['cancelled_rides']+"1";
                $company_payment = $list3['company_payment'];
                $final = $company_payment+$amount_charge;
				$query4="UPDATE driver SET cancelled_rides='$cancelled_rides',busy=0,company_payment='$final' WHERE driver_id='$driver_id'" ;
                $db->query($query4);
				$query5="select * from user_device where user_id='$user_id' AND login_logout=1";
				$result5 = $db->query($query5);
				$list5=$result5->rows;
                $language="select * from messages where language_id='$language_id' and message_id=36";
	            $lang_result = $db->query($language);
                $lang_list=$lang_result->row;

                $message=$lang_list['message_name'];
                $ride_id= (String) $ride_id;
                $ride_status= "9";
				if (!empty($list5))
				{
					foreach ($list5 as $user)
					{
						$device_id = $user['device_id'];
						$flag = $user['flag'];
						if($flag == 1)
						{
							IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
						}
						else
						{
							AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
						}
					}
				}else{
					$query3="select * from user WHERE user_id='$user_id'" ;
                $result3 = $db->query($query3);	
                $list3=$result3->row;
                $device_id=$list3['device_id'];

              $language="select * from messages where language_id='$language_id' and message_id=36";
	                        $lang_result = $db->query($language); 
                                $lang_list=$lang_result->row;

                                $message=$lang_list['message_name'];
                $ride_id= (String) $ride_id;
                $ride_status= (String) $ride_status;

        		if($device_id!="")
        		{
	       			if($list3['flag'] == 1)
               		{
                    	IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
	       			} 
               		else 
               		{  
		    			AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
	       			} 
        		}
				}
				$re = array('result'=> 1,'msg'=> "Ride Cancelled Successfully!!",);   
}
else 
{
	$re = array('result' => 0,'msg'	=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
