<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");
include 'pn_android.php';
include 'pn_iphone.php';
include 'ride-sms.php';
include 'ride-accept-email.php';
include 'firebase_clar.php';
$ride_id = $_REQUEST['ride_id'];
$driver_id = $_REQUEST['driver_id'];
$driver_token = $_REQUEST['driver_token'];
$language_id=1;

if($ride_id != "" && $driver_id != "" && $driver_token!= "" && $language_id != "")
{
    $last_time_stamp = date("h:i:s A");
    $query="select * from ride_table where ride_id='$ride_id'";
    $result = $db->query($query);
    $list = $result->row;
    $ride_status = $list['ride_status'];
    $ride_type = $list['ride_type'];
    if($ride_type == 1){
     $ride_time = strtotime($list['ride_time']);
     $ride_expire_time =   strtotime(date('H:i:s',time() - 1 * 60));
    }else{
       $ride_time = 1;
       $ride_expire_time = 0;
    }

    if($ride_status == 1 && $ride_time >= $ride_expire_time)
    {
	$query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
		$date=$dt->format('M j, Y');
        $day=date("l");
        $date=$day.", ".$date;
        $new_time=date("H:i");

        $query1="UPDATE ride_table SET ride_accept_time='$last_time_stamp',driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='3' WHERE ride_id='$ride_id'" ;
        $db->query($query1);

        $query5="UPDATE driver SET last_update='$new_time',last_update_date='$date',busy=1 WHERE driver_id='$driver_id'" ;
        $db->query($query5);
					
	    $query5="UPDATE table_user_rides SET driver_id='$driver_id' WHERE booking_id='$ride_id' AND ride_mode=1";
        $db->query($query5);

        $query5="UPDATE ride_allocated SET allocated_ride_status=1 WHERE allocated_ride_id='$ride_id' AND allocated_driver_id='$driver_id'";
        $db->query($query5);

        $query3="select * from driver_ride_allocated where ride_id='$ride_id' AND ride_mode=1";
        $result3 = $db->query($query3);
        $list3=$result3->rows;
        foreach ($list3 as $value)
        {
            $driver_id = $value['driver_id'];
            ride_clear($driver_id);
        }

        $query3="select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id where ride_table.ride_id='$ride_id'";
        $result3 = $db->query($query3);
			$list3=$result3->row;
			$user_id=$list3['user_id'];
            $email = $list3['user_email'];
            $user_name = $list3['user_name'];
        $car_type_name = $list3['car_type_name'];
        $driver_name = $list3['driver_name'];
        $driver_phone= $list3['driver_phone'];
        $driver_image= $list3['driver_image'];
        $car_number = $list3['car_number'];
        $pickup_location= $list3['pickup_location'];
        $drop_location = $list3['drop_location'];
			$ride_status = $list3['ride_status'];
           $pem_file = $list3['pem_file'];
           $query4="select * from user where user_id='$user_id'";
           $result4 = $db->query($query4);
           $list41=$result4->rows;
            $user_phone = $list41['user_phone'];
            //send_sms($user_phone);
            email($email,$user_name,$car_type_name,$pickup_location,$drop_location,$driver_name,$driver_phone,$driver_image,$car_type_name);
            $query4="select * from user_device where user_id='$user_id' AND login_logout=1";
            $result4 = $db->query($query4);
            $list4=$result4->rows;
            $language="select * from messages where language_id='$language_id' and message_id=25";
            $lang_result = $db->query($language);
            $lang_list=$lang_result->row;
            $message=$lang_list['message_name'];
            $ride_id= (String) $ride_id;
            $ride_status= (String) $ride_status;
            if (!empty($list4))
			{
				foreach ($list4 as $login)
				{
					$device_id = $login['device_id'];
					$flag = $login['flag'];
                    if($flag == 1)
                    {
                        IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status,$pem_file);
                    }
                    else
                    {
                        AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);

                    }
                }
			}else{
                   $query4="select * from user where user_id='$user_id'";
                   $result4 = $db->query($query4);
                   $list4=$result4->row;
                   $device_id=$list4['device_id'];
                    if($device_id!="")
                    {
                      if($list4['flag'] == 1)
                      {
                            IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status,$pem_file);
                      }
                      else
                      {
                            AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
                      }
                    }
            }
			$re = array('result'=> 1,'msg'=> "Booking Accepted!!",'details'	=> $list3);
		}
	else {
			$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}else{
        ride_clear($driver_id);
  $re = array('result'=> 0,'msg'=>"Ride Expire");
}
}
else 
{
	$re = array('result' => 0,'msg'	=> "Required 1 missing!!",);
}
$log  = "Ride Accept api -: ".date("F j, Y, g:i a").PHP_EOL.
    "Response: ".json_encode($re).PHP_EOL.
    "ride_id: ".$ride_id.PHP_EOL.
    "driver_id: ".$driver_id.PHP_EOL.
    "driver_token: ".$driver_token.PHP_EOL.
    "-------------------------".PHP_EOL;

file_put_contents('../logfile/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
echo json_encode($re, JSON_PRETTY_PRINT);
?>
