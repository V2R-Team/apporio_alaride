<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");
include 'pn_android.php';
include 'pn_iphone.php';
include 'firebase_clar.php';

$ride_id = $_REQUEST['ride_id'];
$reason_id = $_REQUEST['reason_id'];
$language_id=1;

if($ride_id!= "" && $reason_id!= "" )
{
    $last_time_stamp = date("h:i:s A");
    $query1="UPDATE ride_table SET ride_status='2',last_time_stamp='$last_time_stamp', reason_id='$reason_id' WHERE ride_id='$ride_id'";
    $db->query($query1);
    $query3="select * from driver_ride_allocated where ride_id='$ride_id' AND ride_mode=1";
    $result3 = $db->query($query3);
    $list3=$result3->rows;
    foreach ($list3 as $value)
    {
        $driver_id = $value['driver_id'];
        ride_clear($driver_id);
    }
                $query2 = "select * from ride_table WHERE ride_id='$ride_id'" ;
                $result2 = $db->query($query2);	
                $list2 = $result2->row;
                $driver_id = $list2['driver_id'];
                $ride_accept_time = $list2['ride_accept_time'];
                $minutes_to_add = 3;
                $time = new DateTime($ride_accept_time);
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $stamp = $time->format('h:i:s A');
                $last_time_stamp = date("h:i:s A");
                $time1 = strtotime($stamp);
                $time2 = strtotime($last_time_stamp);
                if ($time2 > $time1)
                {
                    $city_id = $list2['city_id'];
                    $user_id = $list2['user_id'];
                    $car_type_id = $list2['car_type_id'];
                    $query2 = "select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
                    $result2 = $db->query($query2);
                    $list3 = $result2->row;
                    $base_distance_price = $list3['base_distance_price'];
                    $query3="select * from user WHERE user_id='$user_id'" ;
                    $result3 = $db->query($query3);
                    $list3=$result3->row;
                    $previous_outstanding = $list3['previous_outstanding'];
                     $all = $previous_outstanding+$base_distance_price;
                     $query4="UPDATE user SET previous_outstanding='$all' WHERE user_id='$user_id'" ;
                     $db->query($query4);
                }
                if ($driver_id != 0)
                {
                    $user_id = $list2['user_id'];
                    $ride_status = $list2['ride_status'];
                    $pem_file = $list2['pem_file'];
                    $query3="select * from driver WHERE driver_id='$driver_id'" ;
                    $result3 = $db->query($query3);
                    $list3=$result3->row;
                    $cancelled_rides=$list3['cancelled_rides']+"1";
                    $device_id=$list3['device_id'];

                    $query4="UPDATE driver SET cancelled_rides='$cancelled_rides',busy=0 WHERE driver_id='$driver_id'" ;
                    $db->query($query4);

                    $language="select * from messages where language_id='$language_id' and message_id=33";
                    $lang_result = $db->query($language);
                    $lang_list=$lang_result->row;

                    $message=$lang_list['message_name'];

                    $ride_id= (String) $ride_id;
                    $ride_status= (String) $ride_status;

                    if($device_id!="")
                    {
                        if($list3['flag'] == 1)
                        {
                            IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                }

                                                         $language="select * from messages where language_id='$language_id' and message_id=27";
	                                                 $lang_result = $db->query($language); 
                                                         $lang_list=$lang_result->row;
                                                         $message_name=$lang_list['message_name'];
				$re = array('result'=> 1,'msg'=> $message_name);   
}
else 
{
	$re = array('result' => 0,'msg'	=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
