<!DOCTYPE html>
<html>
<head>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #floating-panel {
            background: #fff;
            padding: 5px;
            font-size: 14px;
            font-family: Arial;
            border: 1px solid #ccc;
            box-shadow: 0 2px 2px rgba(33, 33, 33, 0.4);
            display: none;
            height: 100px;
            width: 300px;
        }
    </style>
</head>
<body>
<div id="map"></div>
<div id="floating-panel">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td width="240">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30"><strong>Driver Name : </strong><?php echo $data->driver_name ?></td>
                    </tr>
                    <tr>
                        <td height="30"><strong>Car : </strong><?php echo $data->car_type_name ?></td>
                    </tr>
                    <tr>
                        <td height="30"><strong>Car Number: </strong> <span><?php echo $data->car_number ?></td>
                    </tr>
                </table>
            </td>
            <td width="60" align="center">
                <a href="tel:<?php echo $data->driver_phone ?>"><img src="http://www.apporiotaxi.com/Apporiotaxi/uploads/cl.png"></a>
            </td>
        </tr>
    </table>

</div>
<script src="https://www.gstatic.com/firebasejs/3.0.5/firebase.js"></script>

<script>
    var map = undefined;
    var marker = undefined;
    var position = [<?php echo $data->pickup_lat ?>, <?php echo $data->pickup_long ?>];

    function initialize() {

        var latlng = new google.maps.LatLng(position[0], position[1]);
        var myOptions = {
            zoom: 18,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        var name = '<?php echo $data->driver_name ?>';
        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: name,
            animation: google.maps.Animation.DROP,
            icon:'http://www.apporiotaxi.com/admin/img/car2.png'
        });
        var control = document.getElementById('floating-panel');
        control.style.display = 'block';
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(control);
    }
    var config = {
        apiKey: "AIzaSyDNESpI-tIgQ8n_F-HD4Frz2EoJcwOhyys",
        authDomain: "alaride-178310.firebaseapp.com",
        databaseURL: "https://alaride-178310.firebaseio.com",
        projectId: "alaride-178310",
        storageBucket: "alaride-178310.appspot.com",
        messagingSenderId: "78080869834"
    };
    firebase.initializeApp(config);
    var driverId = <?php echo $data->driver_id ?>;
    var firebases = firebase.database().ref('Drivers_A/' + driverId);
    firebases.on("value", function(snapshot) {
        var newPosition = snapshot.val();
        var lat = newPosition.driver_current_latitude;
        var longitude = newPosition.driver_current_longitude;
        var result = [lat, longitude];
        transition(result);
		var latLng = new google.maps.LatLng(lat, longitude); 
         map.panTo(latLng);
		//window.setTimeout(function() {
          //  map.panTo(marker.getPosition());
          //}, 100);
    });
    var numDeltas = 100;
    var delay = 100; //milliseconds
    var i = 0;
    var deltaLat;
    var deltaLng;
    function transition(result){
        i = 0;
        deltaLat = (result[0] - position[0])/numDeltas;
        deltaLng = (result[1] - position[1])/numDeltas;
        moveMarker();
    }

    function moveMarker(){
        position[0] += deltaLat;
        position[1] += deltaLng;
        var latlng = new google.maps.LatLng(position[0], position[1]);
        marker.setPosition(latlng);
        if(i!=numDeltas){
            i++;
            setTimeout(moveMarker, delay);
        }
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnJYaYEdOvK48PVHbYa5jjQ8H2EaYmKe8&callback=initialize">
</script>
</body>
</html>