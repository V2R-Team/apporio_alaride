<?php
class Commonmodel extends CI_Model{

    function Check_User($phone_number)
    {
        $data = $this->db->select('*')
            ->from('user')
            ->where(['user_phone'=>$phone_number])
            ->get();
        return $data->row();
    }

    function Check_Driver($phone_number)
    {
        $data = $this->db->select('*')
            ->from('driver')
            ->where(['driver_phone'=>$phone_number])
            ->get();
        return $data->row();
    }


    function user_profile($id)
    {
        $data = $this->db->select('*')
                        ->from('user')
                        ->where(['user_id'=>$id])
                        ->get();
        return $data->row();
    }

    function driver_profile($id)
    {
        $data = $this->db->select('*')
                        ->from('driver')
                        ->where(['driver_id'=>$id])
                        ->get();
        return $data->row();
    }

    function driver_notification($application,$id,$driver_signup_date)
    {
        $where = "push_app='$application' OR push_driver_id='$id'";
        $data = $this->db->select('*')
                            ->from('push_messages')
                            ->where($where)
                            ->where(['push_messages_date >='=>$driver_signup_date])
                            ->order_by("push_id", "DESC")
                            ->get();
        return $data->result();
    }

    function user_notification($application,$id,$user_signup_date)
    {
        $where = "push_app='$application' OR push_user_id='$id'";
        $data = $this->db->select('*')
                        ->from('push_messages')
                         ->where($where)
                         ->where(['push_messages_date >='=>$user_signup_date])
                         ->order_by("push_id", "DESC")
                         ->get();
        return $data->result();
    }

    function notification($application)
    {
        $data = $this->db->select('*')
                         ->from('push_messages')
                         ->where('push_app',$application)
                         ->order_by("push_id", "DESC")
                         ->get();
        return $data->result();
    }

	function customer_support($support)
	{
		$this->db->insert('customer_support',$support);
	}
	
    function sos()
	{
		$data = $this->db->get_where('sos',['sos_status'=>1]);
		return $data->result();
	}
	
	function SOS_Request($text)
	{
		$this->db->insert('sos',$text);
	}
	
    function get_city()
    {
        $data = $this->db->select('table_city.city_id,table_city.city_name,table_city.city_latitude,table_city.city_longitude,table_city.city_admin_status')
                         ->from('table_city')
                         ->join('table_rate_card','table_rate_card.city_id=table_city.city_id','inner')
                         ->where('table_city.city_admin_status',1)
                         ->get();
        return $data->result_array();
    }

    function get_car($city_id)
    {
        $data = $this->db->select('*')
                         ->from('table_car_type')
                         ->join('table_rate_card','table_rate_card.car_type_id=table_car_type.car_type_id','inner')
                         ->where(['table_car_type.car_type_admin_status'=>1,'table_rate_card.city_id'=>$city_id])
                         ->get();
        return $data->result_array();
    }

    function get_car_model($car_type_id)
    {
        $data = $this->db->select('*')
                         ->from('table_car_model')
                         ->where(['car_model_status'=>1,'car_type_id'=>$car_type_id])
                         ->get();
        return $data->result();
    }

    function cancel_reason($app_id)
    {
        $data = $this->db->select('*')
                        ->from('cancel_reasons')
                        ->where(['reason_type'=>$app_id,'cancel_reasons_status'=>1])
                        ->get();
        return $data->result();
    }

    function ride_details($ride_id)
    {
        $data = $this->db->select('*')
                        ->from('ride_table')
                        ->join('driver','driver.driver_id=ride_table.driver_id','inner')
                        ->join('car_type','car_type.car_type_id=ride_table.car_type_id','inner')
                        ->where(['ride_table.ride_id'=>$ride_id])
                        ->get();
        return $data->row();
    }

}