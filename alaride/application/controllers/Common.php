<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Common extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Commonmodel');
    }

    function Send_Otp_post(){
        $phone_number = $this->post('phone');
        $flag = $this->post('flag');
        if (!empty($phone_number) && !empty($flag))
        {
            if ($flag == 1)
            {
                $data = $this->Commonmodel->Check_User($phone_number);
                if (empty($data))
                {
                    $phone_number =  str_replace("+","",$phone_number);
                    $otp = $this->Send_Sms_Api($phone_number);
                    $this->set_response([
                        'status' =>1,
                        'message' => 'Otp Send',
                        'otp'=>$otp
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' =>0,
                        'message' => 'phone Number Already Registerd'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $data = $this->Commonmodel->Check_Driver($phone_number);
                if (empty($data))
                {
                    $phone_number =  str_replace("+","",$phone_number);
                    $otp = $this->Send_Sms_Api($phone_number);
                    $this->set_response([
                        'status' =>1,
                        'message' => 'Otp Send',
                        'otp'=>$otp
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' =>0,
                        'message' => 'phone Number Already Registerd'
                    ], REST_Controller::HTTP_CREATED);
                }
            }
        }else{
            $this->set_response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Send_Sms_Api($mobile){
        $otp = $this->generateRandomString();
        $username = "923102601212";
        $password = "2953";
        $sender = "alaride";
        $message = "Your One Time Password of Register is".$otp;
        $url = "http://sendpk.com/api/sms.php?username=".$username."&password=".$password."&mobile=".$mobile."&sender=".urlencode($sender)."&message=".urlencode($message)."";

        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        $responce = curl_exec($ch);
        curl_close($ch);
        return $otp;
    }

    function Notification_post()
    {
        $application = $this->post('application');
        $id = $this->post('id');
        if (!empty($application) && !empty($id))
        {
            $data = $this->Commonmodel->notification($application);
            if(!empty($data)){
                if($application == 2){
                    $driver = $this->Commonmodel->driver_profile($id);
                    $driver_signup_date = $driver->driver_signup_date;
                    $data = $this->Commonmodel->driver_notification($application,$id,$driver_signup_date);
                    $this->set_response([
                                    'status' =>1,
                                    'message' => 'All Notifications',
                                    'details'=>$data
                                ], REST_Controller::HTTP_CREATED);
                }else{
                    $user = $this->Commonmodel->user_profile($id);
                    $user_signup_date = $user->user_signup_date;
                    $data = $this->Commonmodel->user_notification($application,$id,$user_signup_date);
                    $this->set_response([
                                        'status' =>1,
                                        'message' => 'All Notifications',
                                        'details'=>$data
                                    ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $this->set_response([
                                'status' =>0,
                                'message' => 'No Record Found'
                            ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->set_response([
                                'status' =>0,
                                'message' => 'No Record Found'
                            ], REST_Controller::HTTP_CREATED);
        }
    }

	function Customer_support_post(){
		$application = $this->post('application');
		$name = $this->post('name');
		$email = $this->post('email');
		$phone = $this->post('phone');
		$query = $this->post('query');
		if(!empty($application) && !empty($name) && !empty($email) && !empty($phone) && !empty($query)){
			$dt = DateTime::createFromFormat('!d/m/Y',date("d/m/Y"));
         	$data=$dt->format('M j'); 
 	        $day=date("l");
 	        $date=$day.", ".$data ;
 	        $time=date("h:i A");
       	    $date_add=$date.", ".$time;
			$support = array(
			                'application' => $application,
							'name' => $name,
							'email' => $email,
							'phone'=>$phone,
							'query'=>$query,
							'date'=>$date_add
			                );
			$this->sendMessage();
            $this->Commonmodel->customer_support($support);
			$this->set_response([
						'status' =>1,
						'message' => 'Thanks For Query'
					], REST_Controller::HTTP_CREATED);
		}else{
			$this->set_response([
						'status' =>0,
						'message' => 'Required Field Missing'
					], REST_Controller::HTTP_CREATED);
		}
	}
	
	function SOS_get()
	{
		$data =  $this->Commonmodel->sos();
		if(!empty($data)){
					$this->set_response([
					'status' => 1,
					'message' => 'SOS',
					'details'=>$data
				], REST_Controller::HTTP_CREATED);
		}else{
			$this->set_response([
						'status' =>0,
						'message' => 'No Record Found'
					], REST_Controller::HTTP_CREATED);
		}
	}
	
	function SOS_Request_post()
	{
		$ride_id =  $this->post('ride_id');
		$driver_id =  $this->post('driver_id');
		$user_id =  $this->post('user_id');
		$sos_number =  $this->post('sos_number');
		$application =  $this->post('application');
		$latitude = $this->post('latitude');
		$longitude = $this->post('longitude');
		if(!empty($ride_id) && !empty($driver_id) && !empty($user_id) && !empty($sos_number) && !empty($application) && !empty($latitude) && !empty($longitude)){
			$request_date = date("Y-m-d H:i:s");
			$text =  array(
							'ride_id' => $ride_id,
							'driver_id' => $driver_id,
							'user_id' => $user_id,
							'sos_number'=>$sos_number,
							'application'=>$application,
							'request_date'=>$request_date,
							'latitude' => $latitude,
		                    'longitude' => $longitude
			              );
            $this->sos_sendMessage();
			  $this->Commonmodel->SOS_Request($text);
					$this->set_response([
					'status' => 1,
					'message' => 'SOS Request Successfully Save'
				], REST_Controller::HTTP_CREATED);
		}else{
			$this->set_response([
						'status' =>0,
						'message' => 'Required Field Missing'
					], REST_Controller::HTTP_CREATED);
		}
	}

    function Get_all_get()
    {
        $city = $this->Commonmodel->get_city();
        $cities = array();
        foreach($city as $k => $v)
        {
            $cities[$v['city_id']]['city_id']=$v['city_id'];
            $cities[$v['city_id']]['city_name']=$v['city_name'];
            $cities[$v['city_id']]['city_latitude'] = $v['city_latitude'];
            $cities[$v['city_id']]['city_longitude'] = $v['city_longitude'];
            $cities[$v['city_id']]['city_admin_status'] = $v['city_admin_status'];
        }
        $city = array();
        foreach($cities as $v){
            $city[] = array('city_id'=>$v['city_id'],'city_name'=>$v['city_name'],'city_latitude'=>$v['city_latitude'],'city_longitude'=>$v['city_longitude'],'city_admin_status'=>$v['city_admin_status']);
        }
        foreach ($city as $key=>$value)
        {
            $city_id = $value['city_id'];
            $car = $this->Commonmodel->get_car($city_id);
            $a= array();
            foreach ($car as $data=>$login)
            {
                $car_type_id = $login['car_type_id'];
                $carmodel = $this->Commonmodel->get_car_model($car_type_id);
                foreach($carmodel as $carmodel)
                {
                    $a[]=$carmodel;
                }
                $car[$data]= $login;
                $car[$data]["Car_Model_list"]=  $a;
                $a = array();
            }
            $city[$key]= $value;
            $city[$key]["Car_type_list"]= $car;
        }
        $this->set_response([
            'status' => 1,
            'message' => 'City And Car Type List',
            'details'=>$city
        ], REST_Controller::HTTP_CREATED);
    }

    function Cancel_Reasons_post()
    {
        $app_id = $this->post('app_id');
        if (!empty($app_id))
        {
            $data = $this->Commonmodel->cancel_reason($app_id);
            if (!empty($data))
            {
                $this->set_response([
                    'status' => 1,
                    'message' => 'Cancel Reason',
                    'details'=>$data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'No Data Found'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Share_Ride_url_post()
    {
        $ride_id = $this->post('ride_id');
        if (!empty($ride_id))
        {
            $data = $this->Commonmodel->ride_details($ride_id);
            if (!empty($data))
            {
                $this->load->library('encrypt');
                $enc_ride_id = $this->encrypt->encode($ride_id);
                $enc_ride_id = str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_ride_id);
                $this->set_response([
                                    'result' => '1',
                                    'message' => 'Share Url',
                                     'details'=>$enc_ride_id
                                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->set_response([
                    'result' => '0',
                    'message' => 'No Record Found'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->set_response([
                'result' => '0',
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function sendMessage(){
        $content = array(
            "en" => 'New Customer Support Query',
        );
        $fields = array(
            'app_id' => "0de94aab-987d-4261-b487-06dfd6e9fe17",
            'included_segments' => array('All'),
            'data' => array("template_id" =>"a32f55c8-9156-4d53-aa0e-07cc17107c2d"),
            'url'=>"http://www.apporiotaxi.com/admin/home.php?pages=customer-support",
            'contents' => $content
        );
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NDU5MmRkNjEtYmIwMC00MDAyLTg5NjctZTkxZTRmZTYzM2Ji'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

        function sos_sendMessage()
        {
            $content = array(
                "en" => 'New SOS Request',
            );
            $fields = array(
                'app_id' => "0de94aab-987d-4261-b487-06dfd6e9fe17",
                'included_segments' => array('All'),
                'data' => array("template_id" =>"a32f55c8-9156-4d53-aa0e-07cc17107c2d"),
                'url'=>"http://www.apporiotaxi.com/admin/home.php?pages=sos",
                'contents' => $content
            );
            $fields = json_encode($fields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                'Authorization: Basic NDU5MmRkNjEtYmIwMC00MDAyLTg5NjctZTkxZTRmZTYzM2Ji'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        }

    public function generateRandomString()
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}