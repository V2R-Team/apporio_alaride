<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class User extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usermodel');
    }

    function Ride_Details_post()
    {
        $ride_mode = $this->post('ride_mode');
        $booking_id = $this->post('booking_id');
        if (!empty($ride_mode) && !empty($booking_id))
        {
            if($ride_mode == 1)
            {
                $normal_ride = $this->Usermodel->normal_ride($booking_id);
                if(!empty($normal_ride))
                {
                    $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                    if (!empty($normal_done_ride))
                    {
                        $begin_lat = $normal_done_ride->begin_lat;
                        if (empty($begin_lat))
                        {
                            $begin_lat = $normal_ride->pickup_lat;
                        }
                        $begin_long = $normal_done_ride->begin_long;
                        if (empty($begin_long))
                        {
                            $begin_long = $normal_ride->pickup_long;
                        }
                        $begin_location = $normal_done_ride->begin_location;
                        if(empty($begin_location))
                        {
                            $begin_location = $normal_ride->pickup_location;
                        }
                        $end_lat = $normal_done_ride->end_lat;
                        if(empty($end_lat))
                        {
                            $end_lat = $normal_ride->drop_lat;
                        }
                        $end_long = $normal_done_ride->end_long;
                        if(empty($end_long))
                        {
                            $end_long = $normal_ride->drop_long;
                        }
                        $end_location = $normal_done_ride->end_location;
                        if(empty($end_location))
                        {
                            $end_location = $normal_ride->drop_location;
                        }
                        $begin_time = $normal_done_ride->begin_time;
                        $end_time = $normal_done_ride->end_time;
                        $arrived_time = $normal_done_ride->arrived_time;
                        $payment_status = $normal_done_ride->payment_status;
                        $distance = $normal_done_ride->distance;
                        $waiting_time = $normal_done_ride->waiting_time;
                        $done_ride_time = $normal_done_ride->tot_time." "."Min";
                        $time = $normal_done_ride->tot_time." "."Min";
                        $amount = $normal_done_ride->amount;
                        $waiting_price = $normal_done_ride->waiting_price;
                        $ride_time_price =  $normal_done_ride->ride_time_price;
                        $total_amount = $normal_done_ride->total_amount;
                        $driver_name = $normal_done_ride->driver_name;
                        $driver_image = $normal_done_ride->driver_image;
                        $driver_rating = $normal_done_ride->rating;
                        $driver_phone = $normal_done_ride->driver_phone;
                        $driver_lat = $normal_done_ride->current_lat;
                        $driver_long = $normal_done_ride->current_long;
                        $car_number = $normal_done_ride->car_number;
                        $driver_location = $normal_done_ride->current_location;
                        $coupan_price = $normal_done_ride->coupan_price;
                        $peak_time_charge = $normal_done_ride->peak_time_charge;
                        $night_time_charge = $normal_done_ride->night_time_charge;
                        $previous_outstanding = $normal_done_ride->previous_outstanding;
                    }else{
                        $begin_lat = $normal_ride->pickup_lat;
                        $begin_long = $normal_ride->pickup_long;
                        $begin_location = $normal_ride->pickup_location;
                        $end_lat = $normal_ride->drop_lat;
                        $end_long = $normal_ride->drop_long;
                        $end_location = $normal_ride->drop_location;
                        $begin_time = "";
                        $end_time = "";
                        $arrived_time = "";
                        $payment_status = "";
                        $distance = "";
                        $waiting_time = "";
                        $done_ride_time = "";
                        $time = "";
                        $amount = "";
                        $waiting_price = "";
                        $ride_time_price =  "";
                        $coupan_price = "";
                        $total_amount = "";
                        $driver_name = "";
                        $driver_image = "";
                        $driver_rating = "";
                        $driver_phone = "";
                        $driver_lat = "";
                        $driver_long = "";
                        $car_number = "";
                        $driver_location = "";
                        $peak_time_charge = "";
                        $night_time_charge = "";
                        $previous_outstanding = "";
                    }
                    $normal_ride->pickup_lat = $begin_lat;
                    $normal_ride->pickup_long = $begin_long;
                    $normal_ride->pickup_location = $begin_location;
                    $normal_ride->drop_lat = $end_lat;
                    $normal_ride->drop_long = $end_long;
                    $normal_ride->drop_location = $end_location;
                    $normal_ride->total_amount = (string)$total_amount;
                    $normal_ride->begin_time = $begin_time;
                    $normal_ride->end_time = $end_time;
                    $normal_ride->arrived_time = $arrived_time;
                    $normal_ride->payment_status = $payment_status;
                    $normal_ride->distance = $distance;
                    $normal_ride->waiting_time = $waiting_time;
                    $normal_ride->done_ride_time = $done_ride_time;
                    $normal_ride->time = $time;
                    $normal_ride->amount = $amount;
                    $normal_ride->waiting_price = $waiting_price;
                    $normal_ride->ride_time_price = $ride_time_price;
                    $normal_ride->coupan_price = $coupan_price;
                    $normal_ride->driver_name = $driver_name;
                    $normal_ride->driver_image = $driver_image;
                    $normal_ride->driver_rating = $driver_rating;
                    $normal_ride->driver_phone = $driver_phone;
                    $normal_ride->driver_lat = $driver_lat;
                    $normal_ride->driver_long = $driver_long;
                    $normal_ride->car_number = $car_number;
                    $normal_ride->driver_location = $driver_location;
                    $payment_option_id = $normal_ride->payment_option_id;
                    $payment = $this->Usermodel->payment($payment_option_id);
                    $payment_option_name = $payment->payment_option_name;
                    $normal_ride->payment_option_name = $payment_option_name;
                    $normal_ride->peak_time_charge = $peak_time_charge;
                    $normal_ride->night_time_charge = $night_time_charge;
                    $normal_ride->previous_outstanding = $previous_outstanding;
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Ride Details',
                        'details' => $normal_ride
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' =>0,
                        'message' => 'Worng Booking Id'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $rental_ride = $this->Usermodel->rental_ride($booking_id);
                if (!empty($rental_ride))
                {
                    $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                    if(!empty($rental_done_ride))
                    {
                        $begin_lat = $rental_done_ride->begin_lat;
                        if (empty($begin_lat))
                        {
                            $begin_lat = $rental_ride->pickup_lat;
                        }
                        $begin_long = $rental_done_ride->begin_long;
                        if (empty($begin_long))
                        {
                            $begin_long = $rental_ride->pickup_long;
                        }
                        $begin_location = $rental_done_ride->begin_location;
                        if(empty($begin_location))
                        {
                            $begin_location = $rental_ride->pickup_location;
                        }
                        $end_lat = $rental_done_ride->end_lat;
                        $end_long = $rental_done_ride->end_long;
                        $end_location = $rental_done_ride->end_location;
                        $driver_arive_time = $rental_done_ride->driver_arive_time;
                        $begin_time = $rental_done_ride->begin_time;
                        $end_time = $rental_done_ride->end_time;
                        $total_distance_travel = $rental_done_ride->total_distance_travel;
                        $total_time_travel = $rental_done_ride->total_time_travel;
                        $rental_package_price = $rental_done_ride->rental_package_price;
                        $rental_package_hours = $rental_done_ride->rental_package_hours;
                        $extra_hours_travel = $rental_done_ride->extra_hours_travel;
                        $extra_hours_travel_charge = $rental_done_ride->extra_hours_travel_charge;
                        $rental_package_distance = $rental_done_ride->rental_package_distance;
                        $extra_distance_travel = $rental_done_ride->extra_distance_travel;
                        $extra_distance_travel_charge = $rental_done_ride->extra_distance_travel_charge;
                        $final_bill_amount = $rental_done_ride->final_bill_amount;
                        $driver_name = $rental_done_ride->driver_name;
                        $driver_image = $rental_done_ride->driver_image;
                        $driver_rating = $rental_done_ride->rating;
                        $driver_phone = $rental_done_ride->driver_phone;
                        $driver_lat = $rental_done_ride->current_lat;
                        $driver_long = $rental_done_ride->current_long;
                        $car_number = $rental_done_ride->car_number;
                        $driver_location = $rental_done_ride->current_location;
                        $total_amount = $rental_done_ride->total_amount;
                        $coupan_price = $rental_done_ride->coupan_price;
                    }else{
                        $begin_lat = $rental_ride->pickup_lat;
                        $begin_long = $rental_ride->pickup_long;
                        $begin_location = $rental_ride->pickup_location;
                        $end_lat = "";
                        $end_long = "";
                        $end_location = "";
                        $final_bill_amount = 0;
                        $driver_arive_time = "";
                        $begin_time = "";
                        $end_time = "";
                        $total_distance_travel = "";
                        $total_time_travel = "";
                        $rental_package_price = "";
                        $rental_package_hours = "";
                        $extra_hours_travel = "";
                        $extra_hours_travel_charge = "";
                        $rental_package_distance = "";
                        $extra_distance_travel = "";
                        $extra_distance_travel_charge = "";
                        $driver_name = "";
                        $driver_image = "";
                        $driver_rating = "";
                        $driver_phone = "";
                        $driver_lat = "";
                        $driver_long = "";
                        $car_number = "";
                        $driver_location = "";
                        $total_amount = "";
                        $coupan_price = "";
                    }
                    $payment_option_id = $rental_ride->payment_option_id;
                    $payment = $this->Usermodel->payment($payment_option_id);
                    $payment_option_name = $payment->payment_option_name;
                    $rental_ride->pickup_lat = $begin_lat;
                    $rental_ride->pickup_long = $begin_long;
                    $rental_ride->pickup_location = $begin_location;
                    $rental_ride->end_lat = $end_lat;
                    $rental_ride->end_long = $end_long;
                    $rental_ride->end_location = $end_location;
                    $rental_ride->final_bill_amount = (string)$final_bill_amount;
                    $rental_ride->driver_arive_time = $driver_arive_time;
                    $rental_ride->begin_time = $begin_time;
                    $rental_ride->end_time = $end_time;
                    $rental_ride->total_distance_travel = $total_distance_travel;
                    $rental_ride->total_time_travel = $total_time_travel;
                    $rental_ride->rental_package_price  = $rental_package_price;
                    $rental_ride->rental_package_hours = $rental_package_hours;
                    $rental_ride->extra_hours_travel = $extra_hours_travel;
                    $rental_ride->extra_hours_travel_charge = $extra_hours_travel_charge;
                    $rental_ride->rental_package_distance = $rental_package_distance;
                    $rental_ride->extra_distance_travel = $extra_distance_travel;
                    $rental_ride->extra_distance_travel_charge = $extra_distance_travel_charge;
                    $rental_ride->driver_name = $driver_name;
                    $rental_ride->driver_image = $driver_image;
                    $rental_ride->driver_rating = $driver_rating;
                    $rental_ride->driver_phone = $driver_phone;
                    $rental_ride->driver_lat = $driver_lat;
                    $rental_ride->driver_long = $driver_long;
                    $rental_ride->car_number = $car_number;
                    $rental_ride->driver_location = $driver_location;
                    $rental_ride->total_amount = $total_amount;
                    $rental_ride->coupan_price = $coupan_price;
                    $rental_ride->payment_option_name = $payment_option_name;
                    $this->set_response([
                        'status' =>1,
                        'message' => 'Ride Details',
                        'details'=>$rental_ride
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Wrong Booking Id',
                    ], REST_Controller::HTTP_CREATED);
                }

            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Required Field Missing',
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Ride_History_post()
    {
        $user_id = $this->post('user_id');
        if (!empty($user_id))
        {
            $data =  $this->Usermodel->user_rides($user_id);
            if(!empty($data))
            {
                foreach($data as $key=>$value){
                    $ride_mode = $value['ride_mode'];
                    $booking_id = $value['booking_id'];
                    if($ride_mode == 1)
                    {
                        $normal_ride = $this->Usermodel->normal_ride($booking_id);
                        $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                        if (!empty($normal_done_ride))
                        { $begin_lat = $normal_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $normal_ride->pickup_lat;
                            }
                            $begin_long = $normal_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $normal_ride->pickup_long;
                            }
                            $begin_location = $normal_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $normal_ride->pickup_location;
                            }
                            $end_lat = $normal_done_ride->end_lat;
                            if(empty($end_lat))
                            {
                                $end_lat = $normal_ride->drop_lat;
                            }
                            $end_long = $normal_done_ride->end_long;
                            if(empty($end_long))
                            {
                                $end_long = $normal_ride->drop_long;
                            }
                            $end_location = $normal_done_ride->end_location;
                            if(empty($end_location))
                            {
                                $end_location = $normal_ride->drop_location;
                            }
                            $amount = $normal_done_ride->amount;
                            $waiting_price = $normal_done_ride->waiting_price;
                            $ride_time_price =  $normal_done_ride->ride_time_price;
                            $total_amount = $normal_done_ride->total_amount;
                            $coupan_price = $normal_done_ride->coupan_price;
                        }else{
                            $begin_lat = $normal_ride->pickup_lat;
                            $begin_long = $normal_ride->pickup_long;
                            $begin_location = $normal_ride->pickup_location;
                            $end_lat = $normal_ride->drop_lat;
                            $end_long = $normal_ride->drop_long;
                            $end_location = $normal_ride->drop_location;
                            $total_amount = "0";
                        }
                        $normal_ride->pickup_lat = $begin_lat;
                        $normal_ride->pickup_long = $begin_long;
                        $normal_ride->pickup_location = $begin_location;
                        $normal_ride->drop_lat = $end_lat;
                        $normal_ride->drop_long = $end_long;
                        $normal_ride->drop_location = $end_location;
                        $normal_ride->total_amount = (string)$total_amount;
                        $rental_ride = array(
                            "rental_booking_id"=> "",
                            "user_id"=> "",
                            "rentcard_id"=> "",
                            "car_type_id"=>"",
                            "booking_type"=>"",
                            "driver_id"=>"",
                            "pickup_lat"=> "",
                            "pickup_long"=> "",
                            "pickup_location"=> "",
                            "start_meter_reading"=>"",
                            "start_meter_reading_image"=> "",
                            "end_meter_reading"=> "",
                            "end_meter_reading_image"=> "",
                            "booking_date"=> "",
                            "booking_time"=> "",
                            "user_booking_date_time"=>"",
                            "last_update_time"=>"",
                            "booking_status"=>"",
                            "booking_admin_status"=>"",
                            "car_type_name"=>"",
                            "car_name_arabic"=> "",
                            "car_type_name_french"=> "",
                            "car_type_image"=> "",
                            "ride_mode"=>"",
                            "car_admin_status"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "",
                            "facebook_image"=> "",
                            "facebook_firstname"=> "",
                            "facebook_lastname"=> "",
                            "google_id"=> "",
                            "google_name"=> "",
                            "google_mail"=> "",
                            "google_image"=> "",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "",
                            "login_logout"=> "",
                            "rating"=> "",
                            "status"=> "",
                            "end_lat"=>"",
                            "end_long"=>"",
                            "end_location"=>"",
                            "final_bill_amount"=> ""
                        );
                    }else{
                        $rental_ride = $this->Usermodel->rental_ride($booking_id);
                        $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                        if(!empty($rental_done_ride))
                        {
                            $begin_lat = $rental_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $rental_ride->pickup_lat;
                            }
                            $begin_long = $rental_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $rental_ride->pickup_long;
                            }
                            $begin_location = $rental_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $rental_ride->pickup_location;
                            }
                            $end_lat = $rental_done_ride->end_lat;
                            $end_long = $rental_done_ride->end_long;
                            $end_location = $rental_done_ride->end_location;
                            $final_bill_amount = $rental_done_ride->final_bill_amount;
                        }else{
                            $begin_lat = $rental_ride->pickup_lat;
                            $begin_long = $rental_ride->pickup_long;
                            $begin_location = $rental_ride->pickup_location;
                            $end_lat = "";
                            $end_long = "";
                            $end_location = "";
                            $final_bill_amount = 0;
                        }
                        $rental_ride->pickup_lat = $begin_lat;
                        $rental_ride->pickup_long = $begin_long;
                        $rental_ride->pickup_location = $begin_location;
                        $rental_ride->end_lat = $end_lat;
                        $rental_ride->end_long = $end_long;
                        $rental_ride->end_location = $end_location;
                        $rental_ride->final_bill_amount = (string)$final_bill_amount;
                        $normal_ride = array(
                            "ride_id"=>"",
                            "user_id"=> "",
                            "coupon_code"=>"",
                            "pickup_lat"=>"",
                            "pickup_long"=>"",
                            "pickup_location"=>"",
                            "drop_lat"=>"",
                            "drop_long"=>"",
                            "drop_location"=>"",
                            "ride_date"=> "",
                            "ride_time"=>"",
                            "last_time_stamp"=>"",
                            "ride_image"=>"" ,
                            "later_date"=> "",
                            "later_time"=>"",
                            "driver_id"=> "",
                            "car_type_id"=>"",
                            "ride_type"=>"",
                            "ride_status"=> "",
                            "reason_id"=> "",
                            "payment_option_id"=>"",
                            "card_id"=> "",
                            "ride_admin_status"=>"",
                            "car_type_name"=> "",
                            "car_name_arabic"=>"",
                            "car_type_name_french"=>"",
                            "car_type_image"=>"",
                            "ride_mode"=> "",
                            "car_admin_status"=>"",
                            "total_amount"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "samirgoel3@gmail.com",
                            "facebook_image"=> "facebook imgae url need to send",
                            "facebook_firstname"=> "Samir",
                            "facebook_lastname"=> "Goel",
                            "google_id"=> "112279197400670101492",
                            "google_name"=> "samir goel",
                            "google_mail"=> "samirgoel3@gmail.com",
                            "google_image"=> "google user image",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "0",
                            "login_logout"=> "1",
                            "rating"=> "4.0411764705882",
                            "status"=> "1",
                        );
                    }
                    $data[$key] = $value;
                    $data[$key]["Normal_Ride"] = $normal_ride;
                    $data[$key]["Rental_Ride"] = $rental_ride;
                }
                $this->response([
                    'status' =>1,
                    'message' => 'Ride History',
                    'details'=> $data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'Sorry You Have Not Rides'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Active_Ride_History_post(){
        $user_id = $this->post('user_id');
        if (!empty($user_id))
        {
            $data =  $this->Usermodel->user_rides($user_id);
            if(!empty($data)) {
                foreach ($data as $active_rides)
                {
                    $ride_mode = $active_rides['ride_mode'];
                    $booking_id = $active_rides['booking_id'];
                    if($ride_mode == 1)
                    {
                        $active_normal_ride = $this->Usermodel->active_normal_ride($booking_id);
                        if(!empty($active_normal_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }else{
                        $active_rental_ride = $this->Usermodel->active_rental_ride($booking_id);
                        if(!empty($active_rental_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }
                }
                if(!empty($c)){
                    foreach($c as $key=>$value){
                        $ride_mode = $value['ride_mode'];
                        $booking_id = $value['booking_id'];
                        if($ride_mode == 1)
                        {
                            $normal_ride = $this->Usermodel->normal_ride($booking_id);
                            $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                            if (!empty($normal_done_ride))
                            {
                                $done_ride_id = $normal_done_ride->done_ride_id;
                                $begin_lat = $normal_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $normal_ride->pickup_lat;
                                }
                                $begin_long = $normal_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $normal_ride->pickup_long;
                                }
                                $begin_location = $normal_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $normal_ride->pickup_location;
                                }
                                $end_lat = $normal_done_ride->end_lat;
                                if(empty($end_lat))
                                {
                                    $end_lat = $normal_ride->drop_lat;
                                }
                                $end_long = $normal_done_ride->end_long;
                                if(empty($end_long))
                                {
                                    $end_long = $normal_ride->drop_long;
                                }
                                $end_location = $normal_done_ride->end_location;
                                if(empty($end_location))
                                {
                                    $end_location = $normal_ride->drop_location;
                                }
                                $amount = $normal_done_ride->amount;
                                $waiting_price = $normal_done_ride->waiting_price;
                                $ride_time_price =  $normal_done_ride->ride_time_price;
                                $total_amount = $normal_done_ride->total_amount;
                                $coupan_price = $normal_done_ride->coupan_price;
                            }else{
                                $done_ride_id = "";
                                $begin_lat = $normal_ride->pickup_lat;
                                $begin_long = $normal_ride->pickup_long;
                                $begin_location = $normal_ride->pickup_location;
                                $end_lat = $normal_ride->drop_lat;
                                $end_long = $normal_ride->drop_long;
                                $end_location = $normal_ride->drop_location;
                                $total_amount = "0";
                            }
                            $normal_ride->done_ride_id = $done_ride_id;
                            $normal_ride->pickup_lat = $begin_lat;
                            $normal_ride->pickup_long = $begin_long;
                            $normal_ride->pickup_location = $begin_location;
                            $normal_ride->drop_lat = $end_lat;
                            $normal_ride->drop_long = $end_long;
                            $normal_ride->drop_location = $end_location;
                            $normal_ride->total_amount = (string)$total_amount;
                            $rental_ride = array(
                                "rental_booking_id"=> "",
                                "user_id"=> "",
                                "rentcard_id"=> "",
                                "car_type_id"=>"",
                                "booking_type"=>"",
                                "driver_id"=>"",
                                "pickup_lat"=> "",
                                "pickup_long"=> "",
                                "pickup_location"=> "",
                                "start_meter_reading"=>"",
                                "start_meter_reading_image"=> "",
                                "end_meter_reading"=> "",
                                "end_meter_reading_image"=> "",
                                "booking_date"=> "",
                                "booking_time"=> "",
                                "user_booking_date_time"=>"",
                                "last_update_time"=>"",
                                "booking_status"=>"",
                                "booking_admin_status"=>"",
                                "car_type_name"=>"",
                                "car_name_arabic"=> "",
                                "car_type_name_french"=> "",
                                "car_type_image"=> "",
                                "ride_mode"=>"",
                                "car_admin_status"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                                "end_lat"=>"",
                                "end_long"=>"",
                                "end_location"=>"",
                                "final_bill_amount"=> ""
                            );
                        }else{
                            $rental_ride = $this->Usermodel->rental_ride($booking_id);
                            $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                            if(!empty($rental_done_ride))
                            {
                                $begin_lat = $rental_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $rental_ride->pickup_lat;
                                }
                                $begin_long = $rental_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $rental_ride->pickup_long;
                                }
                                $begin_location = $rental_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $rental_ride->pickup_location;
                                }
                                $end_lat = $rental_done_ride->end_lat;
                                $end_long = $rental_done_ride->end_long;
                                $end_location = $rental_done_ride->end_location;
                                $final_bill_amount = $rental_done_ride->final_bill_amount;
                            }else{
                                $begin_lat = $rental_ride->pickup_lat;
                                $begin_long = $rental_ride->pickup_long;
                                $begin_location = $rental_ride->pickup_location;
                                $end_lat = "";
                                $end_long = "";
                                $end_location = "";
                                $final_bill_amount = 0;
                            }
                            $rental_ride->pickup_lat = $begin_lat;
                            $rental_ride->pickup_long = $begin_long;
                            $rental_ride->pickup_location = $begin_location;
                            $rental_ride->end_lat = $end_lat;
                            $rental_ride->end_long = $end_long;
                            $rental_ride->end_location = $end_location;
                            $rental_ride->final_bill_amount = (string)$final_bill_amount;
                            $normal_ride = array(
                                "ride_id"=>"",
                                "user_id"=> "",
                                "coupon_code"=>"",
                                "pickup_lat"=>"",
                                "pickup_long"=>"",
                                "pickup_location"=>"",
                                "drop_lat"=>"",
                                "drop_long"=>"",
                                "drop_location"=>"",
                                "ride_date"=> "",
                                "ride_time"=>"",
                                "last_time_stamp"=>"",
                                "ride_image"=>"" ,
                                "later_date"=> "",
                                "later_time"=>"",
                                "driver_id"=> "",
                                "car_type_id"=>"",
                                "ride_type"=>"",
                                "ride_status"=> "",
                                "reason_id"=> "",
                                "payment_option_id"=>"",
                                "card_id"=> "",
                                "ride_admin_status"=>"",
                                "car_type_name"=> "",
                                "car_name_arabic"=>"",
                                "car_type_name_french"=>"",
                                "car_type_image"=>"",
                                "ride_mode"=> "",
                                "car_admin_status"=>"",
                                "total_amount"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "samirgoel3@gmail.com",
                                "facebook_image"=> "facebook imgae url need to send",
                                "facebook_firstname"=> "Samir",
                                "facebook_lastname"=> "Goel",
                                "google_id"=> "112279197400670101492",
                                "google_name"=> "samir goel",
                                "google_mail"=> "samirgoel3@gmail.com",
                                "google_image"=> "google user image",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "0",
                                "login_logout"=> "1",
                                "rating"=> "4.0411764705882",
                                "status"=> "1",
                            );
                        }
                        $c[$key] = $value;
                        $c[$key]["Normal_Ride"] = $normal_ride;
                        $c[$key]["Rental_Ride"] = $rental_ride;
                    }
                    $this->response([
                        'status' =>1,
                        'message' => 'Active Ride History',
                        'details'=> $c
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' =>0,
                        'message' => 'Sorry NO Active Rides'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'Sorry You Have Not Rides'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }


}