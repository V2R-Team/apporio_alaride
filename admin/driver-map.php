<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
include('common.php');
$all_driver = array();
$query="select * from driver where  current_lat !='' and current_long !=''";
$result = $db->query($query);
$list=$result->rows;
foreach($list as $driver) {
    $driver_id = $driver['driver_id'];
    $driver_name = $driver['driver_name'];
    $current_lat = $driver['current_lat'];
    $current_long = $driver['current_long'];
    $busy = $driver['busy'];
    $driver_status =  "On Ride";
    $image = ICON2_URL;
    if ($busy == 0)
    {
        $online_offline = $driver['online_offline'];
        if ($online_offline != 1)
        {
            $driver_status =  "Offline";
            $image = ICON1_URL;
        }else{
            $driver_status =  "Online";
            $image = ICON_URL;
        }
    }

    $all_driver[] = array(
        'driver_id' =>$driver_id,
        'driver_name'=>$driver_name,
        'current_lat'=>$current_lat,
        'current_long'=>$current_long,
        'driver_status'=>$driver_status,
        'map_icon'=>$image
    );
}

?>
<style>
body{
	zoom:100% !important;
}
</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Driver Maps</h3>
        <select id="type" onchange="filterMarkers(this.value);" style="height: 50px; width: 177px;float: right;">
            <option value="">All Driver</option>
            <option value="Online">Online</option>
            <option value="Offline">Offline</option>
            <option value="On Ride">On Ride</option>
        </select>
        <nav id="sidebar-wrapper" class="">
            <div id="driver_details">
                <ul class="sidebar-nav ">
                    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i style="color:#ffffff;" class="fa fa-times"></i></a>
                    <li style="clear:both;"></li>

                </ul>
            </div>
        </nav>

    </div>
    <div class="row">
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $admin_settings['admin_panel_map_key']?>"></script>
        <div class="form-group ">
            <div class="col-lg-12">

            </div>
        </div>

        <div id="map" style="width:100%;height:550px;  "></div>

        <script type="text/javascript">
            var gmarkers1 = [];
            var markers1 = [];
            var infowindow = new google.maps.InfoWindow({
                content: ''
            });

            // Our markers
            markers1 = [
                <?php
                foreach($all_driver as $driver_map) { ?>
                ['<?php echo $driver_map['driver_id'] ?>','<?php echo $driver_map['driver_name']?>', <?php echo $driver_map['current_lat'] ?>,<?php echo $driver_map['current_long'] ?>,'<?php echo $driver_map['driver_status'] ?>','<?php echo $driver_map['map_icon'] ?>','<div style=" text-transform: capitalize; cursor:pointer;" class="map_details_click" onclick="showmapdetails(<?php echo $driver_map["driver_id"] ?>)" ><?php echo $driver_map['driver_name'] ?></div>'],
                <?php };?>
            ];


            function initialize() {
                var center = new google.maps.LatLng(<?php echo $admin_settings['admin_panel_latitude']?>, <?php echo $admin_settings['admin_panel_longitude']?>);
                var mapOptions = {
                    zoom: 4,
                    center: center,
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                };

                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                for (i = 0; i < markers1.length; i++) {
                    addMarker(markers1[i]);
                }
            }

            /**
             * Function to add marker to map
             */

            function addMarker(marker) {
                var category = marker[4];
                var title = marker[1];
                var pos = new google.maps.LatLng(marker[2], marker[3]);
                var content = marker[6];
                var icon = marker[5];
                marker1 = new google.maps.Marker({
                    title: title,
                    position: pos,
                    category: category,
                    icon: icon,
                    map: map
                });

                gmarkers1.push(marker1);

                // Marker click listener
                google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                    return function () {
                        infowindow.setContent(content);
                        infowindow.open(map, marker1);
                        map.panTo(this.getPosition());
                        map.setZoom(15);
                    }
                })(marker1, content));
            }

            /**
             * Function to filter markers by category
             */

            filterMarkers = function (category) {
                for (i = 0; i < markers1.length; i++) {
                    marker = gmarkers1[i];
                    // If is same category or category not picked
                    if (marker.category == category || category.length === 0) {
                        marker.setVisible(true);
                    }
                    // Categories don't match
                    else {
                        marker.setVisible(false);
                    }
                }
            }

            // Init map
            initialize();

        </script>


        <script>

            function showmapdetails(driver_id) {
                $('#sidebar-wrapper').addClass("active");
                $.ajax({
                    url: "ajax.php?action=driver_details1&driver_id="+driver_id,
                    success: function(res) {
                        $('#driver_details').html(res);
                    }
                });
            }

            function showmapdetails1(driver_id) {
                $('#sidebar-wrapper').addClass("active");
                $.ajax({
                    url: "ajax.php?action=driver_details1_ride&driver_id="+driver_id,
                    success: function(res) {
                        $('#driver_details').html(res);
                    }
                });
            }


        </script>
    </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>