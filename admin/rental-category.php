<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
        $query="select * from rental_category";
	$result = $db->query($query);
	$list=$result->rows;
        
        	
    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE rental_category SET rental_category_admin_status='".$_GET['status']."' WHERE rental_category_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=rental-category");
    }


if(isset($_POST['savechanges'])) 
     {
       $query2="UPDATE rental_category SET rental_category='".$_POST['rental_category']."',rental_category_hours='".$_POST['rental_category_hours']."',rental_category_kilometer='".$_POST['rental_category_kilometer']."' where rental_category_id='".$_POST['savechanges']."'";
       $db->query($query2); 
       $db->redirect("home.php?pages=rental-category");
     }

//delete

if(isset($_POST['delete']))
{
    $delqry1="DELETE from rental_category where rental_category_id='".$_POST['delete']."'";
    $db->query($delqry1);
    $db->redirect("home.php?pages=rental-category");
}
    
?>


<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Rental Packages</h3>
      <span class="tp_rht">
            <a href="home.php?pages=add-rental-category" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Rental Package"><i class="fa fa-plus"></i></a>
      </span>
      </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>

                    <th>Packages Name</th>
                      <th>Travel Hours</th>
                      <th>Travel Kilometre</th>
                    <th width="8%">Status</th>
                    <th width="4%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 

                  foreach($list as $rent){?>
                  <tr>

                    <td>
                      <?php
                    	$rental_category = $rent['rental_category'];
				        echo $rental_category;
                      ?>
                    </td>
                      <td>
                          <?php
                          $rental_category_hours = $rent['rental_category_hours'];
                          echo $rental_category_hours." Hours";
                          ?>
                      </td>
                      <td>
                          <?php
                          $rental_category_kilometer = $rent['rental_category_kilometer'];
                          $rental_category_distance_unit = $rent['rental_category_distance_unit'];
                          echo $rental_category_kilometer." ".$rental_category_distance_unit;
                          ?>
                      </td>
                    <?php
                                if($rent['rental_category_admin_status']==1) {
                                ?>
                                <td class="">
				    <label class="label label-success" > Active</label>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="">
                                    <label class="label label-default" > Deactive</label> 
                                </td>
                            <?php } ?>
 <td>
     <div class="row " style="width:70px;">
                                                
                                            <span data-target="#<?php echo $rent['rental_category_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>

         <span data-target="#delete<?php echo $rent['rental_category_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>


     </div> </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>

<?php foreach($list as $rent){?>
<div class="modal fade" id="<?php echo $rent['rental_category_id'];?>" role="dialog">
  <div class="modal-dialog">
      <form method="post" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Rent Category</h4>
      </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Rent Package Name</label>
                <input type="text" class="form-control"  placeholder="Rent Category Name" name="rental_category" value="<?php echo $rent['rental_category'];?>" id="rental_category" required>
              </div>
            </div>
          </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Travel Hours</label>
                        <input type="text" class="form-control"  placeholder="Travel Hours" name="rental_category_hours" value="<?php echo $rent['rental_category_hours'];?>" id="rental_category_hours" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Travel Kilometre</label>
                        <input type="text" class="form-control"  placeholder="Travel Kilometre" name="rental_category_kilometer" value="<?php echo $rent['rental_category_kilometer'];?>" id="rental_category_kilometer" required>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $rent['rental_category_id'];?>" class="btn btn-info">Save Changes</button>
        </div>

    </div>
      </form>
  </div>
</div>
<?php }?>



<?php
foreach($list as $Delrental){ ?>
    <div class="modal fade" id="delete<?php echo $Delrental['rental_category_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete</h4>
                </div>

                    <div class="modal-body">
                        <div class="row">
                            <h4>Do You Really Want To Delete This Category?</h4></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $Delrental['rental_category_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>

        </div>
    </div>
    </div>
<?php } ?>
<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>