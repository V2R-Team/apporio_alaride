<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');

$query1="select * from table_documents";
$result1 = $db->query($query1);
$list=$result1->rows;
if(isset($_POST['savechanges']))
{
    $query2="UPDATE table_documents SET document_name='".$_POST['doc_name']."' where document_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=documents");
}
?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Documents</h3>
        <span class="tp_rht">
                <a href="home.php?pages=add-document" data-toggle="tooltip" title="Add Documents" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
                <!--<a href="home.php?pages=add-document" class="btn btn-default btn-lg" id="add-button"  role="button">Add Document</a>-->
      </span>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Document Name</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach($list as $document){?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td>
                                            <?php
                                            $document_name = $document['document_name'];
                                            echo $document_name;
                                            ?>
                                        </td>
                                        <td>
                                            <span data-target="#<?php echo $document['document_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                        </td>
                                    </tr>
                                <?php
                                    $i++;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php foreach($list as $document){?>
    <div class="modal fade" id="<?php echo $document['document_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Document Name</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Name</label>
                                    <input type="text" class="form-control"  placeholder="Name" name="doc_name" value="<?php echo $document['document_name'];?>" id="doc_name" required>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $document['document_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php }?>
</section>
<!-- Main Content Ends -->
</body></html>