<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id WHERE ride_platform=2 ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key=>$value)
{
    $driver_id = $value['driver_id'];
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];

    if($driver_id == 0)
    {
        $driver_name = "";
        $driver_email = "";
        $driver_phone = "";
    }else{
        $query1 = "select * from driver where driver_id ='$driver_id'";
        $result1 = $db->query($query1);
        $list1 = $result1->row;
        $driver_name = $list1['driver_name'];
        $driver_email = $list1['driver_email'];
        $driver_phone = $list1['driver_phone'];
    }
    $list[$key]=$value;
    $list[$key]["driver_name"] = $driver_name;
    $list[$key]["driver_email"] = $driver_email;
    $list[$key]["driver_phone"] = $driver_phone;
    $list[$key]["payment_option_name"]=$payment_option_name;
}
?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Manual Dispatch Rides</h3>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl" style="height: 300px">
                        <table id="datatable" class="table table-striped table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th width="5">Sr.No</th>
                                <th width="5">Ride Id</th>
                                <th width="5">Rider Details</th>
                                <th width="5">Driver Details</th>
                                <th>Pickup Address</th>
                                <th>Drop Address</th>
                                <th width="">Payment Mode</th>
                                <th width="10%">Ride booked time</th>
                                <th width="5%">Current Status</th>
                                <th width="8%">Ride Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $a = 1;
                            foreach($list as $ridenow){
                                ?>
                                <tr>
                                    <td> <?php echo $a;?></td>
                                    <td><a href="home.php?pages=trip-details&id=<?=$ridenow['ride_id']?>" ><span title="Full Details" class="bookind-id"> <?=$ridenow['ride_id']?> </span></a></td>
                                    <td><?php
                                        $user_name = $ridenow['user_name'];
                                        $user_phone = $ridenow['user_phone'];
                                        $user_email = $ridenow['user_email'];
                                        echo nl2br($user_name."\n".$user_phone."\n".$user_email);
                                        ?></td>
                                    <td>
                                        <?php  $driver_name = $ridenow['driver_name'];
                                        if($driver_name == "")
                                        { ?>
                                            <h4 style="color:red;">Not Assign</h4>

                                        <?php }else{
                                            echo nl2br($ridenow['driver_name']."\n".$ridenow['driver_phone']."\n".$ridenow['driver_email']);
                                        } ?></td>

                                    <td>
                                        <?php
                                        $pickup_location = $ridenow['pickup_location'];
                                        echo $pickup_location;
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                        $drop_location = $ridenow['drop_location'];
                                        echo $drop_location;
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                        $payment_option_name = $ridenow['payment_option_name'];
                                        echo $payment_option_name;
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                        $ride_date = $ridenow['ride_date'];
                                        echo $ride_date.",".$ridenow['ride_time'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $ride_status = $ridenow['ride_status'];
                                        $timestap = $ridenow['last_time_stamp'];
                                        switch ($ride_status){
                                            case "1":
                                                echo nl2br("New Booking \n ".$timestap);
                                                break;
                                            case "2":
                                                echo nl2br("Cancelled By User  \n ".$timestap);
                                                break;
                                            case "3":
                                                echo nl2br("Accepted by Driver  \n ".$timestap);
                                                break;
                                            case "4":
                                                echo nl2br("Cancelled by driver  \n ".$timestap);
                                                break;
                                            case "5":
                                                echo nl2br("Driver Arrived  \n ".$timestap);
                                                break;
                                            case "6":
                                                echo nl2br("Trip Started  \n ".$timestap);
                                                break;
                                            case "7":
                                                echo nl2br("Trip Completed  \n ".$timestap);
                                                break;
                                            case "8":
                                                echo nl2br("Trip Book By Admin  \n ".$timestap);
                                                break;
                                            case "17":
                                                echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                break;
                                            default:
                                                echo "----";
                                        }
                                        ?>
                                    </td>

                                    <td align="center">
                                        <?php
                                        $ride_status = $ridenow['ride_status'];
                                        switch ($ride_status)
                                        {
                                            case "1" :
                                                ?>
                                                <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                <?php
                                                break;
                                            case "2" :
                                                echo "Ride Cancel By User";
                                                break;
                                            case "3": ?>
                                                <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                <a target="_blank" href="home.php?pages=track-ride&id=<?=$ridenow['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
                                                <?php break;
                                            case "4":
                                                echo "Cancelled by driver";
                                                break;
                                            case "5": ?>
                                        <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                        <a target="_blank" href="home.php?pages=track-ride&id=<?=$ridenow['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
                                      <?php break;
                                            case "6": ?>
                                                <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                             <a target="_blank" href="home.php?pages=track-ride&id=<?=$ridenow['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
                                      <?php break;
                                            case "7":
                                                echo "Trip End";
                                                break;
                                        }
                                      ?>
                                    </td>
                                </tr>
                                <?php
                                $a++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
