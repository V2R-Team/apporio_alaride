<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
if (isset($_GET['driver_id'])){
    $driver_id = $_GET['driver_id'];
    $query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id WHERE driver.driver_id='$driver_id'";
    $result = $db->query($query);
    $list=$result->row;
    $total_document_need = $list['total_document_need'];
    $verfiy_document = $list['verfiy_document'];
    $query = "select * from table_driver_document INNER JOIN table_documents ON table_driver_document.document_id=table_documents.document_id WHERE table_driver_document.driver_id='$driver_id'";
    $result = $db->query($query);
    $document = $result->rows;
}
if (isset($_GET['driver_document_id']) && isset($_GET['driver_id']))
{

    $driver_id =  $_GET['driver_id'];
    $driver_document_id = $_GET['driver_document_id'];
    $query="select * from driver WHERE driver_id='$driver_id'";
    $result = $db->query($query);
    $list=$result->row;
    $verfiy_document = $list['verfiy_document']+1;
    $query2="UPDATE driver SET verfiy_document='$verfiy_document' where driver_id='$driver_id'";
    $db->query($query2);
    $query2="UPDATE table_driver_document SET documnet_varification_status=2 where driver_document_id='$driver_document_id'";
    $db->query($query2);
    $db->redirect("home.php?pages=verify-driver&driver_id=$driver_id");
}
?>
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Pending Approval</h3>
      <span class="tp_rht">
         <a href="home.php?pages=pending-driver-approvals" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>	
    </div>
    <div class="row">

        <div class="panel panel-default">
          <div class="panel-body">
          	<div class="col-md-12">
	          <div class="col-md-4">
			
	                  <div class="bs-example" data-example-id="simple-jumbotron">
	                  <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:2px;">
		                    <strong>PERSONAL DETAILS</strong> 
		                  </div>
		            <div class="alert alert-info alert-dismissible" role="alert" style=" height:145px;">
		                    <table width="100%" style="font-size:16px;">
		                  	<tbody>
		                  		<tr>
		                  			<td width="30%"><b>Name</b></td>
		                  			<td width="50%"><?= $list['driver_name'];?></td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>Email Id</b></td>
		                  			<td width="50%">
                                        <?php
                                        $driver_email = $list['driver_email'];
                                        echo $driver_email;
                                    ?></td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>Phone No.</b></td>
		                  			<td width="50%">
                                        <?php
                                        $driver_phone = $list['driver_phone'];
                                        echo $driver_phone;
                                        ?>
                                        </td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>City</b></td>
		                  			<td width="50%"><?= $list['city_name'];?></td>
		                  		</tr>
		                  	</tbody>
		                  </table>	
		           </div>      
	                  </div>
	                 
		  </div>	
		  <div class="col-md-4" >
			
	                  <div class="bs-example" data-example-id="simple-jumbotron">
	                  <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:2px;">
		                    <strong>VEHICLE DETAILS</strong> 
		                  </div>
	                    <div class="alert alert-info alert-dismissible" role="alert" style=" height:145px;">
				 <table width="100%" style="font-size:16px;">
		                  	<tbody>
		                  		<tr>
		                  			<td width="40%"><b>Car Type</b></td>
		                  			<td width="50%"><?= $list['car_type_name']?></td>
		                  		</tr>
		                  		<tr>
		                  			<td width="40%"><b>Car Model</b></td>
		                  			<td width="50%"><?= $list['car_model_name']?></td>
		                  		</tr>

		                  	</tbody>
		                  </table>	
	                    </div>
	                  </div>
	                 
		  </div>	

		   <div class="row">
		     <div class="col-md-12 col-sm-12 col-xs-12">
		     <div class="col-md-4">
		        <div class="alert alert-success alert-dismissible col" role="alert" style="margin-bottom:2px;">
		              <strong>UPLOADED DOCUMENTS</strong> 
		        </div>
		      </div>  
		     </div>	
	  	     <div class="col-md-12 col-sm-12 col-xs-12">
	          	<div class="col-sm-4 col-md-4"> 

		          <table class="table table-striped table-bordered table-responsive dataTable no-footer" role="grid" aria-describedby="datatable_info">
		          	<thead>

		          	    <tr role="row">
		          		<th>Sr. No.</th>
		          		<th>Document Name</th>
                        <th>Expiry Date</th>
		          		<th>Link</th>
		          		<th>Action</th>
		                    </tr?		
		          	</thead>
		          	<tbody>
                    <?php
                    $j = 1;
                    foreach($document as $driver_documnets): ?>
		          	    <tr role="row">
		          		<td><?= $j;?></td>
		          		<td><?= $driver_documnets['document_name'];?></td>
                        <td>  <?= $driver_documnets['document_expiry_date'];?></td>
		          		<td><a target="_blank" href="<?php echo '../'.$driver_documnets['document_path'];?>"><i class="fa fa-file" aria-hidden="true" target="_blank"></i> <em>file</em></a></td>
		          		<td>
                            <?php if($driver_documnets['documnet_varification_status'] == 1) {?>
                            <a href="home.php?pages=verify-driver&driver_id=<?php echo $driver_documnets['driver_id']; ?>&driver_document_id=<?php echo $driver_documnets['driver_document_id']; ?>"><label class="btn btn-success br2 btn-xs fs12 activebtn"> verify </label></a>
                        <?php }elseif ($driver_documnets['documnet_varification_status'] == 2){
                                echo "verified";
                            }else{ ?>
                                <a href="home.php?pages=verify-driver&driver_id=<?php echo $driver_documnets['driver_id']; ?>&driver_document_id=<?php echo $driver_documnets['driver_document_id']; ?>"><label class="btn btn-success br2 btn-xs fs12 activebtn"> verify </label></a>
                            <?php }?>
                        </td>

                        </tr>
                        <?php endforeach;?>
		          	<tbody>
		          </table>
	          	</div>
	       	     </div>
      	         </div>
		  <div class="form-group">
                        <div class="clearfix"></div><br>
                        <a class="col-md-5 col-sm-5 col-xs-12">
                          <div class="clearfix"></div><br>
                            <?php if($total_document_need == $verfiy_document){ ?>
                                <a href="home.php?pages=pending-driver-approvals&status=1&driver_id=<?php echo $list['driver_id']; ?>"<button type="button" class="btn btn-success" style="margin-right:3px;">Verify</button></a>
              <a href="home.php?pages=pending-driver-approvals&status=2&driver_id=<?php echo $list['driver_id']; ?>"<button type="button" class="btn btn-warning">Reject</button></a>
                  <?php }else{ ?>
                      <a href="home.php?pages=pending-driver-approvals&status=2&driver_id=<?php echo $list['driver_id']; ?>"<button type="button" class="btn btn-warning">Reject</button></a>
                          <?php  }?>

                    </div>
                  </div>	
          </div>

          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
    <!-- End row --> 
    
  </div>
  
</div>
<!-- popup -->

    <div class="modal fade" id="pendingapproval" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Notify Driver</h4>
                    <div class="col-md-12">
 			<p>Welcome to apporio taxi panel</p>
 			 <button type="button" class="btn btn-success" style="margin-right:3px;" >Skip</button>
                         <button type="button" class="btn btn-warning" >Send Mail</button>
 		</div>
                </div>
 		
            </div>

        </div>
    </div>

<!-- Main Content Ends -->

</body>
</html>
