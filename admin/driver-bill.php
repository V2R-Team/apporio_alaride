<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
if(isset($_POST['paid'])){
    $driver_id = $_GET['id'];
    $date = date("Y-m-d");
    $query1 = "UPDATE table_driver_bill SET bill_status=1,bill_settle_date='$date' WHERE bill_id='".$_POST['paid']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=driver-bill&id=$driver_id");
}
$driver_id = $_GET['id'];
$query ="select * from table_driver_bill WHERE driver_id='$driver_id' ORDER BY bill_id DESC";
$result = $db->query($query);
$list = $result->rows;
$query = "select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id where driver.driver_id='$driver_id'";
    $result = $db->query($query);
    $list1=$result->row;
    $driver_name = $list1['driver_name'];
?>
<div class="wraper container-fluid">
    <div class="row-fluid user-infos alexanderMahrt">
        <div class="span10 offset1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Driver information</h3>
                </div>
                <div class="panel-body">
                    <div class="row-fluid">
                        <div class="span3" style="width: 350px;float: left;">
                            <img class="img-circle"
                                 src="<?php echo '../' . $list1['driver_image']; ?>" alt="Driver Pic" height="200px" width="200px">
                        </div>
                        <div class="span6" style="width: 700px;float: left;">
                            <table class="table table-condensed table-responsive table-user-information">
                                <tbody>
                                <tr>
                                    <td><strong>Driver Name:</strong></td>
                                    <td><?php
                                        $driver_name = $list1['driver_name'];
                                        echo $driver_name;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Driver Phone:</strong></td>
                                    <td> <?php
                                        $driver_phone = $list1['driver_phone'];
                                        echo $driver_phone;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>City:</strong></td>
                                    <td><?php
                                        $city_name = $list1['city_name'];
                                        echo $city_name;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Car Type:</strong></td>
                                    <td><?php
                                        $car_type_name = $list1['car_type_name'];
                                        echo $car_type_name;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Car Model:</strong></td>
                                    <td><?php
                                        $car_model_name = $list1['car_model_name'];
                                        echo $car_model_name;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Car Number:</strong></td>
                                    <td><?php
                                        $carnumber = $list1['car_number'];
                                        echo $carnumber;
                                        ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-title">
        <h3 class="title">Bills</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Billed Date</th>
                                    <th>Bill Period</th>
                                    <th>Bill Amount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($list as $driver_bil){?>
                                    <tr>
                                        <td><?php
                                            $bill_to_date = $driver_bil['bill_to_date'];
                                            $bill_to_date = date("d/m/Y", strtotime($bill_to_date));
                                            echo $bill_to_date;
                                            ?></td>
                                        <td><?php
                                            $bill_from_date = date("d/m/Y", strtotime($driver_bil['bill_from_date']));
                                            $bill_to_date = date("d/m/Y", strtotime( $driver_bil['bill_to_date']));
                                                echo $bill_from_date." To ".$bill_to_date;
                                            ?></td>

                                        <td><?php
                                            $outstanding_amount = $driver_bil['outstanding_amount'];
                                            echo $outstanding_amount;
                                            ?></td>
                                        <td><?php
                                                $bill_status = $driver_bil['bill_status'];
                                                if($bill_status == 1)
                                                {
                                                    echo "Settled";
                                                }else{
                                                    echo "Unsettled";
                                                }
                                            ?></td>
                                        <td>
                                       <?php
                                            $bill_status = $driver_bil['bill_status'];
                                            if ($bill_status == 0) { ?>
                                               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo $driver_bil['bill_id'];?>">Settle</button>
                                            <?php } else {
                                                echo "Settled On ".date("d/m/Y", strtotime($driver_bil['bill_settle_date']));
                                            }?>
                                            <a href="home.php?pages=driver-transactions&bill_id=<?php echo $driver_bil['bill_id'];?>" target="_blank"><button type="button" class="btn btn-success">View Transactions</button></a>
                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach($list as $driver_bil){ ?>
    <div class="modal fade" id="<?php echo $driver_bil['bill_id'];?>" role="dialog">
        <div class="modal-dialog">
<form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Bill Setlle </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h3>Are You Really Want To Settle This Bill</h3></div>
                    <div class="modal-footer">
                        <button type="submit" name="paid" value="<?php echo $driver_bil['bill_id'];?>" class="btn btn-danger">Settled</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
</form>
        </div>
    </div>
    </div>
<?php } ?>
</section>
</body></html>