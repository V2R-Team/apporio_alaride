<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
if (isset($_GET['status'])){
    $query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id WHERE driver.verification_status='".$_GET['status']."'";
    $result = $db->query($query);
    $list=$result->rows;
    if(!empty($list)){
        require_once 'PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Id');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Driver Email');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Driver Phone');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Commission');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'City');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Car Type Name');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Car Model Name');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Register Date');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Register Date');
        $row = 2;
        foreach($list as $value)
        {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['driver_id']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['driver_email']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['driver_phone']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['commission']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['city_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['car_type_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['car_model_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['register_date']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $value['current_location']);
            $row++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=driver.xlsx");
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

    }else{
        echo '<script type="text/javascript">alert("No Data For Export")</script>';
    }
}
?>