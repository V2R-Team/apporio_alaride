<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

if(isset($_POST['save']))
{
    $query="select * from admin WHERE admin_username='".$_POST['admin_username']."'";
    $result = $db->query($query);
    $list=$result->row;
    if (empty($list))
    {
        $query2="INSERT INTO admin (admin_fname,admin_lname,admin_email,admin_phone,admin_username,admin_password,admin_role) 
    VALUES ('".$_POST['admin_fname']."','".$_POST['admin_lname']."','".$_POST['admin_email']."','".$_POST['admin_phone']."','".$_POST['admin_username']."','".$_POST['admin_password']."','".$_POST['admin_role']."')";
        $db->query($query2);
        $msg = "Admin Details Save Successfully";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-admin");
    }else{
        $msg = "Username Already Exit";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    }

}

?>

<script>
    function validatelogin() {
        if(document.getElementById('admin_fname').value == "")
        {
            alert("Enter First Name");
            return false;
        }
        if(document.getElementById('admin_lname').value == "")
        {
            alert("Enter last Name");
            return false;
        }
        if(document.getElementById('admin_email').value == "")
        {
            alert("Enter Email");
            return false;
        }
        if(document.getElementById('admin_phone').value == "")
        {
            alert("Enter Mobile Number");
            return false;
        }
        if(document.getElementById('admin_username').value == "")
        {
            alert("Enter Login username");
            return false;
        }
        if(document.getElementById('admin_password').value == "")
        {
            alert("Enter Password");
            return false;
        }
        if(document.getElementById('admin_role').value == "")
        {
            alert("Select Admin Group");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Admin</h3>

        <span class="tp_rht">
         <a href="home.php?pages=admin" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">First Name *</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="First Name" name="admin_fname" id="admin_fname" >
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Last Name *</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Last Name" name="admin_lname" id="admin_lname" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Email *</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Email" name="admin_email" id="admin_email" >
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Phone *</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="phone" name="admin_phone" id="admin_phone" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Login Username *</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Login Username" name="admin_username" id="admin_username" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Password *</label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control"  placeholder="Password" name="admin_password" id="admin_password" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Group *</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="admin_role" id="admin_role" >
                                        <option value="">-- Select --</option>
                                        <option value="1">Super Administrator</option>
                                        <option value="2">Dispatcher Admin</option>
                                        <option value="3">Billing Admin</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
