<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$query = "select * from rental_booking INNER JOIN user ON rental_booking.user_id=user.user_id INNER JOIN car_type ON rental_booking.car_type_id=car_type.car_type_id where rental_booking.booking_type=1 ORDER BY rental_booking.rental_booking_id DESC LIMIT 10";
$result = $db->query($query);
$list=$result->rows;
foreach ($list as $key=>$value)
{
    $driver_id = $value['driver_id'];
    if($driver_id == 0)
    {
        $driver_name = "";
    }else{
        $query1 = "select * from driver where driver_id ='$driver_id'";
        $result1 = $db->query($query1);
        $list1 = $result1->row;
        $driver_name = $list1['driver_name'];
    }
    $list[$key]=$value;
    $list[$key]["driver_name"] = $driver_name;
}
$query = "select * from rental_booking INNER JOIN user ON rental_booking.user_id=user.user_id INNER JOIN car_type ON rental_booking.car_type_id=car_type.car_type_id where rental_booking.booking_type=2 ORDER BY rental_booking.rental_booking_id DESC LIMIT 10";
$result = $db->query($query);
$ride_later=$result->rows;
foreach ($ride_later as $key=>$value)
{
    $driver_id = $value['driver_id'];
    if($driver_id == 0)
    {
        $driver_name = "";
    }else{
        $query1 = "select * from driver where driver_id ='$driver_id'";
        $result1 = $db->query($query1);
        $list1 = $result1->row;
        $driver_name = $list1['driver_name'];
    }
    $ride_later[$key]=$value;
    $ride_later[$key]["driver_name"] = $driver_name;
}
if(isset($_GET['driver_id']) && isset($_GET['ride_id']) && isset($_GET['status'])){
    $driver_id = $_GET['driver_id'];
    $rental_booking_id = $_GET['ride_id'];
    $time1 = date("h:i:s A");
    $query1="UPDATE rental_booking SET last_update_time='$time1',booking_status=10 WHERE rental_booking_id='$rental_booking_id'";
    $db->query($query1);
    $query1234="select * from driver where driver_id='$driver_id'";
    $result1234 = $db->query($query1234);
    $list1234=$result1234->row;
    $device_id=$list1234['device_id'];
    $flag=$list1234['flag'];
    $query5="INSERT INTO booking_allocated (rental_booking_id,driver_id) VALUES ('$rental_booking_id','$driver_id')";
    $db->query($query5);
    $message = "New Rentel Booking";
    $ride_id= (String) $rental_booking_id;
    $ride_status= (String) 10;
    if($device_id!="")
    {
        if($flag == 1)
        {
           IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status);
        }
        else
        {
            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        }
    }
    $url = 'https://apporio-taxi.firebaseio.com/Activeride/'.$driver_id.'/.json';
    $fields = array(
        'ride_id' => (string)$ride_id,
        'ride_status'=>"10",
    );

    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
    $response = curl_exec($ch);
    $db->redirect("home.php?pages=rental-ride");
}
$sql = "SELECT * FROM `driver` WHERE `driver_admin_status`=1 AND `online_offline`=1 AND`busy`=0 AND `login_logout`=1";
$result3=$db->query($sql);
$drivers=$result3->rows;
$query1234 ="select * from cancel_reasons where cancel_reasons_status=1 AND reason_type=3";
$result1234 = $db->query($query1234);
$list123=$result1234->rows;
?>

<style>
    .clear{clear:both !important;}
    .bookind-id {
        cursor: pointer;
        text-decoration: underline;
    }

</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Rental Rides</h3>
    </div>
    <div class="row m-t-30">
        <div class="col-sm-12">
            <ul class="nav-tabs navi navi_tabs">
                <li class="active"><a data-toggle="tab" href="#personal"><i class="fa fa-car" aria-hidden="true"></i>
                        <b>Ride Now Rental Bookings</b></a></li>
                <li class=""><a data-toggle="tab" href="#address"><i class="fa fa-clock-o" aria-hidden="true"></i> <b>Ride Later Rental Bookings</b></a></li>
            </ul>
            <div class="panel panel-default p-0">

                <div class="panel-body p-0">

                    <div class="tab-content m-0">
                        <div id="personal" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table id="datatable1" class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="46px">URN&nbsp;&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Unique Reference Number"></i></th>
                                                <th width="5">Rider Name</th>
                                                <th width="5">Driver</th>
                                                <th>Car Type</th>
                                                <th>Pickup Address</th>
                                                <th width="10%">Booking Time</th>
                                                <th width="5%">Current Status</th>
                                                <th width="8%">Ride Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($list as $rental){?>
                                                <tr>
                                                <td><a href="home.php?pages=rental_trip-details&id=<?=$rental['rental_booking_id']?>" ><span title="Full Details" class="bookind-id"> <?php echo $rental['rental_booking_id'];?> </span></a></td>
                                                    <td><?php $user_name = $rental['user_name'];
                                                        echo $user_name;
                                                        ?></td>
                                                    <td>
                                                        <?php  $driver_name = $rental['driver_name'];
                                                        if($driver_name == "")
                                                        { ?>
                                                            <h4 style="color:red;">Not Assign</h4>

                                                        <?php }else{
                                                            echo $driver_name;
                                                        } ?></td>
                                                    <td>
                                                        <?php
                                                        $car_type_name = $rental['car_type_name'];
                                                        echo $car_type_name;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $pickup_location = $rental['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>


                                                    <td>
                                                        <?php
                                                        $ride_date = $rental['booking_date'];
                                                        echo $ride_date.",".$rental['booking_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $rental['booking_status'];
                                                        $timestap = $rental['last_update_time'];
                                                        switch ($ride_status){
                                                            case "10":
                                                                echo nl2br("New Booking \n ".$timestap);
                                                                break;
                                                            case "11":
                                                                echo nl2br("Accepted by Drive  \n ".$timestap);
                                                                break;
                                                            case "12":
                                                                echo nl2br("Driver Arrived  \n ".$timestap);
                                                                break;
                                                            case "13":
                                                                echo nl2br("Trip Started  \n ".$timestap);
                                                                break;
                                                            case "14":
                                                                echo nl2br("Trip Reject By Driver  \n ".$timestap);
                                                                break;
                                                            case "15":
                                                                echo nl2br("Trip Cancel By User  \n ".$timestap);
                                                                break;
                                                            case "16":
                                                                echo nl2br("Trip Completed  \n ".$timestap);
                                                                break;
                                                            case "19":
                                                                echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                                break;
                                                        }
                                                        ?>
                                                    </td>

                                                    <td align="center">
                                                        <span data-target="#cancel<?php echo $rental['rental_booking_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="clear"></div>

                        <div id="address" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table id="datatable" class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="46px">URN&nbsp;&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Unique Reference Number"></i></th>
                                                <th width="5">Rider Name</th>
                                                <th width="5">Driver</th>
                                                <th>Car Type</th>
                                                <th>Pickup Address</th>
                                                <th width="10%">Booking Time</th>
                                                <th width="5%">Current Status</th>
                                                <th width="8%">Ride Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($ride_later as $rental_later){?>
                                                <tr>
                                                 <td><a href="home.php?pages=rental_trip-details&id=<?=$rental_later['rental_booking_id']?>" ><span title="Full Details" class="bookind-id"> <?php echo $rental_later['rental_booking_id'];?> </span></a></td>
                                                    <td><?php $user_name = $rental_later['user_name'];
                                                        echo $user_name;
                                                        ?></td>
                                                    <td><?php  $driver_name = $rental_later['driver_name'];
                                                        if($driver_name == "")
                                                        { ?>
                                                            <button  class="btn btn-success"  data-target="#DriverAssign<?php echo $rental_later['rental_booking_id'];?>" data-toggle="modal" >Assign</button>
                                                        <?php }else{
                                                            echo $driver_name;
                                                        } ?>
                                                        </td>
                                                    <td>
                                                        <?php
                                                        $car_type_name = $rental_later['car_type_name'];
                                                        echo $car_type_name;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $pickup_location = $rental_later['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_date = $rental_later['booking_date'];
                                                        echo $ride_date.",".$rental_later['booking_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $rental_later['booking_status'];
                                                        $timestap = $rental_later['last_update_time'];
                                                        switch ($ride_status){
                                                            case "10":
                                                                echo nl2br("New Booking \n ".$timestap);
                                                                break;
                                                            case "11":
                                                                echo nl2br("Accepted by Drive  \n ".$timestap);
                                                                break;
                                                            case "12":
                                                                echo nl2br("Driver Arrived  \n ".$timestap);
                                                                break;
                                                            case "13":
                                                                echo nl2br("Trip Started  \n ".$timestap);
                                                                break;
                                                            case "14":
                                                                echo nl2br("Trip Reject By Driver  \n ".$timestap);
                                                                break;
                                                            case "15":
                                                                echo nl2br("Trip Cancel By User  \n ".$timestap);
                                                                break;
                                                            case "16":
                                                                echo nl2br("Trip Completed  \n ".$timestap);
                                                                break;
                                                            case "19":
                                                                echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                                break;
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <span data-target="#ridelatercancel<?php echo $rental_later['rental_booking_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>

                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Assign Driver-->
<?php foreach($ride_later as $rental_later){?>
    <div class="modal fade"  id="DriverAssign<?php echo $rental_later['rental_booking_id'];?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Driver for Ride Id #<?php echo $rental_later['rental_booking_id'];?></h4>
                    <div id="assign_driver_success" style="display:none;"> <br>
                        <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $rental_later['rental_booking_id'];?> </div>
                    </div>
                </div>
                <div class="modal-body" >
                    <div class="tab-content">
                        <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                <tr>
                                    <th>Driver Name</th>
                                    <th>Assign</th>
                                </tr>
                                <?php foreach($drivers as $login) { ?>
                                    <tr>
                                        <td><?php echo $login['driver_name'];?></td>
                                        <td><a href="home.php?pages=rental-ride&status=11&driver_id=<?php echo $login['driver_id']?>&ride_id=<?php echo $rental_later['rental_booking_id'];?>" class="" title="Active">
                                                <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" >Assign</button>
                                            </a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($ride_later as $rental_later){?>
    <div class="modal fade"  id="ridelatercancel<?php echo $rental_later['rental_booking_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cancel Ride #<?php echo $ride['ride_id']; ?></h4>
                </div>
                <form method="get" action="rental_cancel_booking.php">
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tbody>
                                    <?php
                                    foreach($list123 as $reasons){?>
                                        <tr><td><input type="radio" name="cancel_reason_id" value="<?php echo $reasons['reason_id']; ?>"> &nbsp; <?php echo $reasons['reason_name']; ?><br></td></tr>
                                    <?php } ?>
                                    <input type="hidden" name="rental_booking_id" value="<?php echo $rental_later['rental_booking_id'];?>">
                                  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success br2 btn-xs fs12 activebtn" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($list as $rental) { ?>
    <div class="modal fade"  id="cancel<?php echo $rental['rental_booking_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cancel Ride #<?php echo $ridenow['ride_id']; ?></h4>
                </div>
                <form method="get" action="rental_cancel_booking.php">
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tbody>
                                    <?php
                                    foreach($list123 as $reasons){?>
                                        <tr><td><input type="radio" name="cancel_reason_id" value="<?php echo $reasons['reason_id']; ?>"> &nbsp; <?php echo $reasons['reason_name']; ?><br></td></tr>
                                    <?php } ?>
                                    <input type="hidden" name="rental_booking_id" value="<?php echo $rental['rental_booking_id'];?>">
                                 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success br2 btn-xs fs12 activebtn" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
</section>
<!-- Main Content Ends -->

</body></html>