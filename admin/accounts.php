<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$where = "";
if(isset($_POST['username']) && isset($_POST['email']) && isset($_POST['phone'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $where .= " and driver.driver_name LIKE '%$username%'";
    $where .= " and driver.driver_email LIKE '%$email%'";
    $where .= " and driver.driver_phone LIKE '%$phone%'";
    $where .= " and table_driver_bill.bill_status=0";
}else{
    $where .= " and table_driver_bill.bill_status=0";
}
$query="select * from table_driver_bill INNER JOIN driver ON table_driver_bill.driver_id=driver.driver_id WHERE 1 =1 $where";
$result = $db->query($query);
$list = $result->rows;
$data = array();
foreach($list as $k => $v)
{
    $data[$v['driver_id']]['driver_id'] = $v['driver_id'];
    $data[$v['driver_id']]['driver_name'] = $v['driver_name'];
    $data[$v['driver_id']]['outstanding_amount'][] = $v['outstanding_amount'];
    $data[$v['driver_id']]['company_payment'] = $v['company_payment'];
}

?>
<script>
    function validatelogin() {
        var username = document.getElementById('username').value;
        var email = document.getElementById('email').value;
        var phone = document.getElementById('phone').value;
        if(username == "" && email =="" && phone == "" )
        {
            alert("Enter Any Keyword for Search");
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Driver Accounts</h3>
        <span>
            <a href="home.php?pages=genrate-bill"><img src="images/32247-200.png" style="width: 50px;float: right;" title="Genrate Bill" alt="Genrate Bill"></a>
      </span>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" style="padding: 15px 0px 2px 30px;">
                <div class="panel-body" style="padding-top:0px;">
                    <form method="post" onSubmit="return validatelogin()">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="username" id="username" style="width: 100%"  title="Type in a name" placeholder="Search for Driver names..">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="email" id="email" style="width: 100%"  title="Type in a email" placeholder="Search for Driver email..">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="phone" id="phone" style="width: 100%"  title="Type in a name" placeholder="Search for Driver phone..">
                        </div>
                        <button type="submit" class="btn btn-success m-l-10">Search</button>


                        <div id='button-group'>
                            <ul class='list-inline links-list'>
                                <li>
                                    <a href="http://saassystems.co.uk/index.php?graphics/JobSheet/6c0cdf1f29ec7edb48316da3080f9c6b/jobSheet" title="Job Sheet" class=""><i class="fa fa-text-file"></i></a>
                                </li>

                            </ul>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <form role="form" name="form" method="post" enctype="multipart/form-data" onsubmit="return myFunction()">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="5%">Driver Id</th>
                                        <th>Driver Name</th>
                                        <th>Billed Outstanding</th>
                                        <th>Unbilled Amount</th>
                                        <th>Total Outstanding</th>
                                        <th>Bills</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($data as $driver_account){?>
                                        <tr>
                                            <td><?php echo $driver_account['driver_id'] ?></td>
                                            <td><a href="home.php?pages=driver-bill&id=<?=$driver_account['driver_id']?>" ><span title="All Bill Details" class="bookind-id"> <?php echo $driver_account['driver_name'];?> </span></a></td>
                                            <td><?php
                                                $outstanding_amount = $driver_account['outstanding_amount'];
                                                echo array_sum($outstanding_amount);
                                               ?>
                                            </td>
                                            <td><?php echo $driver_account['company_payment'] ?> </td>
                                            <td><?php
                                                $outstanding_amount = array_sum($driver_account['outstanding_amount']);
                                                $company_payment = $driver_account['company_payment'];
                                                $total = $outstanding_amount+$company_payment;
                                                echo $total;
                                            ?> </td>
                                            <td><a href="home.php?pages=driver-bill&id=<?=$driver_account['driver_id']?>"><button type="button" class="btn btn-info">Info</button></a> </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</form>
</section>
</body></html>