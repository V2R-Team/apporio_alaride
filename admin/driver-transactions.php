<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$bill_id = $_GET['bill_id'];
$query ="select * from table_driver_bill WHERE bill_id='$bill_id'";
$result = $db->query($query);
$list = $result->row;
$driver_id = $list['driver_id'];
$start_date = $list['bill_from_date'];
$bill_to_date = $list['bill_to_date'];
$start = explode(" ",$start_date);
$start_date = $start[0];
$end = explode(" ",$bill_to_date);
$end_date = $end[0];
$query ="select * from done_ride INNER JOIN payment_confirm ON done_ride.done_ride_id=payment_confirm.order_id INNER JOIN ride_table ON done_ride.ride_id=ride_table.ride_id INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id WHERE ride_table.driver_id='$driver_id' AND payment_confirm.payment_date >='$start_date' AND payment_confirm.payment_date <='$end'";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key=>$value)
{
    $city_id = $value['city_id'];
    $query="select * from city WHERE city_id='$city_id'";
    $result = $db->query($query);
    $list1=$result->row;
    $currency = $list1['currency'];
    $list[$key] = $value;
    $list[$key]['currency'] = $currency;
}

?>
    <div class="wraper container-fluid">
      <div class="page-title">
        <h3 class="title">View Transactions</h3>
		 </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                  <table id="datatable" class="table table-striped table-bordered table-responsive">
                    <thead>
                      <tr>
                          <th>User Name</th>
                          <th>Driver Name</th>
                          <th>Total Amount</th>
                          <th>Company Cut</th>
                          <th>Driver Cut</th>
                          <th>Waiting Charges</th>
                          <th>Time Charges</th>
                          <th>Peak Time Charge</th>
                          <th>Night Time Charge</th>
                          <th>Coupan Price</th>
                        <th>Payment Mode</th>
                          <th>Payment Date</th>
                        <th width="8%"> Status</th>
                        <th>Full Details</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($list as $payment_confirm){?>
                      <tr>
                        <td><?php
						 	$begin_location = $payment_confirm['user_name'];
							if($begin_location=="")
							{
								echo "------";
							}
							else
							{
								echo $begin_location;
							}							
						 ?></td>
                        <td><?php
						 	$end_location=$payment_confirm['driver_name'];
							if($end_location=="")
							{
								echo "------";
							}
							else
							{
								echo $end_location;
							}							
						 ?></td>
                        <td><?php
                            $currency = $payment_confirm['currency'];
						 	$payment_amount=$payment_confirm['payment_amount'];
							if($payment_amount=="")
							{
								echo "------";
							}
							else
							{
								echo $currency." ".$payment_amount;
							}							
						 ?></td>

                          <td><?php
                              $currency = $payment_confirm['currency'];
                              $company_commision=$payment_confirm['company_commision'];
                              if($company_commision=="")
                              {
                                  echo "------";
                              }
                              else
                              {
                                  echo $currency." ".$company_commision;
                              }
                              ?></td>
                          <td><?php
                              $currency = $payment_confirm['currency'];
                              $driver_amount=$payment_confirm['driver_amount'];
                              if($driver_amount=="")
                              {
                                  echo "------";
                              }
                              else
                              {
                                   echo $currency." ".$driver_amount;
                              }
                              ?></td>
                          <td><?php
                              $currency = $payment_confirm['currency'];
                              $waiting_price =$payment_confirm['waiting_price'];
                              if($waiting_price=="")
                              {
                                  echo "------";
                              }
                              else
                              {
                                  echo $currency." ".$waiting_price;
                              }
                              ?></td>
                          <td><?php
                              $currency = $payment_confirm['currency'];
                              $ride_time_price =$payment_confirm['ride_time_price'];
                              if($ride_time_price=="")
                              {
                                  echo "------";
                              }
                              else
                              {
                                  echo $currency." ".$ride_time_price;
                              }
                              ?></td>
                          <td><?php
                              $currency = $payment_confirm['currency'];
                              $peak_time_charge =$payment_confirm['peak_time_charge'];
                              echo $currency." ".$peak_time_charge;
                              ?></td>
                          <td><?php
                              $currency = $payment_confirm['currency'];
                              $night_time_charge =$payment_confirm['night_time_charge'];
                              echo $currency." ".$night_time_charge;
                              ?></td>
                          <td>
                              <?php
                              $currency = $payment_confirm['currency'];
                              $coupan_price =$payment_confirm['coupan_price'];
                              if($coupan_price=="")
                              {
                                  echo "------";
                              }
                              else
                              {
                                  echo $currency." ".$coupan_price;
                              }
                              ?>
                          </td>
                        <td><?php
						 	$payment_method=$payment_confirm['payment_method'];
							if($payment_method=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_method;
							}							
						 ?></td>
						  <td><?php
                              $payment_date_time=$payment_confirm['payment_date_time'];
                              if($payment_date_time=="")
                              {
                                  echo "------";
                              }
                              else
                              {
                                  echo $payment_date_time;
                              }
                              ?></td>
                        <td><?php
						 	$payment_status=$payment_confirm['payment_status'];
							if($payment_status=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_status;
							}							
						 ?></td>
                         
                        <td><label style="cursor:pointer;" class="label label-success" data-toggle="modal" data-target="#details<?php echo $payment_confirm['order_id'];?>"  > Full Details </label></td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php foreach($list as $payment_confirm) { ?>
  
  <!-- Popup -->
  <div class="modal fade"  id="details<?php echo $payment_confirm['order_id'];?>" role="dialog">
    <div class="modal-dialog"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Transaction Full Details</h4>
          <div id="assign_driver_success" style="display:none;"> <br>
            <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $ridelater['ride_id'];?> </div>
          </div>
        </div>
        <div class="modal-body" >
          <div class="tab-content">
            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
              <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                <thead>
                  <tr>
                    <th width="40%">Request id</th>
                    <td class=""><?php
						 	$order_id=$payment_confirm['order_id'];
							if($order_id=="")
							{
								echo "------";
							}
							else
							{
								echo $order_id;
							}							
						 ?></td>
                  </tr>
                  <tr>
                    <th>Pickup Address</th>
                    <td class=""><?php
						 	$begin_location=$payment_confirm['begin_location'];
							if($begin_location=="")
							{
								echo "------";
							}
							else
							{
								echo $begin_location;
							}							
						 ?></td>
                  <tr>
                    <th>Drop Address</th>
                    <td class=""><?php
						 	$end_location=$payment_confirm['end_location'];
							if($end_location=="")
							{
								echo "------";
							}
							else
							{
								echo $end_location;
							}							
						 ?></td>
                  </tr>
                  <tr>
                    <th>Total Amount</th>
                    <td><?php
						 	$payment_amount=$payment_confirm['payment_amount'];
							if($payment_amount=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_amount;
							}							
						 ?></td>
                  </tr>
                  <tr>
                    <th>Payment Mode</th>
                    <td><?php
						 	$payment_method=$payment_confirm['payment_method'];
							if($payment_method=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_method;
							}							
						 ?></td>
                  </tr>
                  <tr>
                    <th>Payment Status</th>
                    <td><?php
						 	$payment_status=$payment_confirm['payment_status'];
							if($payment_status=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_status;
							}							
						 ?></td>
                  </tr>
                  
                  <tr>
                    <th>Payment Platform</th>
                    <td><?php
						 	$payment_platform=$payment_confirm['payment_platform'];
							if($payment_platform=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_platform;
							}							
						 ?></td>
                  </tr>
                  
                  <tr>
                    <th>Payment Date</th>
                    <td><?php
						 	$payment_date_time=$payment_confirm['payment_date_time'];
							if($payment_date_time=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_date_time;
							}							
						 ?></td>
                  </tr>
                  
                  <tr>
                    <th>Payment Status</th>
                    <td><?php
						 	$payment_status=$payment_confirm['payment_status'];
							if($payment_status=="")
							{
								echo "------";
							}
							else
							{
								echo $payment_status;
							}							
						 ?></td>
                  </tr>
                  
                  <tr>
                    <th>Arrived Time</th>
                    <td><?php
						 	$arrived_time=$payment_confirm['arrived_time'];
							if($arrived_time=="")
							{
								echo "------";
							}
							else
							{
								echo $arrived_time;
							}							
						 ?></td>
                  </tr>
                   <tr>
                    <th>Start Time</th>
                    <td><?php
						 	$begin_time=$payment_confirm['begin_time'];
							if($begin_time=="")
							{
								echo "------";
							}
							else
							{
								echo $begin_time;
							}							
						 ?></td>
                  </tr>
                  <tr>
                    <th>End Time</th>
                    <td><?php
						 	$end_time=$payment_confirm['end_time'];
							if($end_time=="")
							{
								echo "------";
							}
							else
							{
								echo $end_time;
							}							
						 ?></td>
                  </tr>
                   <tr>
                    <th>Waiting Time</th>
                    <td><?php
						 	$waiting_time=$payment_confirm['waiting_time'];
							if($waiting_time=="")
							{
								echo "------";
							}
							else
							{
								echo $waiting_time;
							}							
						 ?></td>
                  </tr>
                  
                   <tr>
                    <th>Ride Time</th>
                    <td><?php
						 	$ride_time=$payment_confirm['tot_time'];
							if($ride_time=="")
							{
								echo "------";
							}
							else
							{
								echo $ride_time;
							}							
						 ?></td>
                  </tr>
                  
                  <tr>
                    <th>Distance</th>
                    <td><?php
						 	$distance=$payment_confirm['distance'];
							if($distance=="")
							{
								echo "------";
							}
							else
							{
								echo $distance;
							}							
						 ?></td>
                  </tr>
                   <tr>
                    <th>Total Time</th>
                    <td><?php
						 	$tot_time=$payment_confirm['tot_time'];
							if($tot_time=="")
							{
								echo "------";
							}
							else
							{
								echo $tot_time;
							}							
						 ?></td>
                  </tr>
                  
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>