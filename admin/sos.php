<?php
session_start();
include_once '../apporioconfig/start_up.php';
if (!isset($_SESSION['ADMIN']['ID'])) {
    $db->redirect("home.php?pages=index");
}
include('common.php');


$query = "select * from sos";
$result = $db->query($query);
$list = $result->rows;



$sql1 = "SELECT * FROM sos_request INNER JOIN driver ON sos_request.driver_id=driver.driver_id INNER JOIN user ON sos_request.user_id=user.user_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id ORDER BY sos_request_id DESC";
$Query = $db->query($sql1);
$list2 = $Query->rows;



if (isset($_GET['status']) && isset($_GET['id'])) {

    $query1 = "UPDATE sos SET sos_status='" . $_GET['status'] . "' WHERE sos_id='" . $_GET['id'] . "'";
    $db->query($query1);
    $db->redirect("home.php?pages=sos");
}
if (isset($_POST['delete'])) {
   $query1 = "Delete FROM sos  WHERE sos_id='" . $_POST['delete'] . "'";
   $db->query($query1);
    $db->redirect("home.php?pages=sos");
}
if (isset($_POST['addContact'])) {
    $query2 = "INSERT INTO sos (sos_name,sos_number) VALUES ('".$_POST['sos_name']."','".$_POST['sos_number']."')";
    $db->query($query2);
    $db->redirect("home.php?pages=sos");
}
if (isset($_POST['savechanges'])) {
    $sos_name = $_POST['sos_name'];
    $query2 = "UPDATE sos  SET sos_name='$sos_name',sos_number='".$_POST['sos_number']."' WHERE sos_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=sos");
}

?>


<div class="wraper container-fluid">
    <div class="page-title">
        <h3 style="margin-top:15px;" class="title">SOS Contacts</h3>

        <div class="row" style="border: groove; padding:8px">
<span class="tp_rht">
</span>
            <span data-target="#add"
                  data-toggle="modal"><a data-original-title="Add New"
                                         data-toggle="tooltip" data-placement="top"
                                         class="btn btn-primary add_btn" style="float:right;"> <i
                            class="fa fa-plus"></i> </a></span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Contact Name</th>
                                    <th>Contact Number</th>
                                    <th>Status</th>
                                    <th width="80">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $i = 1;
                                foreach ($list as $sos) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?= $sos['sos_name']; ?></td>
                                        <td><?= $sos['sos_number']; ?></td>
                                        <?php
                                        if ($sos['sos_status'] == 1) {
                                            ?>
                                            <td class="">

                                                <label class="label label-success"> Active</label>

                                            </td>
                                            <?php
                                        } else {
                                            ?>
                                            <td class="">

                                                <label class="label label-default"> Deactive</label>

                                            </td>
                                        <?php } ?>
                                        <td>

<span data-target="#<?php echo $sos['sos_id']; ?>"
      data-toggle="modal"><a data-original-title="Edit"
                             data-toggle="tooltip" data-placement="top"
                             class="btn menu-icon btn_edit"> <i
                class="fa fa-pencil"></i> </a></span>
                                            <?php if ($sos['sos_status'] == 2) { ?>
                                                <a href="home.php?pages=sos&status=1&id=<?php echo $sos['sos_id']; ?>"
                                                   data-original-title="Active" class="btn menu-icon btn_eye"> <i
                                                            class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                                <a href="home.php?pages=sos&status=2&id=<?php echo $sos['sos_id']; ?>"
                                                   data-original-title="Inactive" data-toggle="tooltip"
                                                   data-placement="top" class="btn menu-icon btn_eye_dis"> <i
                                                            class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>

                                            <span data-target="#delete<?php echo $sos['sos_id']; ?>"
                                                  data-toggle="modal"><a data-original-title="Delete"
                                                                         data-toggle="tooltip" data-placement="top"
                                                                         class="btn menu-icon btn_delete"> <i
                                                            class="fa fa-trash"></i> </a></span>


                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h3>SOS Requests</h3>
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable1" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Driver Name</th>
                                    <th>Driver Phone</th>
                                    <th>Car Type</th>
                                    <th>Car Number</th>
                                    <th>User Name</th>
                                    <th>User Phone</th>
                                    <th>Report Number</th>
                                    <th>Date And Time</th>
                                    <th>Location</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($list2 as $sos) {
                                    ?>
                                    <tr>
                                        <td><?php echo $sos['sos_request_id']; ?></td>
                                        <td><?php echo $sos['driver_name']; ?></td>
                                        <td><?php echo $sos['driver_phone']; ?></td>
                                        <td><?php echo $sos['car_type_name']; ?></td>
                                        <td><?php echo $sos['car_number']; ?></td>
                                        <td><?php echo $sos['user_name']; ?></td>
                                        <td><?php echo $sos['user_phone']; ?></td>
                                        <td><?php echo $sos['sos_number']; ?></td>
                                        <td><?php echo $sos['request_date']; ?></td>
                                        <td>
                                            <a target="_blank" href="home.php?pages=sos-location&id=<?=$sos['sos_request_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End row -->

</div>


<?php foreach($list as $sos) {  ?>
    <div class="modal fade" id="<?php echo $sos['sos_id']; ?>" role="dialog">
        <form class="modal-dialog" method="post">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit SOS Contact</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Contact Name</label>
                                <input type="text" class="form-control" placeholder="Contact Name"
                                       name="sos_name"
                                       value="<?php echo $sos['sos_name']; ?>" id="sos_name" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Contact Number</label>
                                <input class="form-control" placeholder="Company Phone"
                                       name="sos_number"
                                       value="<?php echo $sos['sos_number']; ?>" id="sos_number"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button name="savechanges" value="<?php echo $sos['sos_id']; ?>"
                            class="btn btn-info">Save Changes
                    </button>
                </div>
            </div>
        </form>
    </div>
    </div>
<?php } ?>
<!-- Page Content Ends -->
<!-- ================== -->


<?php
foreach($list as $sos) { ?>
    <div class="modal fade" id="delete<?php echo $sos['sos_id']; ?>" role="dialog">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Delete Company</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>This Is Demo Version You Will Not Delete</h3></div>
                        <div class="modal-footer">
                            <button name="delete" value="<?php echo $sos['sos_id']; ?>"
                                    class="btn btn-danger">Delete
                            </button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php } ?>
<!--ADD MODAL-->
<div class="modal fade" id="add" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Add SOS Contact</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Contact Name</label>
                                <input class="form-control" placeholder="Contact Name" name="sos_name"
                                       value="" id="sos_name" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Contact Number</label>
                                <input class="form-control" placeholder="Contact Number" name="sos_number"
                                       value="" id="sos_number" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button name="addContact" value=""
                                class="btn btn-success">Add
                        </button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>

</section>
<!-- Main Content Ends -->

</body></html>