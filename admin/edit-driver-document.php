<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$driver_document_id = $_GET['driver_document_id'];
$document_id = $_GET['id'];
$query1="SELECT * FROM table_documents WHERE document_id='$document_id'";
$result1 = $db->query($query1);
$list = $result1->row;
if ($_POST['Update'])
{
    $driver_document_id = $_POST['driver_document_id'];
    $expire = $_POST['expire'];
    if(!empty($_FILES['document']))
    {
        $img_name = $_FILES['document']['name'];

        $filedir  = "../uploads/driver/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['document']['name'],-4));
        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = time().$img_name."document_image_".$driver_document_id.$fileext;
            $filepath1 = "uploads/driver/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['document']['tmp_name'], $filepath);

            $upd_qry = "UPDATE table_driver_document SET document_path ='$filepath1' where driver_document_id ='$driver_document_id'";
            $db->query($upd_qry);
        }
    }
    $upd_qry = "UPDATE table_driver_document SET document_expiry_date='$expire' where driver_document_id ='$driver_document_id'";
    $db->query($upd_qry);
    $msg = "Updated Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=drivers");
}
?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Edit Driver Document</h3>
    </div>

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="form" >
                    <form class="cmxform form-horizontal tasi-form" name="peak" enctype="multipart/form-data" onSubmit="return validatelogin()" method="post" >
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-body">
                                    <div class="form" >

                                        <div class="form-group ">
                                            <label class="control-label col-lg-2"><?= $list['document_name']; ?> </label>
                                            <div class="col-lg-6">
                                                <input type="file" class="form-control" placeholder="<?= $list['document_name']; ?>" name="document"  id="document" required>
                                                <input type="hidden"  name="driver_document_id"  id="driver_document_id" value="<?php echo $_GET['driver_document_id'];?>" >
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label class="control-label col-lg-2">Expire Date*</label>
                                            <div class="col-lg-6">
                                                <input type="date" class="form-control"  placeholder="Expire Date" name="expire" id="expire" required>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="Update" value="Save Changes" >
                                        </div>
                                    </div>

</div>
</div>

</div>
</div>
</form>

</div>
<div class="clear"></div>
</div>
</div>
</div>

</div>
<script>
    jQuery(document).ready(function() {
        var timeAnswers =$('#timepicker, #timepicker1');
        $(timeAnswers).each(function(){
            $(this).timepicki({
                overflow_minutes:true,
                increase_direction:'up',
                disable_keyboard_mobile: true
            });
        });
    });
</script>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>