<?php
include_once '../apporioconfig/start_up.php';
  $query = "select * from pages where page_id='3'";
  $result = $db->query($query);
  $text = $result->row;   

if(isset($_POST['save'])) 
     {  
$title = addslashes($_POST['title']);
$desciption = addslashes($_POST['description']);
$query2="UPDATE pages SET title='$title', description='$desciption' WHERE page_id=3";
$db->query($query2); 
$db->redirect("home.php?pages=terms-condition");
  
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Taxi Admin Panel">
        <link rel="icon" href="img/favicon.jpg" sizes="50x50" />
            
        <title>Taxi Admin</title>
        <?php include('style.php'); ?>

        <!--bootstrap-wysihtml5-->
        <link rel="stylesheet" type="text/css" href="taxi/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link href="taxi/summernote/summernote.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet">

</head>

<body>
         <!-- Sidebar Menu Start-->
        <?php include('sidebar.php'); ?>
        <!-- Sidebar Menu Ends-->

        <!-- Aside Ends-->


        <!--Main Content Start -->
        <section class="content">
            
            <!-- Header -->
            <?php include('header.php'); ?>
            <!-- Header Ends -->


            <!-- Page Content Start -->
            <!-- ================== -->

          <!-- End row -->
                <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Terams & Conditions</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Title</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Title" name="title" value="<?= $text['title'];?>" id="">
                  </div>
                </div>
                
                <div class="form-group">
                   <label class="col-md-2 control-label">Description</label>
                   <div class="col-md-10">
                
                          <textarea class="summernote" name="description" placeholder="Description" rows="5"><?= $text['description'];?></textarea>
                   </div>
                </div>
               
               <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>


              


            </div>
            <!-- Page Content Ends -->
            <!-- ================== -->

            <!-- Footer Start -->
            <?php include('footer.php'); ?>
            <!-- Footer Ends -->



        </section>
        



    
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

        <script type="text/javascript" src="taxi/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="taxi/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

        <!--form validation init-->
        <script src="taxi/summernote/summernote.min.js"></script>

        <script src="js/jquery.app.js"></script>

        <script>

            jQuery(document).ready(function(){
                $('.wysihtml5').wysihtml5();

                $('.summernote').summernote({
                    height: 200,                 // set editor height

                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor

                    focus: true                 // set focus to editable area after initializing summernote
                });

            });
        </script>


    </body>
</html>
