<?php
session_start();
include_once '../apporioconfig/start_up.php';
if (!isset($_SESSION['ADMIN']['ID'])) {
    $db->redirect("home.php?pages=index");
}
include('common.php');


$query = "select * from company ORDER BY company_id DESC";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key => $value) {
    $company_id = $value['company_id'];
    $query = "select * from driver WHERE company_id='$company_id'";
    $result = $db->query($query);
    $ex_rows = $result->num_rows;
    $list[$key] = $value;
    $list[$key]["total_driver"] = $ex_rows;
}

if (isset($_GET['status']) && isset($_GET['id'])) {

    $query1 = "UPDATE company SET company_status='" . $_GET['status'] . "' WHERE company_id='" . $_GET['id'] . "'";
    $db->query($query1);
    $db->redirect("home.php?pages=view-company");
}
if (isset($_POST['delete'])) {
    $query1 = "Delete FROM company  WHERE company_id='" . $_POST['delete'] . "'";
    $db->query($query1);
    $db->redirect("home.php?pages=view-company");
}

if (isset($_POST['savechanges'])) {
    $query2 = "UPDATE company SET company_name='" . $_POST['company_name'] . "',company_email='" . $_POST['company_email'] . "',company_phone='" . $_POST['company_phone'] . "',company_address='" . $_POST['company_address'] . "',company_contact_person='" . $_POST['company_contact_person'] . "' where company_id='" . $_POST['savechanges'] . "'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-company");
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwuW7ZtAG05nr_X5TKV4oHi7GBxeK7vDY&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('company_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Taxi Companies</h3>
            <span class="tp_rht">
            <!--<a href="home.php?pages=add-company" class="btn btn-default btn-lg" id="add-button" title="Add Company" role="button">Add Company</a>-->
            <a href="home.php?pages=add-company" data-toggle="tooltip" title="Add Company"
               class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
          </span>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Company Email</th>
                                        <th>Company Phone</th>
                                        <th>Company Address</th>
                                        <th>Company Contact Person</th>
                                        <th>No. of Drivers</th>
                                        <th>Status</th>
                                        <th width="80">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php

                                    foreach ($list as $company) {
                                        ?>
                                        <tr>
                                            <td>
                                                <span title="Full Details" class="bookind-id" data-toggle="modal"
                                                      data-target="#full<?php echo $company['company_id']; ?>"> <?php echo $company['company_name']; ?> </span>
                                            </td>
                                            <td><?php
                                                $company_email = $company['company_email'];
                                                echo $company_email;
                                                ?></td>
                                            <td><?php
                                                $company_phone = $company['company_phone'];
                                                echo $company_phone;
                                                ?></td>
                                            <td><?= $company['company_address']; ?></td>
                                            <td><?= $company['company_contact_person']; ?></td>
                                            <td><?= $company['total_driver']; ?></td>
                                            <?php
                                            if ($company['company_status'] == 1) {
                                                ?>
                                                <td class="">

                                                    <label class="label label-success"> Active</label>

                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="">

                                                    <label class="label label-default"> Deactive</label>

                                                </td>
                                            <?php } ?>
                                            <td>

                                                <span data-target="#<?php echo $company['company_id']; ?>"
                                                      data-toggle="modal"><a data-original-title="Edit"
                                                                             data-toggle="tooltip" data-placement="top"
                                                                             class="btn menu-icon btn_edit"> <i
                                                                class="fa fa-pencil"></i> </a></span>
                                                <?php if ($company['company_status'] == 2) { ?>
                                                    <a href="home.php?pages=view-company&status=1&id=<?php echo $company['company_id']; ?>"
                                                       data-original-title="Active" class="btn menu-icon btn_eye"> <i
                                                                class="fa fa-eye"></i> </a>
                                                <?php } else { ?>
                                                    <a href="home.php?pages=view-company&status=2&id=<?php echo $company['company_id']; ?>"
                                                       data-original-title="Inactive" data-toggle="tooltip"
                                                       data-placement="top" class="btn menu-icon btn_eye_dis"> <i
                                                                class="fa fa-eye-slash"></i> </a>
                                                <?php } ?>

                                                <span data-target="#delete<?php echo $company['company_id']; ?>"
                                                      data-toggle="modal"><a data-original-title="Delete"
                                                                             data-toggle="tooltip" data-placement="top"
                                                                             class="btn menu-icon btn_delete"> <i
                                                                class="fa fa-trash"></i> </a></span>


                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>


<?php foreach ($list as $company) { ?>
    <div class="modal fade" id="<?php echo $company['company_id']; ?>" role="dialog">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Edit Company Details</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Company Name</label>
                                    <input type="text" class="form-control" placeholder="Company Name"
                                           name="company_name"
                                           value="<?php echo $company['company_name']; ?>" id="company_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Company Email</label>
                                    <input type="text" class="form-control" placeholder="Company Email"
                                           name="company_email"
                                           value="<?php echo $company['company_email']; ?>" id="company_email" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Company Phone</label>
                                    <input type="text" class="form-control" placeholder="Company Phone"
                                           name="company_phone"
                                           value="<?php echo $company['company_phone']; ?>" id="company_phone" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Company Address</label>
                                    <input type="text" class="form-control" placeholder="Company Address"
                                           name="company_address" value="<?php echo $company['company_address']; ?>"
                                           id="company_address" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Company Contact Person</label>
                                    <input type="text" class="form-control" placeholder="Company Contact Person"
                                           name="company_contact_person"
                                           value="<?php echo $company['company_contact_person']; ?>"
                                           id="company_contact_person" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $company['company_id']; ?>"
                                class="btn btn-info">Save Changes
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php } ?>
<!-- Page Content Ends -->
<!-- ================== -->


<?php
foreach ($list as $company) { ?>
    <div class="modal fade" id="delete<?php echo $company['company_id']; ?>" role="dialog">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Delete Company</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>This Is Demo Version You Will Not Delete</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $company['company_id']; ?>"
                                    class="btn btn-danger">Delete
                            </button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
<?php } ?>
<?php
$dummyImg = "http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
foreach ($list as $company) {
    ?>
    <div class="modal fade" id="full<?php echo $company['company_id']; ?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content" style="padding:20px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Company Full Details</h4>
                </div>
                <div class="modal-body" style="max-height: 500px; overflow-x: auto;">
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <td class="" colspan="" style="text-align-last:left; width:150px;"><img
                                        src="<?php if ($company['company_image'] != '' && isset($company['company_image'])) {
                                            echo '../' . $company['company_image'];
                                        } else {
                                            echo $dummyImg;
                                        } ?>" width="150px" height="150px"></td>
                            <td>
                                <table style="margin-left:20px;" aling="center" border="0">
                                    <tbody>
                                    <tr>
                                        <th class="">Company Name</th>
                                        <td class=""><?php
                                            $company_name = $company['company_name'];
                                            echo $company_name;
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <th class="">Company Email</th>
                                        <td class=""><?php
                                            $company_email = $company['company_email'];
                                            $e1 = substr($company_email, 0, 2);
                                            $e2 = explode("@", $company_email);
                                            $domain = $e2[1];
                                            echo "*****@" . $domain;

                                            ?></td>
                                    </tr>
                                    <tr>
                                        <th class="">Company Phone</th>
                                        <td class=""><?php
                                            $company_phone = $company['company_phone'];
                                            $e1 = substr($company_phone, 7);
                                            echo "********" . $e1;
                                            ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th class="">Company Address</th>
                            <td class=""><?php
                                $company_address = $company['company_address'];
                                echo $company_address;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th class="">Company Contact Person</th>
                            <td class=""><?php
                                $company_contact_person = $company['company_contact_person'];
                                echo $company_contact_person;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th class="">Vat Number</th>
                            <td class=""><?php
                                $vat_number = $company['vat_number'];
                                echo $vat_number;
                                ?>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Modal content closed-->

        </div>
    </div>
<?php } ?>
</section>
<!-- Main Content Ends -->

</body></html>