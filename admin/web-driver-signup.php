<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from web_driver_signup WHERE id=1";
$result = $db->query($query);
$list = $result->row;

/*if(isset($_POST['save']))
{
    $title = $_POST['title'];
    $description = $_POST['description'];
    $query2="UPDATE web_driver_signup SET title='$title',description='$description' WHERE id=1";
    $db->query($query2);
    $db->redirect("home.php?pages=web-about-us");
}*/

?>

<script>
    function validatelogin() {
        var title = document.getElementById('title').value;
        var description = document.getElementById('description').value;
        if(title == "")
        {
            alert("Enter Title");
            return false;
        }
        if(description == "")
        {
            alert("Enter Description");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Driver Signup Page</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Title*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Title" name="title" value="<?= $list['title'] ?>" id="title" >
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Description*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="description" name="description"   placeholder="Description"><?php echo $list['description'];?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html>
