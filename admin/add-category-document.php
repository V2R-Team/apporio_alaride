<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="select * from city";
$result = $db->query($query);
$list=$result->rows;

$query2="select * from table_documents;";
$result2 = $db->query($query2);
$list2=$result2->rows;


if(isset($_POST['save'])) {
    $city_id = $_POST['city'];
    $query1="select * from table_document_list WHERE city_id='$city_id'";
    $result1 = $db->query($query1);
    $list12=$result1->rows;
    if(count($list12) == 0)
    {
        foreach ($_POST['docList'] as $row) {
            $query3 = "INSERT INTO table_document_list (city_id,document_id) VALUES ('$city_id',$row)";
            $db->query($query3);
        }
    }else{
        $msg = "Document Already Added";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-category-document");
    }
}

?>

<script>
    function validatelogin() {
        var city_name = document.getElementById('city_name').value;
        var doc_list = document.getElementById('doc_list').value;
        if(city_name == "")
        {
            alert("Enter City Name");
            return false;
        }
        if(doc_list == "")
        {
            alert("Select Documents");
            return false;
        }

    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Driver Document</h3>
        <span class="tp_rht">
            <a href="home.php?pages=view-documents" data-toggle="tooltip" title="Back" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">City*</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="city" id="city_name" >
                                        <option value="" disabled selected>--Please Select City--</option>
                                        <?php foreach($list as $city):?>
                                            <option value="<?php echo $city['city_id'];?>"><?php echo $city['city_name'];?></option>
                                        <?php endforeach;?>
                                    </select>                  </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Document List*</label>
                                <div class="col-lg-6">
                                    <select class="selectpicker" name="docList[]" id="doc_list" data-width="100%" data-live-search="true" multiple data-actions-box="true" multiple>
                                        <option value=""  disabled>--Please Select Documents--</option>
                                        <?php foreach($list2 as $document):?>
                                            <option value="<?php echo $document['document_id'];?>"><?php echo $document['document_name'];?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->
</section>
<!-- Main Content Ends -->
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
    });
</script>

</body>
</html>
