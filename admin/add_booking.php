<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
function sortByOrder($a, $b)
{
    return $a['distance'] - $b['distance'];
}
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$query="select * from car_type";
$result = $db->query($query);
$list = $result->rows;

$query = "select * from country";
$result = $db->query($query);
$country = $result->rows;
$query = "select * from coupons where start_date <= CURDATE() AND expiry_date >= CURDATE()";
$result = $db->query($query);
$coupons = $result->rows;
if(isset($_POST['save'])) {
    $user_id = $_POST['user_id'];
    $phonecode = $_POST['phonecode'];
    $userphone = $_POST['userphone'];
    $phone = $phonecode.$userphone;
    $username = $_POST['username'];
    $email = $_POST['email'];
    $origin = $_POST['origin-input'];
    $destination = $_POST['destination-input'];
    $car_type = $_POST['car_type'];
    $ridus = $_POST['radius'];
    $driver = $_POST['driver'];
    $ride_now = $_POST['ride_now'];
    $lat = $_POST['orig_latitude'];
    $long = $_POST['orig_longitude'];
    $lat1 = $_POST['dest_latitude'];
    $long1 = $_POST['dest_longitude'];
    $datepicker = $_POST['datepicker'];
    $timepicker = $_POST['timepicker'];
    $coupon_code = $_POST['coupon_code'];
    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
    $data=$dt->format('M j');
    $day=date("l");
    $date=$day.", ".$data ;
    $time=date("H:i:s");
    $date1=date("Y-m-d");
    $last_time_stamp = date("h:i:s A");
    if (empty($user_id))
    {
        $data = "select user_id from user WHERE user_phone=$phone";
        $result = $db->query($data);
        $list = $result->row;
        if (!empty($list))
        {
            $user_id = $list['user_id'];
        }else{
            $query2="INSERT INTO user (user_phone,user_name,user_email) VALUES ('$phone','$username','$email')";
            $db->query($query2);
            $user_id = $db->getLastId();
        }
    }
    $query2="INSERT INTO ride_table (ride_platform,coupon_code,date,later_date,later_time,user_id,car_type_id,pickup_lat,pickup_long,pickup_location,drop_lat,drop_long,drop_location,ride_date,ride_time,ride_type,ride_status,ride_admin_status,last_time_stamp) 
         VALUES ('2','$coupon_code','$date1','$datepicker','$timepicker','$user_id','$car_type','$lat','$long','$origin','$lat1','$long1','$destination','$date','$time','$ride_now',1,1,'$last_time_stamp')";
    $db->query($query2);
    $ride_id = $db->getLastId();
    $query5="INSERT INTO table_user_rides(booking_id,ride_mode,user_id) VALUES ('$ride_id','1','$user_id')";
    $db->query($query5);

    if ($driver == "")
    {
        $query = "select * from driver where online_offline = 1 and car_type_id='$car_type' and driver_admin_status=1 and busy=0 and login_logout=1";
        $result = $db->query($query);
        $list = $result->rows;
        $c = array();
        foreach($list as $login)
        {
            $driver_lat = $login['current_lat'];
            $driver_long = $login['current_long'];
            $theta = $long - $driver_long;
            $dist = sin(deg2rad($lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles1 = $dist * 60 * 1.1515;
            $km=$miles1* 1.609344;
            if($km <= $ridus)
            {
                $c[] = array("driver_id"=> $login['driver_id'],"distance" => $km);
            }
        }

        if(!empty($c))
        {
            $nodes = array();
            foreach($c as $driver){
                $driver_id = $driver['driver_id'];
                $ride_id = (string)$ride_id;
                $nodes[$driver_id] = array('ride_id'=>$ride_id,'ride_status'=>"1");
                $query3="select * from driver_ride_allocated where driver_id='$driver_id'";
                $result3 = $db->query($query3);
                $driver_allocated = $result3->row;
                if (empty($driver_allocated))
                {
                    $query5="INSERT INTO driver_ride_allocated (driver_id,ride_id,ride_mode) VALUES ('$driver_id','$ride_id','1')";
                    $db->query($query5);
                }else{
                    $query5="UPDATE driver_ride_allocated SET ride_id='$ride_id' WHERE driver_id='$driver_id'" ;
                    $db->query($query5);
                }
                $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id,allocated_date) VALUES ('$ride_id','$driver_id','$date')";
                $db->query($query5);
            }
            $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/.json';
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
            curl_setopt($ch,CURLOPT_POST, count($nodes));
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($nodes));
            $response = curl_exec($ch);
            foreach ($c as $driver){
                $driver_id = $driver['driver_id'];
                $query4="select * from driver where driver_id='$driver_id'";
                $result4 = $db->query($query4);
                $list4=$result4->row;
                $device_id=$list4['device_id'];
                $language="select * from messages where language_id=1 and message_id=32";
                $lang_result = $db->query($language);
                $lang_list=$lang_result->row;
                $message=$lang_list['message_name'];
                $ride_id= (String) $ride_id;
                $ride_status= '1';
                if($device_id!="")
                {
                    if($list4['flag'] == 1)
                    {
                        IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                    }
                    else
                    {
                        AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                    }
                }
            }
            $db->redirect("home.php?pages=ride-now");
        }else{
            $sql2="UPDATE ride_table SET ride_status = 1 WHERE ride_id ='$ride_id'";
            $db->query($sql2);
            $db->redirect("home.php?pages=ride-now");
        }
    }else{
        $query = "select * from driver where driver_id='$driver'";
        $result = $db->query($query);
        $list = $result->row;
        $device_id = $list['device_id'];
        $booking_status = 1;
        $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id,allocated_ride_status) VALUES ('$ride_id','$driver','$booking_status')";
        $db->query($query5);
        $sql2="UPDATE ride_table SET ride_status = 1 WHERE ride_id ='$ride_id'";
        $db->query($sql2);
        $message = "New Ride Allocated";
        $ride_id= (String) $ride_id;
        $ride_status= (String) 1;
        if($device_id  != "")
        {
            if($list['flag'] == 1)
            {
                IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status);
            }
            else
            {
                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
            }
        }
        $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver.'/.json';
        $fields = array(
            'ride_id' => (string)$ride_id,
            'ride_status'=>"1",
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        $db->redirect("home.php?pages=ride-now");
    }
}
?>
<style xmlns="http://www.w3.org/1999/html">
    #map {
        height: 550px;
        width: 100%;;
        position: relative;
        overflow: hidden;
    }


    #mode-selector{
        display:none;
    }
    .total-price {
        margin-top: 30px;
        padding: 0px;
    }
    .total-price ul {
        margin: 0px;
        padding: 0px;
        float: left;
        width: 50%;
    }
    .total-price ul li {
        margin: 0 0 8px;
        padding: 0 0 8px;
        float: left;
        display: block;
        text-align: right;
        border-bottom: 1px solid #dcdcdc;
        font-weight: 600;
    }
    .total-price ul li b {
        margin: 0px;
        padding: 0px;
        float: left;
        width: 120px;
        text-align: left;
        font-weight: 500;
    }
    .total-price em {
        font-style: normal;
    }
    .total-price span {
        margin: 0px;
        padding: 10px 0;
        float: right;
        text-align: center;
        background: #ececec;
        font-size: 24px;
        line-height: 40px;
        width: 180px;
    }
    .total-price span b {
        margin: 0px;
        padding: 0px;
        float: left;
        width: 100%;
        font-size: 30px;
        font-weight: 600;
        color: #19aec4;
    }
    .clear{ clear: both !important;}

</style>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<link href="css/calander.css" rel="stylesheet" />
<script src="js/calander_jquery.js"></script>
<script src="js/calander_jquery-ui.js"></script>
<script src="js/wickedpicker.js"></script>
<link href="css/wickedpicker.css" rel="stylesheet" />
<script>
    var j = jQuery.noConflict();
    j(document).ready(function() {
        j("#datepicker").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }).attr('readOnly', 'true');
        j('#timepicker').wickedpicker({twentyFour: true,title: 'Select Time'});
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#get_details').on('click', function () {
            var phonecode = $('#phonecode').val();
            var phone = $('#userphone').val();
            var userphone = phonecode+phone;
            if(phone == "")
            {
                alert("Enter Phone Number");
                return false;
            }else{
                $.ajax({
                    type: "POST",
                    url: 'serach_user.php',
                    data: 'phone=' + userphone,
                    dataType: 'json',
                    success: function (data)
                    {
                        if (data != "") {
                            $('#user_id').val(data.user_id);
                            $('#username').val(data.user_name);
                            $('#email').val(data.user_email);
                        }else {
                            $("#username").val('');
                            $('#email').val('');
                            $('#user_id').val('');
                            alert("This Phone Number Is Not Registered")
                        }
                    }
                });
            }


        });
    });
    function getId(val) {
        var orig_latitude = document.getElementById('orig_latitude').value;
        var orig_longitude = document.getElementById('orig_longitude').value;
        if (orig_latitude == "" && orig_longitude == ""){
            alert("Plz Enter Pickup Up and Drop Up Location")
        }else{
            $.ajax({
                type: "POST",
                url: "serach_driver.php",
                data:  {car_type: val, orig_latitude: orig_latitude,orig_longitude: orig_longitude},
                success:
                    function(data){
                        $('#driver').html(data);
                    }
            });
        }

    }



    function setId(val){
        $.ajax({
            type: "POST",
            url: "country_code.php",
            data: "id="+val,
            success:
                function(data){
                    $('#phonecode').val(data);
                }
        });
    }

    function disableMyText(){
        if(document.getElementById("checkMe").checked == true){
            document.getElementById("driver").disabled = true;
        }else{
            document.getElementById("driver").disabled = false;
        }
    }

    function myFunction() {
        document.getElementById("datepicker").disabled = true;
        document.getElementById("timepicker").disabled = true;
    }

    function unclickFunction() {
        document.getElementById("datepicker").disabled = false;
        document.getElementById("timepicker").disabled = false;
    }

    function validatelogin() {
        var userphone = document.getElementById('userphone').value;
        var username = document.getElementById('username').value;
        var origin = document.getElementById('origin-input').value;
        var destination = document.getElementById('destination-input').value;
        var car_type = document.getElementById('car_type').value;
        if(userphone == ""){ alert("Enter Phone Number"); return false; }
        if(username == ""){ alert("Enter Rider Name"); return false; }
        if(document.getElementById('ride_now').checked == false && document.getElementById('ride_later').checked == false){
            alert("Select Ride Type");
            return false;
        }
        if(origin == ""){ alert("Enter Pickup Location"); return false; }
        if(destination == ""){ alert("Enter Drop Up Location"); return false; }
        if(car_type == ""){ alert("Select Car type"); return false; }
        if(document.getElementById('checkMe').checked == false && document.getElementById('driver').value == ""){
            alert("Select Driver Assign Type");
            return false;
        }
        if(document.getElementById('ride_later').checked == true && document.getElementById('datepicker').value == "")
        {
            alert("Select Ride Date");
            return false;
        }
        if(document.getElementById('ride_later').checked == true && document.getElementById('timepicker').value == "")
        {
            alert("Select Ride Time");
            return false;
        }
        if (document.getElementById('payment_option_id').value == "")
        {
            alert("Select Payment");
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Manual Taxi Dispatch</h3>
        <span>
            <a href="home.php?pages=manual-rides" class="btn btn-primary btn-lg" id="add-button"  title="Manual Rides" role="button">Manual Rides</a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form method="post" enctype="multipart/form-data"  onSubmit="return validatelogin()">
                        <div class="col-sm-5">

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control" id="country" name="country" onchange="setId(this.value);">
                                    <?php foreach($country as $country_list){ ?>
                                        <option value="<?php echo $country_list['id'];?>" <?php if($country_list['id'] == 99){ ?> selected <?php } ?>><?php echo $country_list['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="phonecode" value="+91" name="phonecode" type="text" readOnly>
                            </div>

                             <div class="col-md-12 col-sm-9 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="userphone" name="userphone" placeholder="Enter Phone Number" type="text">
                                <input type="hidden" id="user_id" name="user_id" value="">
                                <input type="hidden" id="orig_latitude" name="orig_latitude" value="">
                                <input type="hidden" id="orig_longitude" name="orig_longitude" value="">
                                <input type="hidden" id="dest_latitude" name="dest_latitude" value="">
                                <input type="hidden" id="dest_longitude" name="dest_longitude" value="">
                                <input type="hidden" id="distance" name="distance" value="">
                                <input type="hidden" id="time" name="time" value="">
                            </div>

                            <div class="col-md-3" style="clear:both; margin-bottom:20px;">
                                <button type="button" id="get_details" class="btn btn-default col-md-12">Get Details</button>
                            </div>

                            <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="username" name="username" placeholder="Username" type="text">
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="email" placeholder="Email" name="email" type="email">
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <label class="radio-inline"><input type="radio" name="ride_now" id="ride_now" onclick="myFunction()" value="1">Ride Now</label>
                                <label class="radio-inline"><input type="radio" name="ride_now" id="ride_later"  onclick="unclickFunction()"  value="2">Ride Later</label>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="datepicker" name="datepicker" placeholder="Select Date" type="text">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="timepicker" name="timepicker" placeholder="Select Time" type="text">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="origin-input" name="origin-input" placeholder="Pick up Location" type="text">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="destination-input" name="destination-input" placeholder="Drop off Location" type="text">
                            </div>

                            <div id="mode-selector" class="controls">
                                <input type="radio" name="type" id="changemode-walking">
                                <label for="changemode-walking">Walking</label>

                                <input type="radio" name="type" id="changemode-transit">
                                <label for="changemode-transit">Transit</label>

                                <input type="radio" name="type" id="changemode-driving" checked="checked">
                                <label for="changemode-driving">Driving</label>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="car_type" name="car_type" onchange="getId(this.value);">
                                    <option value="">Select Vehicle type</option>
                                    <?php foreach($list as $cartype){ ?>
                                        <option value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="radius" name="radius">
                                    <option value="5">5 Km Radius</option>
                                    <option value="10">10 Km Radius</option>
                                    <option value="15">15 Km Radius</option>
                                    <option value="20">20 Km Radius</option>
                                </select>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="checkbox">
                                    <label>
                                        <input value="1" type="checkbox" id="checkMe" name="checkMe" onclick="disableMyText()"> Auto Assign Driver
                                    </label>
                                    <h4 style="text-align:center;"><b>OR</b></h4>
                                </div>
                            </div>



                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <select class="form-control" name="driver" id="driver">
                                    <option value="">Select Driver</option>
                                </select>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <select class="form-control" name="coupon_code" id="coupon_code">
                                    <option value="">Apply Coupon</option>
                                    <?php foreach ($coupons as $list_coupan):?>
                                        <option value="<?php echo $list_coupan['coupons_code']; ?>"><?php echo $list_coupan['coupons_code']."(".$list_coupan['coupons_price'].")"; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <select class="form-control" name="payment_option_id" id="payment_option_id">
                                    <option value="">Select Payment Method</option>
                                    <option value="1">Cash</option>
                                </select>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-success" id="save" name="save" value="Book Now" >
                            </div>
                            <div class="clear"></div>
                            <div class="total-price">
                                Estimation
                                <hr>
                                <ul>
                                    <li><b>Estimated Distance </b> :  <em id="dist_fare_price">0 Km</em></li>
                                    <li><b>Estimated Time </b> :  <em id="time_fare_price">0 Min</em></li>
                                </ul>
                            </div>
                        </div>

                    </form>
                    <div class="col-sm-7">
                        <div class="col-sm-12">
                            <div class="row">

                                <div id="map">
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {lat: <?php echo $admin_settings['admin_panel_latitude']?>, lng: <?php echo $admin_settings['admin_panel_longitude']?>},
            zoom: 13
        });

        new AutocompleteDirectionsHandler(map);
    }

    function AutocompleteDirectionsHandler(map) {
        this.map = map;
        this.originPlaceId = null;
        this.destinationPlaceId = null;
        this.travelMode = 'DRIVING';
        var originInput = document.getElementById('origin-input');
        var destinationInput = document.getElementById('destination-input');
        var modeSelector = document.getElementById('mode-selector');
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
        this.directionsDisplay.setMap(map);

        var originAutocomplete = new google.maps.places.Autocomplete(
            originInput);
        var destinationAutocomplete = new google.maps.places.Autocomplete(
            destinationInput);

        this.setupClickListener('changemode-walking', 'WALKING');
        this.setupClickListener('changemode-transit', 'TRANSIT');
        this.setupClickListener('changemode-driving', 'DRIVING');

        this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
        this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
    }

    AutocompleteDirectionsHandler.prototype.setupClickListener = function(id, mode) {
        var radioButton = document.getElementById(id);
        var me = this;
        radioButton.addEventListener('click', function() {
            me.travelMode = mode;
            me.route();
        });
    };

    AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
        var me = this;
        autocomplete.bindTo('bounds', this.map);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.place_id) {
                window.alert("Please select an option from the dropdown list.");
                return;
            }
            if (mode === 'ORIG') {
                me.originPlaceId = place.place_id;
                document.getElementById("orig_latitude").value = place.geometry.location.lat();
                document.getElementById("orig_longitude").value = place.geometry.location.lng();
            } else {
                me.destinationPlaceId = place.place_id;
                document.getElementById("dest_latitude").value = place.geometry.location.lat();
                document.getElementById("dest_longitude").value = place.geometry.location.lng();
            }
            me.route();

        });

    };

    AutocompleteDirectionsHandler.prototype.route = function() {
        if (!this.originPlaceId || !this.destinationPlaceId) {
            return;
        }
        var me = this;
        this.directionsService.route({
            origin: {'placeId': this.originPlaceId},
            destination: {'placeId': this.destinationPlaceId},
            travelMode: this.travelMode
        }, function(response, status) {

            if (status === 'OK') {
                me.directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
        getDistance();
    };
    function getDistance()
    {

        var distanceService = new google.maps.DistanceMatrixService();
        distanceService.getDistanceMatrix({
                origins: [$("#origin-input").val()],
                destinations: [$("#destination-input").val()],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                durationInTraffic: true,
                avoidHighways: false,
                avoidTolls: false
            },
            function (response, status) {
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    console.log('Error:', status);
                } else {
                    document.getElementById("distance").value = response.rows[0].elements[0].distance.value;
                    document.getElementById("dist_fare_price").innerText = response.rows[0].elements[0].distance.text;
                    document.getElementById("time_fare_price").innerText = response.rows[0].elements[0].duration.text;
                    document.getElementById("time").value = response.rows[0].elements[0].duration.value;
                }
            });
    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $admin_settings['admin_panel_map_key']?>&libraries=places&callback=initMap"
        async defer></script>
</section>
<!-- Main Content Ends -->

</body>
</html>