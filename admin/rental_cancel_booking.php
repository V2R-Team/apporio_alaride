<?php
include_once '../apporioconfig/start_up.php';

include 'pn_android.php';
include 'pn_iphone.php';
$cancel_reason_id=$_REQUEST['cancel_reason_id'];
$rental_booking_id = $_REQUEST['rental_booking_id'];
$time1 = date("h:i:s A");
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];


$message = "Ride Cancel By Admin";
$ride_id = (string)$rental_booking_id;
$ride_status = "19";
if ($driver_id != 0) {
    $query25 = "UPDATE driver SET busy=0  WHERE driver_id='$driver_id'";
    $db->query($query25);

    $query11 = "select * from driver where driver_id='$driver_id'";
    $result11 = $db->query($query11);
    $list11 = $result11->row;
    $device_id1 = $list11['device_id'];

    if ($device_id1 != "") {
        if ($list11['flag'] == 1) {
            IphonePushNotificationDriver($device_id1, $message, $ride_id, $ride_status);
        } else {
            AndroidPushNotificationDriver($device_id1, $message, $ride_id, $ride_status);
        }
    }
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver_id.'/.json';
    $fields = array(
        'ride_id' => "No Ride",
        'ride_status'=>"No Ride Status",
    );
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
    $response = curl_exec($ch);
}
$query1="UPDATE rental_booking SET booking_status=19,last_update_time='$time1' WHERE rental_booking_id='$rental_booking_id'";
$db->query($query1);
$query4="select * from user_device where user_id='$user_id' AND login_logout=1";
$result4 = $db->query($query4);
$list4=$result4->rows;
if (!empty($list4))
{
    foreach ($list4 as $login)
    {
        $device_id = $login['device_id'];
        $flag = $login['flag'];
        if($flag == 1)
        {
            IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
        }
        else
        {
            AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);

        }
    }
}else{
    $query4="select * from user where user_id='$user_id'";
    $result4 = $db->query($query4);
    $list4=$result4->row;
    $device_id=$list4['device_id'];
    if($device_id!="")
    {
        if($list4['flag'] == 1)
        {
            IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
        }
        else
        {
            AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
        }
    }
}
$url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/RideTable/'.$rental_booking_id.'/.json';
$fields = array(
    'changed_destination'=>"0",
    'ride_status' => "19",
    'ride_id'=>(string)$rental_booking_id,
    'done_ride_id'=>""
);
$ch = curl_init();
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
$response = curl_exec($ch);

echo '<script type="text/javascript">alert("Ride Cancel Succesfully")</script>';
$db->redirect("home.php?pages=rental-ride");

?>