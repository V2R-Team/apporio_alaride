<?php
include_once '../apporioconfig/start_up.php';
?>
<header class="top-head container-fluid">
    <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>





    <ul class="list-inline navbar-right top-menu top-right-menu">
        <li class="dropdown text-center">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" class="img-circle" src="images/user.png" />
                <span class="username username-hide-on-mobile">  <?php echo $_SESSION['Company']['UN'];?></span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                <li>
                    <a href="home.php?pages=change-password">
                        <i class="fa fa-key" aria-hidden="true"></i>Change Password </a>
                </li>
                <li>
                    <a href="home.php?pages=logout">
                        <i class="fa fa-sign-out"></i>Log Out </a>
                </li>
            </ul>
        </li>
    </ul>

</header>