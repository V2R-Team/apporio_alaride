<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Taxi Admin Panel">

    <title>Taxi Company || Admin</title>


    <?php include('style.php'); ?>
    <!-- Stylesheet Closed -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>


</head>


<body>

<!-- Sidebar Menu Start-->
<?php include('sidebar.php'); ?>


<!--Main Content Start -->
<section class="content">

    <!-- Header Starts -->
    <?php include('header.php'); ?>
    <!-- Header Starts -->

    <!-- Footer Starts -->
    <?php include('footer.php'); ?>
    <!-- Footer Closed -->

    <!-- Javascript Starts -->
    <?php include('design.php'); ?>
    <!-- Javascript Starts -->

