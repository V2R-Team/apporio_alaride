<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['Company']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$company_id = $_SESSION['Company']['ID'];
$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id where ride_table.ride_type=1 AND ride_table.ride_status=7 AND driver.company_id='$company_id' ORDER BY ride_table.ride_id";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key => $value) {
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];
    $list[$key] = $value;
    $list[$key]["payment_option_name"] = $payment_option_name;
}
$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id where ride_table.ride_type = 2 AND ride_table.ride_status=7 AND driver.company_id='$company_id' ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$ride_later = $result->rows;
foreach ($ride_later as $key => $value) {
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];
    $ride_later[$key] = $value;
    $ride_later[$key]["payment_option_name"] = $payment_option_name;
}
?>

<style>
    .clear {
        clear: both !important;
    }

    .bookind-id {
        cursor: pointer;
        text-decoration: underline;
    }

</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Completed Rides</h3>
    </div>
    <div class="row m-t-30">
        <div class="col-sm-12">

            <ul class="nav-tabs navi navi_tabs">
                <li class="active"><a data-toggle="tab" href="#personal"><i class="fa fa-car" aria-hidden="true"></i>
                        <b>Ride Now Bookings</b></a></li>
                <li class=""><a data-toggle="tab" href="#address"><i class="fa fa-clock-o" aria-hidden="true"></i> <b>Ride
                            Later Bookings</b></a></li>
            </ul>
            <div class="panel panel-default p-0">
                <div class="panel-body p-0">

                    <div class="tab-content m-0">
                        <div id="personal" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table id="datatable1"
                                               class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="47px">URN&nbsp;&nbsp;<i class="fa fa-info-circle"
                                                                                   data-toggle="tooltip"
                                                                                   data-placement="top"
                                                                                   title="Unique Reference Number"></i>
                                                </th>
                                                <th width="5">Rider Name</th>
                                                <th width="5">Driver</th>
                                                <th>Pickup Address</th>
                                                <th>Drop Address</th>
                                                <th width="5%">Payment Mode</th>
                                                <th width="10%">Ride booked time</th>
                                                <th width="5%">Current Status</th>
                                                <th>Invoice</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($list as $ridenow) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="home.php?pages=trip-details&id=<?= $ridenow['ride_id'] ?>"><span
                                                                title="Full Details"
                                                                class="bookind-id"> <?php echo $ridenow['ride_id']; ?> </span></a>
                                                    </td>
                                                    <td><?php $user_name = $ridenow['user_name'];
                                                        echo $user_name;
                                                        ?></td>
                                                    <td><?php $driver_name = $ridenow['driver_name'];
                                                        if ($driver_name == "") { ?>
                                                            <h4 style="color:red;">Not Assign</h4>
                                                        <?php } else {
                                                            echo $driver_name;
                                                        } ?></td>

                                                    <td>
                                                        <?php
                                                        $pickup_location = $ridenow['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $drop_location = $ridenow['drop_location'];
                                                        echo $drop_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $payment_option_name = $ridenow['payment_option_name'];
                                                        echo $payment_option_name;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ride_date = $ridenow['ride_date'];
                                                        echo $ride_date . "," . $ridenow['ride_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $ridenow['ride_status'];
                                                        $timestap = $ridenow['last_time_stamp'];
                                                        switch ($ride_status) {
                                                            case "1":
                                                                echo nl2br("New Booking \n " . $timestap);
                                                                break;
                                                            case "2":
                                                                echo nl2br("Cancelled By User  \n " . $timestap);
                                                                break;
                                                            case "3":
                                                                echo nl2br("Accepted by Driver  \n " . $timestap);
                                                                break;
                                                            case "4":
                                                                echo nl2br("Cancelled by driver  \n " . $timestap);
                                                                break;
                                                            case "5":
                                                                echo nl2br("Driver Arrived  \n " . $timestap);
                                                                break;
                                                            case "6":
                                                                echo nl2br("Trip Started  \n " . $timestap);
                                                                break;
                                                            case "7":
                                                                echo nl2br("Trip Completed  \n " . $timestap);
                                                                break;
                                                            case "8":
                                                                echo nl2br("Trip Book By Admin  \n " . $timestap);
                                                                break;
                                                            default:
                                                                echo "----";
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php $ride_status = $ridenow['ride_status'];
                                                        if ($ride_status == 2 || $ride_status == 8 || $ride_status == 9 || $ride_status == 4) { ?>
                                                            <button type="button" id="action_btn"
                                                                    class="btn btn-default btn-xs" data-toggle="tooltip"
                                                                    data-placement="top" title="Assign" data-target="#">
                                                                <i class="fa fa-user-plus" id="btn_icon"></i></button>
                                                        <?php } elseif ($ride_status == 6 || $ride_status == 5) {
                                                            echo "Ride Started";
                                                        } elseif ($ride_status == 7) { ?>
                                                            <a target="_blank" href="home.php?pages=invoice&id=<?php echo $ridenow['ride_id'];?>">
                                                                <button type="button" class="btn menu-icon btn_eye"
                                                                        data-toggle="tooltip" data-placement="top"
                                                                        title="Invoice" data-target="#"><i
                                                                        class="fa fa-print" id=""></i></button>
                                                            </a>
                                                        <?php } else { ?>
                                                            <button type="button" id="action_btn"
                                                                    class="btn btn-default btn-xs" data-toggle="tooltip"
                                                                    data-placement="top" title="Cancel" data-target="#">
                                                                <i class="fa fa-ban" id="btn_icon"></i></button>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="clear"></div>

                        <div id="address" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table id="datatable"
                                               class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="50px">URN&nbsp;&nbsp;<i class="fa fa-info-circle"
                                                                                   data-toggle="tooltip"
                                                                                   data-placement="top"
                                                                                   title="Unique Reference Number"></i>
                                                </th>
                                                <th width="5">Rider Name</th>
                                                <th width="5">Driver</th>
                                                <th>Pickup Address</th>
                                                <th>Drop Address</th>
                                                <th width="5%">Payment Mode</th>
                                                <th width="10%">Ride booked time</th>
                                                <th width="5%">Current Status</th>
                                                <th>Ride Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($ride_later as $ride) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="home.php?pages=trip-details&id=<?= $ride['ride_id'] ?>"><span
                                                                title="Full Details"
                                                                class="bookind-id"> <?php echo $ride['ride_id']; ?> </span></a>
                                                    </td>
                                                    <td><?php $user_name = $ride['user_name'];
                                                        echo $user_name;
                                                        ?></td>
                                                    <td><?php $driver_name = $ride['driver_name'];
                                                        if ($driver_name == "") { ?>
                                                            <h4 style="color:red;">Not Assign</h4>
                                                        <?php } else {
                                                            echo $driver_name;
                                                        } ?></td>

                                                    <td>
                                                        <?php
                                                        $pickup_location = $ride['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $drop_location = $ride['drop_location'];
                                                        echo $drop_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $payment_option_name = $ride['payment_option_name'];
                                                        echo $payment_option_name;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ride_date = $ride['later_date'];
                                                        echo $ride_date . "," . $ride['later_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $ride['ride_status'];
                                                        $timestap = $ride['last_time_stamp'];
                                                        switch ($ride_status) {
                                                            case "1":
                                                                echo nl2br("New Booking \n " . $timestap);
                                                                break;
                                                            case "2":
                                                                echo nl2br("Cancelled By User  \n " . $timestap);
                                                                break;
                                                            case "3":
                                                                echo nl2br("Accepted by Driver  \n " . $timestap);
                                                                break;
                                                            case "4":
                                                                echo nl2br("Cancelled by driver  \n " . $timestap);
                                                                break;
                                                            case "5":
                                                                echo nl2br("Driver Arrived  \n " . $timestap);
                                                                break;
                                                            case "6":
                                                                echo nl2br("Trip Started  \n " . $timestap);
                                                                break;
                                                            case "7":
                                                                echo nl2br("Trip Completed  \n " . $timestap);
                                                                break;
                                                            case "8":
                                                                echo nl2br("Trip Book By Admin  \n " . $timestap);
                                                                break;
                                                            default:
                                                                echo "----";
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php $ride_status = $ride['ride_status'];
                                                        if ($ride_status == 2 || $ride_status == 8 || $ride_status == 9 || $ride_status == 4) { ?>
                                                            <button type="button" class="btn btn-success btn-xs"
                                                                    data-toggle="modal" data-target="#"> Assign
                                                            </button>
                                                        <?php } elseif ($ride_status == 6 || $ride_status == 5) {
                                                            echo "Ride Started";
                                                        } elseif ($ride_status == 7) {
                                                            echo '<a href="javascript:void(0);" onclick="javascript:window.open(&quot;invoice.php&quot;,&quot;_blank&quot;)" ;>
								<button type="button" class="btn menu-icon btn_eye" data-toggle="tooltip" data-placement="top" title="View Invoice"  data-target="#"  ><i class="fa fa-print" ></i></button></a>';
                                                        } else { ?>
                                                            <button type="button" class="btn btn-danger btn-xs"
                                                                    data-toggle="modal"
                                                                    data-target="#cancel<?php echo $ride['ride_id']; ?>">
                                                                Cancel
                                                            </button>
                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>